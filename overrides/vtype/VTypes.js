/**
 * 表单规则校验插件
 *
 * create by gongzhenwen on 2016/9/7 10:05
 */

/**
 * 扩展-中文汉字表示3个字符长度
 */
String.prototype.len = function () {
    return this.replace(/[^\x00-\xff]/g, "***").length;
};


/**
 * 系统扩展Vtype插件
 */
Ext.define('App.overrides.vtype.VTypes', {
    override: 'Ext.form.field.VTypes',

    /**
     * 最大只能输入{maxLen}个字符(一个汉字占3个字符)
     * @param value
     * @param field
     * @returns {boolean}
     */
    maxLen: function (value, field) {
        var maxLen = field.maxLen;
        this.link('maxLenText', '最大只能输入' + maxLen + '个字符(一个汉字占3个字符)');
        return value.len() <= maxLen;
    },

    /**
     * 工号规则校验
     */
    positiveInteger: function (value) {
        return this.positiveIntegerRe.test(value);
    },
    positiveIntegerRe: /^[0-9]*[1-9][0-9]*$/,
    positiveIntegerText: '请输入正常的工号！',
    positiveIntegerMask: /^[0-9]*[1-9][0-9]*$/,


    /**
     * 起止日期规则校验
     *
     * @param {}
     *          val
     * @param {}
     *          field
     * @return {Boolean}
     */
    daterange: function (val, field) {
        var date = field.parseDate(val);
        if (!date) {
            return false;
        }
        if (field.startDateField
            && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
            var start = Ext.getCmp(field.startDateField);
            start.setMaxValue(date);
            start.validate();
            this.dateRangeMax = date;
        } else if (field.endDateField
            && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
            var end = Ext.getCmp(field.endDateField);
            end.setMinValue(date);
            end.validate();
            this.dateRangeMin = date;
        }
        return true;
    },

    commondaterange: function (val, field) {
        var date = field.parseDate(val);
        if (!date) {
            return false;
        }
        var name;
        if (field.dateFlag == 'start'
            && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
            name = 'datefield[dateFlag=end]';
            var end = field.up('container').down(name);
            if (!end) {
                end = field.up('form').down(name);
            }
            if(end){
                end.setMinValue(date);
                if(end.getValue() && end.getValue().setHours(0,0,0,0) < date.setHours(0,0,0,0)){
                    end.setValue(date);
                }
                this.dateRangeMin = date;
            }

        } else if (field.dateFlag == 'end'
            && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
            name = 'datefield[dateFlag=start]';
            var start = field.up('container').down(name);
            if (!start) {
                start = field.up('form').down(name);
            }
            if(start){
                start.setMaxValue(date);
                if(start.getValue() && start.getValue().setHours(0,0,0,0) > date.setHours(0,0,0,0)){
                    start.setValue(date);
                }
                this.dateRangeMax = date;
            }
        }
        return true;
    },
    daterangeText: '开始时间必须小与结束时间'
});