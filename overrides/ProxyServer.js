Ext.define("App.overrides.ProxyServer", {
    override: 'Ext.data.proxy.Server',
    config:{
        limitParam: 'pageSize',
        pageParam: 'pageNum'
    }
});
