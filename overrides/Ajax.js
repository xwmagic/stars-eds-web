Ext.define("App.overrides.Ajax", {
    override: 'Ext.data.request.Ajax',
    //拦截请求，在所有请求签名加上token
    setupHeaders: function(xhr, options, data, params) {
        if(options.headers){
            options.headers.Authorization = localStorage.getItem('Authorization');
        }else {
            options.headers = {
                Authorization:localStorage.getItem('Authorization')
            }
        }
        return this.callParent([xhr, options, data, params]);
    },
    openRequest: function(options, requestOptions, async, username, password) {
        //添加提交地址的统一前缀
        if(URLPreName){
            requestOptions.url = URLPreName + requestOptions.url;
        }
        return this.callParent([options, requestOptions, async, username, password]);
    }
});
