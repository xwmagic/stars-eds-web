/**使得所有ComboBox具备双击展开功能
 * @author xiaowei
 * @date 2017年6月22日
 */
Ext.define('App.overrides.base.ColorPicker', {
    override: 'Ext.ux.colorpick.Field',
    setValue: function(color) {
        if(!color){
            color = '#0ca20a';
        }
        this.callParent(arguments);
    }

});