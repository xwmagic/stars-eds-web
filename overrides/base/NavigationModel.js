/**
 * 如果列表同时存在横向滚动条和竖向滚动条，当竖向滚动条滚动到底部时
 * 点击横向滚动条，滚动条会自动滚动到顶部
 * 6.2.1 bug修复
 */
/*
Ext.define('App.overrides.base.NavigationModel', {
    override: 'Ext.grid.NavigationModel',
    //当列表被点击时
    onContainerMouseDown: function (view, mousedownEvent) {
        var me = this,
            context = new Ext.grid.CellContext(view),
            lastFocused,
            position;
        //执行Ext.grid.NavigationModel父类的同名方法
        //执行此方法后如果点击的是滚动条view.lastFocused的值会变为scrollbar
        //可以由此判断点击的是滚动条还是列表内容
        //这样就能解决这个bug
        me.callSuper([view, mousedownEvent]);
        lastFocused = view.lastFocused;
        position = (view.actionableMode && view.actionPosition) || lastFocused;
        //判断点击的元素是否是滚动条，如果是则不做任何操作
        if (!position || lastFocused === 'scrollbar') {
            return;
        }

        context.setPosition(position.record, position.column);
        mousedownEvent.position = context;
        me.attachClosestCell(mousedownEvent);

        // If we are not already on that position, set position there.
        if (!me.position.isEqual(context)) {
            me.setPosition(context, null, mousedownEvent);
        }
    }
});*/
