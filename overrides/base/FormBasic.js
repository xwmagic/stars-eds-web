/**修复formBasic的setValues方法 ，当在FormPanel中存在组件radiogroup和checkboxgroup时，setValues方法无法正确设置值的缺陷。
 * @author xiaowei
 * @date 2017年8月2日
 */
Ext.define('App.overrides.base.FormBasic', {
    override: 'Ext.form.Basic',
    getValues:function () {
        var values = this.callParent();
        if(values){
            var pikers = this.owner.query('pickerfield');
            var i;
            for(var i in pikers){
                if(pikers[i].keyName && !pikers[i].disabled &&  !values.hasOwnProperty(pikers[i].keyName)){
                    values[pikers[i].keyName] = pikers[i].getKeyValue();
                }
            }
            var combos = this.owner.query('combo');
            for(i in combos){
                if(combos[i].keyName && !combos[i].disabled && !values.hasOwnProperty(combos[i].keyName)){
                    values[combos[i].keyName] = combos[i].getKeyValue();
                }
            }
        }
        return values;
    },
	setValues: function(values) {
        var me = this,
            v, vLen, val;

        function setVal(fieldId, val) {
            var field = me.findField(fieldId);
			var valObj = {},i, fields;
            if (field && !field.disabled) {
                if(field.is('pickerfield') && field.keyName){
                    field.setKeyValue(values[field.keyName]);
                    field.setValue(val);
                    field.setRawValue(val);
                } else if(field.is('radio') || field.is('checkbox')){
					fields = me.owner.query(field.xtype +'[name='+fieldId+']');
					for(i=0; i<fields.length; i++){
						if(fields[i].inputValue === val){
							fields[i].setValue(true);
						}else{
							fields[i].setValue(false);
						}
					}
				}else{
					field.setValue(val);
				}
               
                if (me.trackResetOnLoad) {
                    field.resetOriginalValue();
                }
            }
        }

        // Suspend here because setting the value on a field could trigger
        // a layout, for example if an error gets set, or it's a display field
        Ext.suspendLayouts();
        if (Ext.isArray(values)) {
            // array of objects
            vLen = values.length;

            for (v = 0; v < vLen; v++) {
                val = values[v];

                setVal(val.id, val.value);
            }
        } else {
            // object hash
            Ext.iterate(values, setVal);
        }
        Ext.resumeLayouts(true);
        return this;
    },
    reset:function () {
        this.callParent(arguments);
        var dates = this.owner.query('commonDateRange');
        var i;
        for(i=0; i<dates.length; i++){
            dates[i].reset();
        }
        var pikers = this.owner.query('pickerfield');
        for(i in pikers){
            if(pikers[i].keyName){
                pikers[i].setKeyValue('');
            }
        }
        var combos = this.owner.query('combo');
        for(i in combos){
            if(combos[i].keyName){
                combos[i].setKeyValue('');
            }
        }
    }

});