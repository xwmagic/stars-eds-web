/**让store具有直接配置url地址的功能
 * @author xiaowei
 * @date 2017年5月3日
 */
Ext.define('App.overrides.base.ProxyStore', {
    override: 'Ext.data.Store',
    onProxyLoad:function (operation) {
        if(this.grid && operation._resultSet){
            operation._resultSet.records = this.grid.beforeAddDatas(operation._resultSet.records);
        }
        this.callParent([operation]);
    },
    getNewRecords : function() {
		var datas = this.callParent(arguments);
		if(datas.length < 1){
            this.each(function (record, i) {
                if(record.data._add){
                    datas.push(record);
				}
            });
		}
        return datas;
	}
});