//每一个列都会出现鼠标悬浮上去显示内容
Ext.define('App.overrides.base.Tree', {
    override: 'Ext.tree.Panel',
    //checkPropagation: 'upAll',//增加upAll，当选中子节点是，自动选中所有父节点
    isCheckTree:false,//将树转换为带复选框的树
    forceNoCheckTree:false,//将在将树转换为不带复选框的树
    initComponent:function () {
        this.on({
            checkchange:function (record, checked) {
                if(this.checkPropagation === 'upAll'){
                    var me = this;
                    //如果孩子节点被选中，那么选中自己
                    if(me.checkChildHasCheck(record) && !record.get('checked')){
                        record.set('checked',true);
                    }
                    //如果有父节点，继续事件
                    if(checked && record.parentNode){
                        me.fireEvent('checkchange',record.parentNode);
                    }
                }
            }/*,
            mousedown: {
                element: 'el',
                fn: function (a) {
                    //修复拖动与点击事件冲突
                    if (a.parentEvent) {
                        this.component.pointTop = a.parentEvent.point.top;
                        this.component.pointLeft = a.parentEvent.point.left;
                    }
                }
            },
            beforeitemclick: function (a, b, c, d, e, f) {
                //修复拖动与点击事件冲突
                if (e.parentEvent && (Math.abs(this.pointTop - e.parentEvent.touch.point.top) > 2 || Math.abs(this.pointLeft - e.parentEvent.touch.point.left) > 2)) {
                    return false
                }
            },
            beforecellclick: function (a, b, c, d, e, f, g, h) {
                //修复拖动与点击事件冲突
                if (g.parentEvent && (Math.abs(this.pointTop - g.parentEvent.touch.point.top) > 2 || Math.abs(this.pointLeft - g.parentEvent.touch.point.left) > 2)) {
                    return false
                }
            }*/
        });
        this.callParent();
        this.getStore().isCheckTree = this.isCheckTree;
        this.getStore().forceNoCheckTree = this.forceNoCheckTree;
    },
    checkChildHasCheck:function (record) {
        if(!record.childNodes || record.childNodes.length < 1){
            return false;
        }
        for(var i=0; i<record.childNodes.length; i++){
            if(record.childNodes[i].get('checked')){
                return true;
            }
        }
        return false;
    },

    /**
     * 向下选中所有孩子包括自己
     * @param record
     */
    checkAllChild:function (record) {
        var i;
        record.set('checked',true);
        if(record.childNodes && record.childNodes.length > 0){
            for(i=0; i<record.childNodes.length; i++){
                if(record.childNodes[i].childNodes && record.childNodes[i].childNodes.length > 0){
                    this.checkAllChild(record.childNodes[i]);
                }
                record.childNodes[i].set('checked',true);
            }
        }
    },
    /**
     * 向下取消所有孩子包括自己得选中状态
     * @param record
     */
    uncheckAllChild:function (record) {
        var i;
        record.set('checked',false);
        if(record.childNodes && record.childNodes.length > 0){
            for(i=0; i<record.childNodes.length; i++){
                if(record.childNodes[i].childNodes && record.childNodes[i].childNodes.length > 0){
                    this.uncheckAllChild(record.childNodes[i]);
                }
                record.childNodes[i].set('checked',false);
            }
        }
    },
    _getSelectionDataIds: function () {
        var records = this.getSelection();
        var datas = [], record, i;
        for (i = 0; i < records.length; i++) {
            datas.push(records[i].get('id'));
        }
        return datas;
    },
    _getCheckedDataIds: function () {
        var records = this.getChecked();
        var datas = [], i;
        for (i = 0; i < records.length; i++) {
            datas.push(records[i].get('id'));
        }
        return datas;
    },
    _getCheckedDatas:function () {
        var records = this.getChecked();
        var datas = [], i;
        for (i = 0; i < records.length; i++) {
            datas.push(records[i].data);
        }
        return datas;
    },
    _getCheckedDataPropertyValue:function (fieldName) {
        var records = this.getChecked();
        var datas = [],  i;
        for (i = 0; i < records.length; i++) {
            datas.push(records[i].get(fieldName));
        }
        return datas;
    }
});