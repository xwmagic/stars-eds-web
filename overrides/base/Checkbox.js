/**Checkbox
 * @author xiaowei
 * @date 2017年6月22日
 */
Ext.define('App.overrides.base.Checkbox', {
    override: 'Ext.form.field.Checkbox',
    inputValue:'1'
});