/**让store具有直接配置url地址的功能
 * @author xiaowei
 * @date 2017年5月3日
 */
Ext.define('App.overrides.base.ProxyStore', {
    override: 'Ext.data.ProxyStore',
	constructor : function() {
		this.callParent(arguments);
		var me = this;
		if(me.proxy && !me.proxy.url && me.url){
			me.proxy.url = me.url;
		}
	},
	listeners:{
    	load:function (th, records, successful, operation,re) {
            if(!successful && operation._response && operation._response.responseText){
                var obj = Ext.decode(operation._response.responseText);
                if(obj.errorMessage){
                    App.ux.Toast.show('提示',obj.errorMessage,'e');
                }
                if(obj.code === '004' && operation.url != 'user/login'){
                    Ext.Msg.alert('提示','当前账户登录已过期！',function(){
                        GTimeOut();
                    });
                    return;
                }
			}
        }
	}
});