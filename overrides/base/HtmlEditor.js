/**HtmlEditor
 * @author xiaowei
 * @date 2017年6月22日
 */
Ext.define('App.overrides.base.HtmlEditor', {
    override: 'Ext.form.field.HtmlEditor',
    border: 1,
    style: {
        borderColor: '#ded9d9',
        borderStyle: 'solid'

    },
    margin:5,
    labelAlign:'top',
    listeners:{
        boxready:function(){
            this.getToolbar().setMaxWidth(this.getWidth()-15);
            this.items.items[1].setMaxWidth(this.getWidth()-15);
        }
    }

});