/**使得所有Grid表单列头字体加大加粗
 * @author xiaowei
 * @date 2017年7月7日
 */
Ext.define('App.overrides.base.GridColumn', {
    override: 'Ext.grid.header.Container',
	defaults: {
		style: {
			fontSize: '14px',
			fontWeight:700,
			color: '#525252'
		}
	},
	initComponent : function() {
		this.callParent(arguments);
	}
	
});