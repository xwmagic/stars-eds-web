Ext.ariaWarn = Ext.emptyFn;
//公共变量
var LRAjax = Ext.create('LRAjax');
//统一URL前缀
var URLPreName = 'api/';
//统一注销接口
var GTimeOut = function(){
	var main = Ext.getCmp('main-view-detail-wrap');
	main.getComponent('contentPanel').removeAll();
    localStorage.setItem("systemMenus", Ext.encode({}));
    localStorage.setItem("Authorization", "");
    if(Ext.isIE){
        location.replace(location.protocol + '//'+ location.host+location.pathname);
	}else{
        location.replace(location.origin+location.pathname);
	}
    function clearCookie(){
        var date=new Date();
        date.setTime(date.getTime()-10000);
        var keys=document.cookie.match(/[^ =;]+(?=\=)/g);
        if (keys) {
            for (var i =  keys.length; i--;)
                document.cookie=keys[i]+"=0; expire="+date.toGMTString()+"; path=/";
        }
    }
    clearCookie();
};
/**
 * code 生成配置
 */
var codeGenPathDef = "E:\\projects\\stars-show-8084\\stars-show-web\\classic\\src\\view\\p_show\\";

Ext.define('Admin.Application', {
    extend: 'Ext.app.Application',
    name: 'Admin',
    requires: [
    	//基础
    	'Ext.grid.GridPanel',
        "Ext.form.RadioGroup",
        "App.ux.Toast",
        'Ext.window.Toast',
        'App.ux.Ajax',
        'App.ux.MathTool',
		//system
        "App.*"
	],
    stores: [
        'NavigationTree'
    ],

    //defaultToken : 'usergrid',

    // The name of the initial view to create. This class will gain a "viewport" plugin
    // if it does not extend Ext.Viewport.
    //
    mainView: 'Admin.view.main.Main',

    onAppUpdate: function () {
        /*Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );*/
    },
	launch: function () {
    	//初始化更多数据方法
        Ext.create('App.ux.MathTool').initMaths();

		var me = this;
        String.prototype.trim = function () {
			return this .replace(/^\s\s*/, '' ).replace(/\s\s*$/, '' );
		};
		Ext.Ajax.on('requestcompvare',function(conn, response, options, eOpts){
			var obj = Ext.decode(response.responseText);
			//判断用户是否过期
			if(obj.code === '004' && options.url != 'user/login'){
				Ext.Msg.alert('提示','当前账户登录已过期！',function(){
					GTimeOut();
				});
				//response.responseText = '';
				return;
			}

		});
		
		Ext.Ajax.on('requestexception',function(conn, response, options, eOpts){
			if(options.url.split('/')[0] == 'realtime'){//排除实时数据
				return;
			}
			if(!options.result){
				Ext.Msg.alert('失败了', '请求服务异常，请检查网络或联系管理员！');
			}else{
				Ext.Msg.alert('失败了', options.result.tip);
			}
		});
    }
});
