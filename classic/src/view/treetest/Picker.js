Ext.define("App.view.classic.treetest.Picker", { 
    extend: "App.view.common.TreeMuiltiPicker",
    xtype: 'classictreetestPicker',
    gridClassName:'App.classic.view.system.menu.MainTreeGrid',
    checkModel:'single',//选择模式
    onlyLeaf:false,
    displayField: 'name',
    valueField: 'id'
});
