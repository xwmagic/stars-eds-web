Ext.define("App.view.classic.system.app.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemappMain',
    requires: [ 
        "App.view.classic.system.app.Form",
        "App.view.classic.system.app.Grid",
        "App.view.classic.system.app.SearchForm",
        "App.view.classic.system.app.Toolbar",
        "App.view.classic.system.app.ViewModel",
        "App.view.classic.system.app.Window",
        "App.view.classic.system.app.WindowAdd",
        "App.view.classic.system.app.WindowEdit"
    ],
    viewModel: 'classicsystemappViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemappSearchForm',hidden: false},
        {xtype: 'classicsystemappToolbar',hidden: false},
        {xtype: 'classicsystemappGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemappToolbar');
            var form = th.down('classicsystemappSearchForm');
            var grid = th.down('classicsystemappGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
