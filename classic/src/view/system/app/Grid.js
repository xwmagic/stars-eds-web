Ext.define("App.view.classic.system.app.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicsystemappGrid',
    _mainConfs: App.view.classic.system.app.Datas.Main,
    url:'sysAppCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"appName",width:150},
            {name:"appCode",width:120},
            {name:"appTypeName",width:120},
            {name:"appEnName",width:120},
            {name:"appUser",width:100},
            {name:"appLogo"},
            {name:"appBkg"},
            {name:"appUrl"},
            {name:"orgName"},
            {name:"appInfo",flex:1}
        ]
    }
});
