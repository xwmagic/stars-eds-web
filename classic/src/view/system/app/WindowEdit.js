Ext.define("App.view.classic.system.app.WindowEdit", { 
    extend: "App.view.classic.system.app.Window",
    alias: "widget.classicsystemappWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'sysAppCtl/update'}, 'close']
});
