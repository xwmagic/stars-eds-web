Ext.define("App.view.classic.system.app.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemappViewModel',
    data: {
        pageName: 'classicsystemappMain',
        add: true,
        update: true,
        del: true
    }
});
