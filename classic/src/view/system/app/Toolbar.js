Ext.define("App.view.classic.system.app.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicsystemappToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'classicsystemappWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'classicsystemappWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'sysAppCtl/delById'}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
