Ext.define("App.view.classic.system.app.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemappSearchForm',
    _mainConfs: App.view.classic.system.app.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"appName",maxLength:100}
                ,{name:"appType",maxLength:10}
                ,{name:"appCode",maxLength:50}
                ,"sr","ec"
            ],
            [
                {name:"appUser",maxLength:100}
                ,{name:"appEnName",maxLength:100}
                ,{name:"appInfo",maxLength:100}
            ],
            [
                {name:"orgName",xtype:"classicsystemappPicker",keyName:'orgId',minWidth:600}
            ]
        ]
    }
});
