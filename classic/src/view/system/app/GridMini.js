Ext.define("App.view.classic.system.app.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicsystemappGridMini',
    _mainConfs: App.view.classic.system.app.Datas.Main,
    url:'sysAppCtl/pageList',//tree的查询地址
    filterName: 'appName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"appName",width:150},
            {name:"appCode",width:120},
            {name:"appType",width:120},
            {name:"appEnName",width:120},
            {name:"appUser",width:100},
            {name:"appLogo"},
            {name:"appBkg"},
            {name:"appUrl"},
            {name:"appInfo",flex:1}
        ]
    }
});
