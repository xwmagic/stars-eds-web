Ext.define("App.view.classic.system.company.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键id'}
            ,{ name:'companyName', type:'string', text:'公司名称'}
            ,{ name:'companyEn', type:'string', text:'公司英文名称'}
            ,{ name:'companyCode', type:'string', text:'公司代码'}
            ,{ name:'companyType', type:'int', text:'公司类型'}
            ,{ name:'logo', type:'string', text:'公司logo'}
            ,{ name:'info', type:'string', text:'描述'}
            ,{ name:'createName', type:'string', text:'创建人'}
            ,{ name:'createId', type:'string', text:'创建人id'}
            ,{ name:'createDate', type:'date', text:'创建时间'}
            ,{ name:'lastUpdateDate', type:'date', text:'修改时间'}
            ,{ name:'lastUpdateId', type:'string', text:'修改人id'}
            ,{ name:'lastUpdateName', type:'string', text:'修改人'}
            ,{ name:'version', type:'int', text:'版本'}
            ,{ name:'isDelete', type:'string', text:'是否删除'}
            ,{ name:'isEnable', type:'string', text:'是否可用'}
            ,{ name:'pid', type:'string', text:'父ID'}
            ,{ name:'layer', type:'int', text:'层级'}
            ,{ name:'sortNum', type:'int', text:'排序号'}
        ]
    }
});
