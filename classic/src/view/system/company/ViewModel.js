Ext.define("App.view.classic.system.company.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemcompanyViewModel',
    data: {
        pageName: 'classicsystemcompanyMain',
        lock: true,
        add: true,
        addChild: true,
        save: true,
        del: true
    }
});
