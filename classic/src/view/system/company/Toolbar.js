Ext.define("App.view.classic.system.company.Toolbar", { 
    extend: "App.view.common.TreeToolbar",
    xtype: 'classicsystemcompanyToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            'lock',
            'c',
            'addChild',//处理方法_create
            'd',//处理方法_delete
            {name: 'save',actionUrl:'sysCompanyCtl/add'}//处理方法_save
        ]
    }
});
