Ext.define("App.view.classic.system.urls.WindowEdit", { 
    extend: "App.view.classic.system.urls.Window",
    alias: "widget.classicsystemurlsWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'sysUrlsCtl/update'}, 'close']
});
