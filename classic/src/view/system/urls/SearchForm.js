Ext.define("App.view.classic.system.urls.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemurlsSearchForm',
    _mainConfs: App.view.classic.system.urls.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"urlName",maxLength:500}
                ,{name:"urlClass",maxLength:200}
                ,"sr","ec"
            ],
            [
                {name:"urlPackage",maxLength:100}
                ,{name:"urlTags",maxLength:100}
                ,{name:"urlInfo",maxLength:100}
            ]
        ]
    }
});
