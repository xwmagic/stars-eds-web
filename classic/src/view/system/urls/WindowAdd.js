Ext.define("App.view.classic.system.urls.WindowAdd", { 
    extend: "App.view.classic.system.urls.Window",
    alias: "widget.classicsystemurlsWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'sysUrlsCtl/add'}, 'close']
});
