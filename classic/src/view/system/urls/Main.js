Ext.define("App.view.classic.system.urls.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemurlsMain',
    requires: [ 
        "App.view.classic.system.urls.Form",
        "App.view.classic.system.urls.Grid",
        "App.view.classic.system.urls.SearchForm",
        "App.view.classic.system.urls.Toolbar",
        "App.view.classic.system.urls.ViewModel",
        "App.view.classic.system.urls.Window",
        "App.view.classic.system.urls.WindowAdd",
        "App.view.classic.system.urls.WindowEdit"
    ],
    viewModel: 'classicsystemurlsViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemurlsSearchForm',hidden: false},
        {xtype: 'classicsystemurlsToolbar',hidden: false},
        {xtype: 'classicsystemurlsGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemurlsToolbar');
            var form = th.down('classicsystemurlsSearchForm');
            var grid = th.down('classicsystemurlsGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
