Ext.define("App.view.classic.system.urls.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicsystemurlsPicker',
    gridClassName:'App.view.classic.system.urls.GridMini',
    displayField: 'urlName',
    valueField:'id'
});
