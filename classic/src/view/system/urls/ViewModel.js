Ext.define("App.view.classic.system.urls.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemurlsViewModel',
    data: {
        pageName: 'classicsystemurlsMain',
        add: true,
        update: true,
        del: true
    }
});
