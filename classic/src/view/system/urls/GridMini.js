Ext.define("App.view.classic.system.urls.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicsystemurlsGridMini',
    _mainConfs: App.view.classic.system.urls.Datas.Main,
    url:'sysUrlsCtl/pageList',//tree的查询地址
    filterName: 'urlName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"urlName",width:300,align:'left'},
            {name:"urlClass",align:'left'   },
            {name:"urlTags"},
            {name:"urlInfo"},
            {name:"urlPackage"}
        ]
    }
});
