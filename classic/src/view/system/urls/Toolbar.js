Ext.define("App.view.classic.system.urls.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicsystemurlsToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'classicsystemurlsWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'classicsystemurlsWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'sysUrlsCtl/delById'},//处理方法_delete，actionUrl配置删除的请求地址
            {name: 'createURLS',text:'生成URL',handler: function () { this.up('commontoolbar')._createUrls();}}
        ]
    },
    _createUrls:function () {
        var me = this;

        $.ajax({
            //请求方式
            type : "POST",
            //请求的媒体类型
            contentType: "application/json;charset=UTF-8",
            //请求地址
            url : "api/v2/api-docs?group=%E6%82%A6%E5%8A%A8%E6%98%9F%E8%BE%B0API",
            //数据，json字符串
            data : '',
            //请求成功
            success : function(result) {
                if(result && result.paths){
                    var temp,param,urlName,paths = result.paths,tags,jsonStr = [];
                    for(var i in paths){
                        i.indexOf('/');
                        urlName = i;
                        if(i.indexOf('/') === 0){
                            urlName = i.substring(1);
                        }
                        temp = urlName.split('/');

                        param = {
                            urlName:urlName
                        };
                        if(paths[i].post){
                            tags = paths[i].post.tags;
                            param.urlTags = tags ? tags.toString() : "";
                            param.urlInfo = paths[i].post.summary;
                        }
                        if(temp.length > 1){
                            param.urlClass = temp[0];
                        }
                        jsonStr.push(param);
                    }
                    me.addUrl({
                        jsonStr:Ext.encode(jsonStr)
                    });
                }
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
                console.log(e.responseText);
            }
        });
        /**/
    },
    addUrl:function (param) {
        App.ux.Ajax.request({
            url: 'sysUrlsCtl/add',
            params:param,
            success: function (obj) {
                console.log("URL地址添加成功："+param.urlName);
            },
            failure: function () {
                console.log("----URL地址添加失败----："+param.urlName);
            }
        });
    }
});
