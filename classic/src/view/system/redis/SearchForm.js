Ext.define("App.view.classic.system.redis.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemredisSearchForm',
    _mainConfs: App.view.classic.system.redis.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"keyRedis",maxLength:500}
                ,{name:"valueJson",maxLength:6000}
                ,"sr"
            ]
        ]
    }
});
