Ext.define("App.view.classic.system.redis.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicsystemredisToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'd',actionUrl:'sysRedisCtl/delById'}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
