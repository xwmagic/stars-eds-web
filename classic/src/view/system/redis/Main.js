Ext.define("App.view.classic.system.redis.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemredisMain',
    requires: [ 
        "App.view.classic.system.redis.Form",
        "App.view.classic.system.redis.Grid",
        "App.view.classic.system.redis.SearchForm",
        "App.view.classic.system.redis.Toolbar",
        "App.view.classic.system.redis.ViewModel",
        "App.view.classic.system.redis.Window",
        "App.view.classic.system.redis.WindowAdd",
        "App.view.classic.system.redis.WindowEdit"
    ],
    viewModel: 'classicsystemredisViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemredisSearchForm',hidden: false},
        {xtype: 'classicsystemredisToolbar',hidden: false},
        {xtype: 'classicsystemredisGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemredisToolbar');
            var form = th.down('classicsystemredisSearchForm');
            var grid = th.down('classicsystemredisGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
