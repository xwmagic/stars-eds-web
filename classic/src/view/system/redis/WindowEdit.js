Ext.define("App.view.classic.system.redis.WindowEdit", { 
    extend: "App.view.classic.system.redis.Window",
    alias: "widget.classicsystemredisWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'sysRedisCtl/update'}, 'close']
});
