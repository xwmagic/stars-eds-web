Ext.define("App.view.classic.system.redis.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemredisViewModel',
    data: {
        pageName: 'classicsystemredisMain',
        add: true,
        update: true,
        del: true
    }
});
