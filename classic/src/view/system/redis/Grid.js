Ext.define("App.view.classic.system.redis.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicsystemredisGrid',
    _mainConfs: App.view.classic.system.redis.Datas.Main,
    url:'sysRedisCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"keyRedis",flex:1,align:'left'},
            {name:"valueJson",flex:1,align:'left'},
            {name:"createDate",width:190}
        ]
    }
});
