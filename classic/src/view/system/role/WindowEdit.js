Ext.define("App.classic.view.system.role.WindowEdit", {
    extend: "App.classic.view.system.role.Window",
    alias: "widget.viewsystemroleWindowEdit",
    initComponent: function () {
        this.items = [
            {name: 'viewsystemroleForm', xtype: 'viewsystemroleForm', isParam: true, autoSetSelect: true},
            {
                name: 'usersGrid', xtype: 'viewsystemroleUsersGrid',
                isParam: true, autoSetSelect: true, title: '关联用户',
                autoLoad: true,defaultParams:{roleId:this._record.id}
            }
        ];
        this.callParent();
    },
    buttons: [{name: 'submit', actionUrl: 'sysRoleCtl/update'}, 'close']
});
