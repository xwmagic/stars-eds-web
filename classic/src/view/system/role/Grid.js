Ext.define("App.classic.view.system.role.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'viewsystemroleGrid',
    _mainConfs: App.classic.view.system.role.Datas.Main,
    url:'sysRoleCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"roleName",width:130},
            {name:"roleTypeName",flex:1},
            {name:"appName",flex:1},
            {name:"companyName",flex:1},
            {name:"orgName",flex:1}
        ]
    }
});
