Ext.define("App.classic.view.system.role.Form", { 
    extend: "App.view.common.Form",
    xtype: 'viewsystemroleForm',
    _mainConfs: App.classic.view.system.role.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
            roleName:{allowBlank:false},
            roleType:{allowBlank:false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true}
            ],
            [
                {name:"roleName",maxLength:100}
            ],
            [
                {name:"roleType",maxLength:10}
            ]
        ]
    }
});
