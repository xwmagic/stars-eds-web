Ext.define("App.classic.view.system.role.UsersGrid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'viewsystemroleUsersGrid',
    url:'userCtl/pageList',
    _mainConfs: App.classic.view.system.role.Datas.User,
    paramName: {//编辑列表获取参数的参数名配置。提交时将会按照配置的参数名提交
        add: 'addJson', //列表新增的数据
        update: 'updateJson',//列表编辑的数据
        del: 'delJson'//列表删除的数据
    },
    tools:[
        'plus', 'minus'
    ],
    _autoLoad: false,
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'userName',flex:1},
            {name: 'userRealName',flex:1},
            {name: 'companyName'},
            {name: 'orgName'}
        ]
    },
    _addRecords: function () {
        var me = this;
        var win = Ext.create({
            xtype: 'viewsystemuserUserSelectWindow',
            excludeDatas:me.getDatas(),
            _targetObject:me
        });
        win.show();
    },
    getChangeValues: function(){
        var store = this.getStore();
        var values = [], value = {},newDatas,
            removeDatas, removes = [],
            i;

        newDatas = store.getNewRecords();
        removeDatas = store.getRemovedRecords();
        if(newDatas){
            for(i=0; i<newDatas.length; i++){
                values.push(newDatas[i].data.id);
            }
        }

        if(removeDatas){
            for(i=0; i<removeDatas.length; i++){
                if(removeDatas[i].data.id){
                    removes.push(removeDatas[i].data.id);
                }
            }
        }

        if(Ext.isObject(this.paramName)){
            if(this.paramName.add && !Ext.isEmpty(values)){
                value[this.paramName.add] = values;
            }
            if(this.paramName.del && !Ext.isEmpty(removes)){
                value[this.paramName.del] = removes;
            }

        }

        return value;
    }
});
