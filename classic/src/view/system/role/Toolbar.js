Ext.define("App.classic.view.system.role.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'viewsystemroleToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'viewsystemroleWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'viewsystemroleWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'sysRoleCtl/delById'},//处理方法_delete，actionUrl配置删除的请求地址,
            {name: 'export',actionUrl:'sysRoleCtl/exportList'},
            {name: 'u',actionObject:'sysAuthRoleWindow',text:'分配权限',iconCls:'x-fa fa-sitemap'}//处理方法_update
        ]
    }
});
