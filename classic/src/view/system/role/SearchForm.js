Ext.define("App.classic.view.system.role.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'viewsystemroleSearchForm',
    _mainConfs: App.classic.view.system.role.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"roleName",maxLength:100},{name:"roleType",maxLength:10},{name:"orgName",xtype:"classicsystemorgPicker",width:300}
                ,"sr"
            ]
        ]
    }
});
