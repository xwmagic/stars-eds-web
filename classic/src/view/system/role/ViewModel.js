Ext.define("App.classic.view.system.role.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.viewsystemroleViewModel',
    data: {
        pageName: 'viewsystemroleMain',
        add: true,
        update: true,
        del: true
    }
});
