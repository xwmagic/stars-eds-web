Ext.define("App.classic.view.system.role.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'viewsystemrolePicker',
    gridClassName:'App.classic.view.system.role.GridMini',
    displayField: 'roleName',
    valueField:'id'
});
