Ext.define("App.classic.view.system.role.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'viewsystemroleGridMini',
    _mainConfs: App.classic.view.system.role.Datas.Main,
    url:'sysRoleCtl/pageList',//tree的查询地址
    filterName: 'roleName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"roleName",flex:1},
            {name:"roleType",flex:1},
            {name:"appName",flex:1},
            {name:"companyName",flex:1},
            {name:"orgName",flex:1}
        ]
    }
});
