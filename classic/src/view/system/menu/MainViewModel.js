Ext.define("App.classic.view.system.menu.MainViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.viewsystemmenuMainViewModel',
    data: {
        pageName: 'Main',
        'add': true,
        'addChild': true,
        'update': true,
        'save': true,
        'addBaseBtn': false,
        'del': true
    }
});
