Ext.define("App.classic.view.system.menu.Datas", {
    statics: {
        Main:[
            {name: "id", type: "string", text: "主键ID"}
            ,{name:"perName", type:"string", text:"菜单名称"}
            ,{name:"title", type:"string", text:"菜单名称"}
            ,{name:"perType", type:"string", text:"菜单类型"}
            ,{name:"perIcon", type:"string", text:"菜单图标"}
            ,{name:"perRoutingUrl", type:"string", text:"前端跳转路由"}
            ,{name:"perUrl", type:"string", text:"访问路径"}
            ,{name:"perBtnValid", type:"string", text:"按钮权限标识"}
            ,{name:"perEnabled", type:"string", text:"是否启用"}
            ,{name:"layer", type:"string", text:"层级"}
            ,{name:"sortNum", type:"string", text:"排序号"}
            ,{name:"pid", type:"string", text:"父id"}
            ,{name:"iconCls", type:"string", text:"图标"}
        ]
    }
});