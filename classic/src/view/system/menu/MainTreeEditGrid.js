Ext.define("App.classic.view.system.menu.MainTreeEditGrid", {
    extend: "App.view.common.EditTreeGridDrag",
    xtype: 'systemmenuMainTreeEditGrid',
    _mainConfs: App.classic.view.system.menu.Datas.Main,
    url: 'auth/getAllMenus',//grid的查询地址
    defaultDatas:{
        perType:'child'
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {
                name: 'perName', xtype: 'treecolumn', width: 250,
                renderer:function (v) {
                    if (!v) {
                        v = '';
                    }else{
                        v = Ext.Component.htmlEscape(v);
                    }
                    return v;
                },
                editor: {
                    xtype: 'textfield',
                    listeners: {
                        change: function (th, v) {
                            th.ownerCt.context.record.set('name', v);
                        }
                    }
                }
            },
            {name: 'perIcon',align:'center',width:150,editor:{xtype:'classicsystemiconsPickerTab'},renderer:function (v,re) {
                    if(!v){
                        return;
                    }
                    return '<i class="'+v+'" aria-hidden="true"></i>';
             }},
            {name: 'perType',valueToName:'commonMenuType',align:'center',editor: {
                xtype: 'combo',
                typeAhead: true,
                triggerAction: 'all',
                store: [
                    ['menu','模块'],
                    ['child','菜单'],
                    ['btn','按钮'],
                    ['url','URL']
                ]
            }},
            {name: 'perRoutingUrl',width:300,htmlEscape: true,editor:{xtype:'classicsystemcomponentsPicker'}},

            {name: 'perUrl',minWidth:150,flex:1,htmlEscape: true/*,editor:{xtype:'classicsystemurlsPicker',editable:true}*/},
            {name: 'perBtnValid',width:200},
            {name: 'perEnabled',valueToName:'booleanYNC',align:'center',editor: {
                xtype: 'combo',
                typeAhead: true,
                triggerAction: 'all',
                store: [
                    ['1','是'],
                    ['0','否']
                ]
            }}

        ]
    }
});
