Ext.define("App.classic.view.system.menu.Main", {
    extend: 'App.ux.BasePanel',
    xtype: 'viewsystemmenuMain',
    requires: [
        "App.classic.view.system.menu.MainForm",
        "App.classic.view.system.menu.MainGrid",
        "App.classic.view.system.menu.MainTreeEditGrid",
        "App.classic.view.system.menu.MainTreeGrid",
        "App.classic.view.system.menu.MainSearchForm",
        "App.classic.view.system.menu.MainToolbar",
        "App.classic.view.system.menu.MainEditToolbar",
        "App.classic.view.system.menu.MainViewModel",
        "App.classic.view.system.menu.MainWindow",
        "App.classic.view.system.menu.MainWindowAdd",
        "App.classic.view.system.menu.MainWindowEdit"
    ],
    viewModel: 'viewsystemmenuMainViewModel',
    layout: {type: 'vbox', align: 'stretch'},
    items: [
        {xtype: 'viewsystemmenuMainSearchForm'},
        {xtype: 'systemmenuMainEditToolbar'},
        {xtype: 'systemmenuMainTreeEditGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('systemmenuMainEditToolbar');
            var form = th.down('viewsystemmenuMainSearchForm');
            var grid = th.down('systemmenuMainTreeEditGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    },
    treeClick:function (data) {
        var grid = this.down('systemmenuMainTreeEditGrid');
        grid._search({
            pid:data.id
        });
    }
});
