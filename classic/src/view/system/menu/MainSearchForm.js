Ext.define("App.classic.view.system.menu.MainSearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'viewsystemmenuMainSearchForm',
    _mainConfs: App.classic.view.system.menu.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'perName'},
                {name: 'perType',xtype:'commonMenuType'},
                {name: 'perRoutingUrl'},
                'sr' //s代表查询按钮，r代表重置按钮
            ]
        ]
    }
});
