Ext.define("App.view.sys.AuthRoleReadContainer", {
    extend: "App.classic.view.system.auth.AuthContainer",
    alias: "widget.sysAuthRoleReadContainer",
    paramName:'roleId',
    _autoLoad:true,
    _addItems: function () {
        var me = this;
        this.items = [
            {
                name: 'hasAuth', flex: 1,forceNoCheckTree:true,
                url:'auth/getRolePermission',
                _autoLoad:this._autoLoad,
                defaultParams: me.getHasAuthParam()
            }
        ];
    },

    refreshAuth:function () {
        this.down('treepanel[name=hasAuth]').defaultParams = this.getHasAuthParam();
        this.down('treepanel[name=hasAuth]')._refresh();
    }
});
