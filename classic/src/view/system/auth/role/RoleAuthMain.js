Ext.define("App.classic.view.system.role.AuthMain", {
    extend: 'App.ux.BasePanel',
    xtype: 'viewsystemroleAuthMain',
    viewModel: 'viewsystemroleViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'viewsystemroleSearchForm',hidden: false},
        {
            xtype: 'container', flex: 1, layout: {type: 'hbox', align: 'stretch'},border:true,
            items: [
                {
                    xtype: 'container', flex: 2, layout: {type: 'vbox', align: 'stretch'},padding: '0 10 0 0',
                    items: [
                        {xtype: 'viewsystemroleAuthToolbar'},
                        {
                            xtype: 'viewsystemroleGrid', flex:1,border:true,
                            listeners: {
                                itemclick: function (th, record) {
                                    var auth = th.up('viewsystemroleAuthMain').down('sysAuthRoleReadContainer');
                                    auth.searchAuth(record.data);
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: 'sysAuthRoleReadContainer', border:true,flex: 1
                }
            ]
        }
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('viewsystemroleAuthToolbar');
            var form = th.down('viewsystemroleSearchForm');
            var grid = th.down('viewsystemroleGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
