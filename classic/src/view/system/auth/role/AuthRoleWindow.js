Ext.define("App.view.sys.role.AuthRoleWindow", {
    extend: "App.classic.view.system.auth.AuthWindow",
    alias: "widget.sysAuthRoleWindow",
    searchUrl:'auth/getRolePermission',
    searchNotUrl:'auth/getAllMenus',
    addUrl:'permissionRelation/add',
    removeUrl:'permissionRelation/delById',
    paramName:'roleId'
});
