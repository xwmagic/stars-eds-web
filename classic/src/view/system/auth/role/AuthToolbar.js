Ext.define("App.classic.view.system.role.AuthToolbar", {
    extend: "App.view.common.Toolbar",
    xtype: 'viewsystemroleAuthToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'setAuth',actionObject:'sysAuthRoleWindow',text:'分配权限',handler:function (btn) {
                this.up('commontoolbar')._update(btn);
            }}//处理方法_update
        ]
    }
});
