Ext.define("App.view.sys.org.AuthOrgWindowAdd", {
    extend: "App.view.sys.org.AuthOrgWindow",
    alias: "widget.sysAuthOrgWindowAdd",
    editFlag:'add',
    buttons:[{name:'submit',text:'确定添加'},'close'],
    _onSubmit: function(btn){
        btn.up('window').down('sysAuthContainer').addAuth();
    }
});
