Ext.define("App.classic.view.system.auth.OrgAuthMain", {
    extend: 'App.ux.BasePanel',
    xtype: 'viewsystemauthOrgAuthMain',
    viewModel: 'viewsystemuserMainViewModel',
    layout: {type: 'vbox', align: 'stretch'},
    items: [
        {xtype: 'classicsystemorgSearchForm'},
        {
            xtype: 'container', flex: 1, layout: {type: 'hbox', align: 'stretch'}
            , border: true,
            items: [
                {
                    xtype: 'container', flex: 2, layout: {type: 'vbox', align: 'stretch'},padding: '0 10 0 0',
                    items: [
                        {xtype: 'classicsystemorgAuthToolbar'},
                        {
                            xtype: 'classicsystemorgAuthTreeGrid', flex: 1,border: true,
                            listeners: {
                                itemclick: function (th, record) {
                                    var auth = th.up('viewsystemauthOrgAuthMain').down('sysAuthOrgReadContainer');
                                    auth.searchAuth(record.data);
                                }
                            }
                        }
                    ]
                },

                {
                    xtype: 'sysAuthOrgReadContainer', flex: 1, border: true
                }
            ]
        }
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemorgAuthToolbar');
            var form = th.down('classicsystemorgSearchForm');
            var grid = th.down('classicsystemorgAuthTreeGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
            grid._commonToolbar = toolbar;
        }
    }
});
