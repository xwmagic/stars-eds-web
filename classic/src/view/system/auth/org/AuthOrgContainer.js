Ext.define("App.view.sys.AuthOrgContainer", {
    extend: "App.classic.view.system.auth.AuthContainer",
    alias: "widget.sysAuthOrgContainer",
    paramName: 'orgId',
    searchUrl:'auth/getOrgPermission',
    searchNotUrl:'',
    addUrl: 'systemPerOrgCtl/add',
    removeUrl: 'systemPerOrgCtl/update',
    getParam: function (ids, me) {
        var param = {
            ids: ids.toString(),
            orgIds:me._record.id,
            orgNames:me._record.orgName
        };
        return param;
    }
});
