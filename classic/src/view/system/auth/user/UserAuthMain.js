Ext.define("App.classic.view.system.auth.UserAuthMain", {
    extend: 'App.ux.BasePanel',
    xtype: 'viewsystemauthUserAuthMain',
    viewModel: 'viewsystemuserMainViewModel',
    layout: {type: 'vbox', align: 'stretch'},
    items: [
        {xtype: 'viewsystemuserMainSearchForm'},
        {
            xtype: 'container', flex: 1, layout: {type: 'hbox', align: 'stretch'}
            , border: true,
            items: [
                {
                    xtype: 'container', flex: 1, layout: {type: 'vbox', align: 'stretch'},padding: '0 10 0 0',
                    items: [
                        {xtype: 'viewsystemuserAuthToolbar'},
                        {
                            xtype: 'viewsystemuserMainGrid', flex: 1,border: true,
                            listeners: {
                                itemclick: function (th, record) {
                                    var auth = th.up('viewsystemauthUserAuthMain').down('sysuserAuthTabPanel');
                                    auth.searchAuth(record.data);
                                }
                            }
                        }
                    ]
                },

                {
                    xtype: 'sysuserAuthTabPanel', flex: 1, border: true
                }
            ]
        }
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('viewsystemuserAuthToolbar');
            var form = th.down('viewsystemuserMainSearchForm');
            var grid = th.down('viewsystemuserMainGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
            grid._commonToolbar = toolbar;
        }
    }
});
