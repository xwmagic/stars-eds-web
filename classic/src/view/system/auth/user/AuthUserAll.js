Ext.define("App.view.sys.user.AuthUserAll", {
    extend: "App.classic.view.system.auth.AuthContainer",
    alias: "widget.sysuserAuthUserAll",
    paramName:'userId',
    _addItems: function () {
        var me = this;
        this.items = [
            {
                name: 'hasAuth', flex: 1,forceNoCheckTree:true,
                url:'auth/getAllTreeMenusList',
                defaultParams: me.getHasAuthParam()
            }
        ];
    },

    refreshAuth:function () {
        this.down('treepanel[name=hasAuth]').defaultParams = this.getHasAuthParam();
        this.down('treepanel[name=hasAuth]')._refresh();
    }

});