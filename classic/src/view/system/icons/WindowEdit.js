Ext.define("App.view.classic.system.icons.WindowEdit", { 
    extend: "App.view.classic.system.icons.Window",
    alias: "widget.classicsystemiconsWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'sysIconsCtl/update'}, 'close']
});
