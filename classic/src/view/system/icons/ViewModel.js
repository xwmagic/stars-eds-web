Ext.define("App.view.classic.system.icons.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemiconsViewModel',
    data: {
        pageName: 'classicsystemiconsMain',
        add: true,
        update: true,
        del: true
    }
});
