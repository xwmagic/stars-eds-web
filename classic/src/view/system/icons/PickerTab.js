Ext.define("App.view.classic.system.icons.PickerTab", {
    extend: "App.view.common.GridPicker",
    xtype: 'classicsystemiconsPickerTab',
    gridClassName:'App.view.classic.system.icons.GridMiniTab',
    displayField: 'iconName',
    valueField:'id',
    columnsNumber:5,//列数
    createPicker: function () {
        if(!Ext.platformTags.desktop){
            this.columnsNumber = 2;
        }
        var me = this,
            picker = me.createComponent();
        //添加cellClick绑定
        picker.addCellClick(me);
        return picker;
    },
    onCellClick: function (view, cell, index, record, e, eOpts) {
        var me = this;
        me.setValue(record.data[index + 1]);
        me.setKeyValue(record.data[index + 1]);
        me.setRawValue(record.data[index + 1]);
        me.getPicker().hide();
        me.inputEl.focus();
    },
    setValue: function (value) {
        var me = this,
            isContinue = true;

        if (me.value != value) {
            isContinue = me.fireEvent('beforechange', me, value, me.value);
        }
        if (!isContinue) {
            return false;
        }

        me.fireEvent('change', me, value, me.value);
        me.value = value;

        if (value === undefined) {
            return false;
        }
        return me;
    },
    createComponent: function () {
        var picker, me = this;
        if (this.gridClassName) {
            picker = Ext.create(this.gridClassName, {
                floating: true,
                columnsNumber:this.columnsNumber,
                width: this.pickerWidth,
                height: this.pickerHeight
            });
        }


        return picker;
    },
    load: function (params) {
        /*var store
            , proxy
        ;

        store = this.getPicker().getStore();

        setTimeout(function () {
            if (params) {
                proxy = store.getProxy();
                proxy.extraParams = params;
            }
            store.load();
        }, 100);*/
    }
});
