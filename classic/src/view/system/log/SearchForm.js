Ext.define("App.view.classic.system.log.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemlogSearchForm',
    _mainConfs: App.view.classic.system.log.Datas.main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"appName"},
                {name:"args"},
                {name:"returnValue"},
                {name:"describeLog"},
                "sr",'ec'
            ],
            [
                {name:"createDate"},
                {name:"createName"},
                {name:"operationType",xtype:'sysLogType'}
            ]
        ]
    }
});
