/**
 * 时间类型
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.classic.system.log.LogType', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.sysLogType',
    statics:{
	    Datas:{
			UNKNOWN:{text:'UNKNOWN',width:80,color:'#c6b81c'},
			DELETE:{text:'删除',width:80,color:'#d63d16'},
			SELECT:{text:'查询',width:80,color:'#22d687'},
			UPDATE:{text:'更新',width:80,color:'#1faed6'},
			INSERT:{text:'添加',width:80,color:'#1278d6'},
			EXCEPTION:{text:'异常',width:80,color:'#d62b13'},
			OTHER:{text:'其他',width:80,color:'#7c1fd6'},
			LOGIN:{text:'LOGIN',width:80,color:'#26d61e'}
        }
    }
});

