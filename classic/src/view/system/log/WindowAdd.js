Ext.define("App.view.classic.system.log.WindowAdd", { 
    extend: "App.view.classic.system.log.Window",
    alias: "widget.classicsystemlogWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'operationLogCtl/add'}, 'close']
});
