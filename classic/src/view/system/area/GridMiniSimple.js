Ext.define("App.view.classic.system.area.GridMiniSimple", {
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicsystemareaGridMiniSimple',
    _mainConfs: App.view.classic.system.area.Datas.Main,
    url:'sysAreaCtl/pageList',//tree的查询地址
    filterName: 'wholeName',
    columnsDefault:{autoLinefeed:true},
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"wholeName",width:150},
            {name:"areaName"},
            {name:"zipCode"}
        ]
    }
});
