Ext.define("App.view.classic.system.area.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemareaSearchForm',
    _mainConfs: App.view.classic.system.area.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"wholeName",maxLength:255}
                ,{name:"simplePy",maxLength:100}
                ,{name:"areaName",maxLength:100}
                ,"sr","ec"
            ],
            [
                {name:"simpleName",maxLength:100}
                ,{name:"pinYin",maxLength:100}
                ,{name:"prePinYin",maxLength:100}
            ],
            [
                {name:"areaCode",maxLength:20}
                ,{name:"zipCode",maxLength:20}
                ,{name:"level",maxLength:1}
            ]
        ]
    }
});
