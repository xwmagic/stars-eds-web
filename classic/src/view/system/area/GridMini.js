Ext.define("App.view.classic.system.area.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicsystemareaGridMini',
    _mainConfs: App.view.classic.system.area.Datas.Main,
    url:'sysAreaCtl/pageList',//tree的查询地址
    filterName: 'wholeName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"wholeName"},
            {name:"areaName"},
            {name:"simpleName"},
            {name:"pinYin"},
            {name:"simplePy"},
            {name:"prePinYin"},
            {name:"areaCode"},
            {name:"zipCode"},
            {name:"lat"},
            {name:"lon"},
            {name:"level"},
            {name:"remark"}
        ]
    }
});
