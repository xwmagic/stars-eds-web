Ext.define("App.view.classic.system.area.SelectWindow", { 
    extend: "App.classic.view.common.GridSelectWindow",
    xtype: 'classicsystemareaSelectWindow',
    formXtype: 'classicsystemareaSearchForm',
    gridXtype: 'classicsystemareaGridSelect'
});
