Ext.define("App.view.classic.system.org.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemorgSearchForm',
    _mainConfs: App.view.classic.system.org.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"companyId"}
                ,{name:"orgName",maxLength:100}
                ,"sr","ec"
            ],
            [
                {name:"orgType",maxLength:50}
                ,{name:"orgCode",maxLength:20}
                ,{name:"orgEn"}
            ]
        ]
    }
});
