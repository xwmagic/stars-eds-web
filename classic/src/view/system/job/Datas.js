Ext.define("App.view.classic.system.job.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'id'}
            ,{ name:'name', type:'string', text:'任务名称'}
            ,{ name:'groupJob', type:'string', text:'任务分组'}
            ,{ name:'cron', type:'string', text:'任务表达式'}
            ,{ name:'clazzPath', type:'string', text:'执行类名'}
            ,{ name:'jobMethod', type:'string', text:'执行方法'}
            ,{ name:'jobDesc', type:'string', text:'job描述信息'}
            ,{ name:'jarPath', type:'string', text:'job的jar路径'}
            ,{ name:'statusJob', type:'string', text:'任务状态'}
            ,{ name:'jobTaskType', type:'string', text:'任务调度类型'}
            ,{ name:'apiUrl', type:'string', text:'接口地址'}
            ,{ name:'params', type:'string', text:'参数'}
            ,{ name:'jobType', type:'string', text:'任务类型'}
            ,{ name:'triggerName', type:'string', text:'触发器名字'}
            ,{ name:'triggerGroup', type:'string', text:'触发器分组'}
            ,{ name:'isNowRun', type:'string', text:'是否运行'}
            ,{ name:'statusDate', type:'date', text:'生效日期'}
            ,{ name:'endDate', type:'date', text:'失效日期'}
        ]
    }
});
