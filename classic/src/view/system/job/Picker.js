Ext.define("App.view.classic.system.job.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicsystemjobPicker',
    gridClassName:'App.view.classic.system.job.GridMini',
    displayField: 'name',
    valueField:'id'
});
