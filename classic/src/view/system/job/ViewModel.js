Ext.define("App.view.classic.system.job.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicsystemjobViewModel',
    data: {
        pageName: 'classicsystemjobMain',
        add: true,
        update: true,
        del: true
    }
});
