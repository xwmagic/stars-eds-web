Ext.define("App.view.classic.system.job.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemjobMain',
    requires: [ 
        "App.view.classic.system.job.Form",
        "App.view.classic.system.job.Grid",
        "App.view.classic.system.job.SearchForm",
        "App.view.classic.system.job.Toolbar",
        "App.view.classic.system.job.ViewModel",
        "App.view.classic.system.job.Window",
        "App.view.classic.system.job.WindowAdd",
        "App.view.classic.system.job.WindowEdit"
    ],
    viewModel: 'classicsystemjobViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemjobSearchForm',hidden: false},
        {xtype: 'classicsystemjobToolbar',hidden: false},
        {xtype: 'classicsystemjobGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemjobToolbar');
            var form = th.down('classicsystemjobSearchForm');
            var grid = th.down('classicsystemjobGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
