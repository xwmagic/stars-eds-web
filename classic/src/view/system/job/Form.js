Ext.define("App.view.classic.system.job.Form", {
    extend: "App.view.common.Form",
    xtype: 'classicsystemjobForm',
    _mainConfs: App.view.classic.system.job.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        name: {allowBlank: false}
        , groupJob: {allowBlank: false}
        , cron: {allowBlank: false}
        , clazzPath: {allowBlank: false}
        , jobMethod: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'id', hidden: true}
                , {name: "name"}
                , {name: "groupJob"}
                , {name: "cron"}
            ],
            [
                {name: "clazzPath"}
                , {name: "jobMethod"}
                , {name: "jobTaskType"}
            ],
            [
                {name: "params"}
                , {name: "jobType"}
                , {name: "triggerName"}
            ], [
                {name: "jobDesc"}
            ]
        ]
    }
});
