Ext.define("App.view.classic.system.job.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicsystemjobGridMini',
    _mainConfs: App.view.classic.system.job.Datas.Main,
    url:'MyScheduleJobCtl/pageList',//tree的查询地址
    filterName: 'name',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name",hyperlinkMode:true,hyperlinkWindow:"classicsystemjobWindowView"},
            {name:"groupJob"},
            {name:"cron"},
            {name:"clazzPath"},
            {name:"jobDesc"},
            {name:"jarPath"},
            {name:"statusJob"},
            {name:"jobTaskType"},
            {name:"apiUrl"},
            {name:"params"},
            {name:"jobType"},
            {name:"triggerName"},
            {name:"triggerGroup"},
            {name:"isNowRun"},
            {name:"statusDate"},
            {name:"endDate"}
        ]
    }
});
