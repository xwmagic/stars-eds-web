Ext.define("App.view.classic.system.job.WindowAdd", { 
    extend: "App.view.classic.system.job.Window",
    alias: "widget.classicsystemjobWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'myScheduleJobCtl/add'}, 'close']
});
