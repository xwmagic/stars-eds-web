Ext.define("App.view.classic.system.job.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicsystemjobGrid',
    _mainConfs: App.view.classic.system.job.Datas.Main,
    url:'myScheduleJobCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name" ,width:150},
            {name:"groupJob",width:150},
            {name:"cron",width:150},
            {name:"clazzPath",width:250},
            {name:"jobMethod",width:250},
            {name:"statusJob"},
            {name:"jobType"},
            {name:"isNowRun",valueToName:'booleanYNC'},
            {name:"jobDesc"},
            {name:"jobTaskType"},
            {name:"statusDate"},
            {name:"endDate"}
        ]
    }
});
