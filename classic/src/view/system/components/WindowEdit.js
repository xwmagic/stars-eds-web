Ext.define("App.view.classic.system.components.WindowEdit", { 
    extend: "App.view.classic.system.components.Window",
    alias: "widget.classicsystemcomponentsWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'sysJsItemCtl/update'}, 'close']
});
