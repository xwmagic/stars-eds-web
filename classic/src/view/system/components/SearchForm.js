Ext.define("App.view.classic.system.components.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemcomponentsSearchForm',
    _mainConfs: App.view.classic.system.components.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"name",maxLength:100},
                {name:"className",maxLength:100},
                {name:"path",maxLength:100}
                ,"sr"
            ]
        ]
    }
});
