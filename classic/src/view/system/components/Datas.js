Ext.define("App.view.classic.system.components.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'id'}
            ,{ name:'name', type:'string', text:'组件名称'}
            ,{ name:'className', type:'string', text:'组件类名'}
            ,{ name:'path', type:'string', text:'组件路径'}
            ,{ name:'remarks', type:'string', text:'组件备注'}
        ]
    }
});
