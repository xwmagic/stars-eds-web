Ext.define("App.view.classic.system.components.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemcomponentsMain',
    requires: [ 
        "App.view.classic.system.components.Form",
        "App.view.classic.system.components.Grid",
        "App.view.classic.system.components.SearchForm",
        "App.view.classic.system.components.Toolbar",
        "App.view.classic.system.components.ViewModel",
        "App.view.classic.system.components.Window",
        "App.view.classic.system.components.WindowAdd",
        "App.view.classic.system.components.WindowEdit"
    ],
    viewModel: 'classicsystemcomponentsViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemcomponentsSearchForm',hidden: false},
        {xtype: 'classicsystemcomponentsToolbar',hidden: false},
        {xtype: 'classicsystemcomponentsGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemcomponentsToolbar');
            var form = th.down('classicsystemcomponentsSearchForm');
            var grid = th.down('classicsystemcomponentsGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
