Ext.define("App.view.classic.system.components.WindowAdd", { 
    extend: "App.view.classic.system.components.Window",
    alias: "widget.classicsystemcomponentsWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'sysJsItemCtl/add'}, 'close']
});
