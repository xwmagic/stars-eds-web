Ext.define("App.view.classic.system.components.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicsystemcomponentsForm',
    _mainConfs: App.view.classic.system.components.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"path",maxLength:600,value:'E:\\starsWebExt\\stars-web-ext\\classic\\src\\view'}
            ]
        ]
    }
});
