Ext.define("App.classic.view.system.user.UserSelectWindow", {
    extend: "App.view.common.Window",
    alias: "widget.viewsystemuserUserSelectWindow",
    layout: {type: 'vbox', align: 'stretch'},
    width: 900,
    height: 600,
    excludeDatas: [],
    _targetObject: '',
    initComponent: function () {
        this.items = [
            {xtype: 'viewsystemuserMainSearchForm'},
            {
                xtype: 'viewsystemuserMainGridMini',
                border:true,
                excludeDatas: this.excludeDatas,
                flex: 1
            }
        ];
        this.callParent();
    },
    listeners: {
        beforerender: function (th) {
            var form = th.down('viewsystemuserMainSearchForm');
            var grid = th.down('viewsystemuserMainGrid');
            form._targetObject = grid;//给查询框关联操作对象
        }
    },
    buttons: [{
        text: '确定', handler: function () {
            var win = this.up('window');
            var winGrid = win.down('grid');
            var selections = winGrid.getSelectionModel().getSelection();
            var datas = [], temp;
            for (var i = 0; i < selections.length; i++) {
                temp = Ext.clone(selections[i].data);
                temp._add = true;
                datas.push(temp);
            }
            win._targetObject.setValue(datas);
            win.close();
        }
    }, {
        text: '取消',
        handler: function () {
            this.up('window').close();
        }
    }]
});
