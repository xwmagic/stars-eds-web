Ext.define("App.classic.view.system.user.MainForm", { 
    extend: "App.view.common.Form",
    xtype: 'viewsystemuserMainForm',
    _mainConfs: App.classic.view.system.user.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        userName: {allowBlank: false},
        userRealName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {
                    xtype: 'label',
                    cls: 'lock-screen-top-label',
                    name: 'info',
                    flex:1,
                    text: '',
                    flag: false
                }
            ],
            [
                {name: 'id',hidden:true},
                {name: 'userName'},
                {name: 'userRealName'}
            ],
            [
                {
                    xtype: 'lrpassword',
                    enableKeyEvents: true,
                    emptyText: '密码',
                    name: 'userPassword',
                    listeners: {
                        keyup: function(obj){
                            var newValue = obj.getValue();
                            var ad = obj.up('form');
                            var pw = ad.down('lrpassword[name=userPasswordConfirm]');
                            var info = ad.down('label[name=info]');
                            var isSame = obj.onTheSame(obj,newValue,pw,info);
                            if(!isSame){

                            }
                        }
                    }
                },
                {
                    xtype: 'lrpassword',
                    emptyText: '密码确认',
                    enableKeyEvents: true,
                    name: 'userPasswordConfirm',
                    listeners: {
                        keyup: function(obj){
                            var newValue = obj.getValue();
                            var ad = obj.up('form');
                            var pw = ad.down('lrpassword[name=userPassword]');
                            var info = ad.down('label[name=info]');
                            var isSame = obj.onTheSame(obj,newValue,pw,info);
                            if(!isSame){

                            }
                        }
                    }
                }
            ],
            [
                {name: 'userEnabled',xtype:'booleanYNC',comType: 'radiogroup',value:0},
                {name:'isLock',xtype:'booleanYNC',comType: 'radiogroup',value:0}
            ],
            [
                {name: 'userImage'},
                {name: 'userEmail'}
            ],
            [
                {name: 'userPhone'}
            ],
            [
                {name:'roleIds',xtype:'viewsystemroleTagField',flex:1}
            ]
        ]
    },
    getValues:function () {
        var values = this.callParent();
        values.userPassword = hex_md5(values.userPassword);
        delete values.userPasswordConfirm;
        return values;
    }
});
