Ext.define("App.classic.view.system.user.MainFormEdit", {
    extend: "App.view.common.Form",
    xtype: 'viewsystemuserMainFormEdit',
    _mainConfs: App.classic.view.system.user.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'id',hidden:true},
                {name: 'userName',readOnly:true},
                {name: 'userRealName'}
            ],
            [
                {name: 'userEnabled',xtype:'booleanYNC',comType: 'radiogroup'},
                {name:'isLock',xtype:'booleanYNC',comType: 'radiogroup',value:0}
            ],
            [
                {name: 'userImage'},
                {name: 'userEmail'}
            ],
            [
                {name: 'userPhone'}
            ],
            [
                {name:'roleIds',xtype:'viewsystemroleTagField',flex:1}
            ]
        ]
    }
});
