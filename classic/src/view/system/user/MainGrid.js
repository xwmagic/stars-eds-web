Ext.define("App.classic.view.system.user.MainGrid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'viewsystemuserMainGrid',
    _mainConfs: App.classic.view.system.user.Datas.Main,
    url:'userCtl/userQueryByList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'userName',width:150,hyperlinkMode:true,hyperlinkWindow:'viewsystemuserMainWindowView'},
            {name: 'userRealName',width:150},
            {name: 'roleName',valueToName:'UserRoles',width:300,align:'left'},
            {name: 'userAddTime',width:150},
            {name: 'userPhone',width:150},
            {name: 'userImage'},
            {name: 'userEmail'},
            {name: 'companyName'},
            {name: 'orgName'},
            {name: 'userEnabled',valueToName:'booleanYNC'}
        ]
    }
});
