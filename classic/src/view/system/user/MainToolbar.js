Ext.define("App.classic.view.system.user.MainToolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'viewsystemuserMainToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'viewsystemuserMainWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'viewsystemuserMainWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'userCtl/userDelete'},//处理方法_delete，actionUrl配置删除的请求地址
            {name: 'setAuth',actionObject:'sysAuthUserWindow',text:'分配权限',bind:{hidden:'{setAuth}'},iconCls:'x-fa fa-sitemap',handler:function (btn) {
                this.up('commontoolbar')._update(btn);
            }}//处理方法_update
        ]
    }
});
