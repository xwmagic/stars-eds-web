Ext.define("App.classic.view.system.user.MainViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.viewsystemuserMainViewModel',
    data: {
        pageName: 'Main',
        'add': true,
        'del': true,
        'setAuth': true,
        'update': true
    }
});
