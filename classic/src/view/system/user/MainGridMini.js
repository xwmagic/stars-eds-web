Ext.define("App.classic.view.system.user.MainGridMini", {
    extend: "App.classic.view.system.user.MainGrid",
    xtype: 'viewsystemuserMainGridMini',
    pageSize:50,
    isExcludeDatas:true,//开启数据排除功能
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'userName',flex:1},
            {name: 'userRealName',flex:1},
            {name: 'companyName',flex:1},
            {name: 'orgName',flex:1},
            {name: 'userEnabled',valueToName:'booleanYNC'}
        ]
    }
});
