Ext.define("App.view.classic.system.dict.TreeEditGrid", { 
    extend: "App.view.common.EditTreeGridDrag",
    xtype: 'classicsystemdictTreeEditGrid',
    _mainConfs: App.view.classic.system.dict.Datas.Main,
    url:'sysDictCtl/findTree',//tree的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"id",hidden:true},
            {name:"name",xtype:"treecolumn",flex:1},
            {name:"code",flex:1}
        ]
    }
});
