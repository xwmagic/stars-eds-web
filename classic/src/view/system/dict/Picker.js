Ext.define("App.view.classic.system.dict.Picker", { 
    extend: "App.view.common.TreeMuiltiPicker",
    xtype: 'classicsystemdictPicker',
    gridClassName:'App.view.classic.system.dict.TreeGridMini',
    checkModel:'single',//选择模式
    onlyLeaf:false,
    displayField: 'name',
    valueField: 'id'
});
