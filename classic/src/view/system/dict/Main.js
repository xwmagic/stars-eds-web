Ext.define("App.view.classic.system.dict.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicsystemdictMain',
    requires: [ 
        "App.view.classic.system.dict.TreeEditGrid",
        "App.view.classic.system.dict.SearchForm",
        "App.view.classic.system.dict.Toolbar",
        "App.view.classic.system.dict.TreeGridMini",
        "App.view.classic.system.dict.ViewModel",
        "App.view.classic.system.dict.Picker"
    ],
    viewModel: 'classicsystemdictViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicsystemdictSearchForm',hidden: false},
        {xtype: 'classicsystemdictToolbar',hidden: false},
        {xtype: 'classicsystemdictTreeEditGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicsystemdictToolbar');
            var form = th.down('classicsystemdictSearchForm');
            var grid = th.down('classicsystemdictTreeEditGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
