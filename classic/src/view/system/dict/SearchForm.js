Ext.define("App.view.classic.system.dict.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicsystemdictSearchForm',
    _mainConfs: App.view.classic.system.dict.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"name",maxLength:50}
                ,{name:"code",maxLength:50}
                ,'sr'
            ]
        ]
    }
});
