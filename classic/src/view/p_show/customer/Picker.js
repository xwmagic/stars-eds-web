Ext.define("App.view.classic.p_show.customer.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicp_showcustomerPicker',
    gridClassName:'App.view.classic.p_show.customer.GridMini',
    displayField: 'customerName',
    valueField:'id'
});
