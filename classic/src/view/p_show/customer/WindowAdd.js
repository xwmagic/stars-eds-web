Ext.define("App.view.classic.p_show.customer.WindowAdd", { 
    extend: "App.view.classic.p_show.customer.Window",
    alias: "widget.classicp_showcustomerWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'pShowCustomerCtl/add'}, 'close']
});
