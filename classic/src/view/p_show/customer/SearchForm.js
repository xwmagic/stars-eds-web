Ext.define("App.view.classic.p_show.customer.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicp_showcustomerSearchForm',
    _mainConfs: App.view.classic.p_show.customer.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"customerName",maxLength:100}
                ,{name:"customerType",maxLength:100}
                ,"sr","ec"
            ],
            [
                {name:"customerTel",maxLength:100}
                ,{name:"customerUser",maxLength:50}
            ],
            [
                {name:"customerSite",maxLength:500}
            ]
        ]
    }
});
