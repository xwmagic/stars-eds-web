Ext.define("App.view.classic.p_show.customer.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicp_showcustomerGridMini',
    _mainConfs: App.view.classic.p_show.customer.Datas.Main,
    url:'pShowCustomerCtl/pageList',//tree的查询地址
    filterName: 'customerName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"customerName",flex:1},
            {name:"customerType",flex:1},
            {name:"customerTel",flex:1},
            {name:"customerUser",flex:1},
            {name:"customerSite",flex:2,minWidth:100}
        ]
    }
});
