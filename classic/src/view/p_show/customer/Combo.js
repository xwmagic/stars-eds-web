Ext.define("App.view.classic.p_show.customer.Combo", { 
    extend: "App.view.BaseCombo",
    xtype: 'classicp_showcustomerCombo',
    dynamicDataUrl:'pShowCustomerCtl/pageList',//数据的查询地址
    valueField: 'customerName',
    displayField: 'customerName'
});
