Ext.define("App.view.classic.p_show.customer.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicp_showcustomerToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name:'c',tooltip:'新增',actionObject:'classicp_showcustomerWindowAdd'},
            {name:'u',tooltip:'编辑',actionObject:'classicp_showcustomerWindowEdit'},
            {name:'d',actionUrl:'pShowCustomerCtl/delById',tooltip:'删除'},
            {name:'export',actionUrl:'pShowCustomerCtl/exportList',tooltip:'导出Excel'},
            {name:'import',actionUrl:'pShowCustomerCtl/importExcel',tooltip:'导入Excel'}
        ]
    }
});
