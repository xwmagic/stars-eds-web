Ext.define("App.view.classic.p_show.customer.EditGrid", { 
    extend: "App.view.common.EditGrid",
    xtype: 'classicp_showcustomerEditGrid',
    _mainConfs: App.view.classic.p_show.customer.Datas.Main,
    url:'pShowCustomerCtl/pageList',//grid的查询地址
    selType: "checkboxmodel",
    editColIndex: 2,
    autoLoad: true,
    tools: ["multiEdit","plus","minus"],
    paramName: {//编辑列表获取参数的参数名配置。提交时将会按照配置的参数名提交
        add: "addJsonPShowCustomer",
        update: "updateJsonPShowCustomer",
        del: "delJsonPShowCustomer"
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"customerName",flex:1},
            {name:"customerType",flex:1},
            {name:"customerTel",flex:1},
            {name:"customerUser",flex:1},
            {name:"customerSite",flex:2,minWidth:100}
        ]
    }
});
