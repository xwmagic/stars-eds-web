Ext.define("App.view.classic.p_show.customer.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicp_showcustomerViewModel',
    data: {
        pageName: 'classicp_showcustomerMain',
        add: true,
        update: true,
        del: true
    }
});
