Ext.define("App.view.classic.p_show.customer.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'customerName', type:'string', text:'客户名称'}
            ,{ name:'customerType', type:'string', text:'客户分类'}
            ,{ name:'customerSite', type:'string', text:'客户地址'}
            ,{ name:'customerTel', type:'string', text:'客户电话'}
            ,{ name:'customerUser', type:'string', text:'联系人'}
        ]
    }
});
