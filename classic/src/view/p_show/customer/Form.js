Ext.define("App.view.classic.p_show.customer.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicp_showcustomerForm',
    _mainConfs: App.view.classic.p_show.customer.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        customerName:{allowBlank:false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true}
                ,{name:"customerName",maxLength:100}
                ,{name:"customerType",maxLength:100}
            ],
            [
                {name:"customerTel",maxLength:100}
                ,{name:"customerUser",maxLength:50}
            ],
            [
                {name:"customerSite",maxLength:500}
            ]
        ]
    }
});
