Ext.define("App.view.classic.p_show.customer.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicp_showcustomerGrid',
    openRowButtons:true,
    columnsDefault:{autoLinefeed:true},
    _mainConfs: App.view.classic.p_show.customer.Datas.Main,
    url:'pShowCustomerCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"customerName",flex:1,hyperlinkMode:true,hyperlinkWindow:"classicp_showcustomerWindowView"},
            {name:"customerType",flex:1},
            {name:"customerTel",flex:1},
            {name:"customerUser",flex:1},
            {name:"customerSite",flex:2,minWidth:100}
        ]
    }
});
