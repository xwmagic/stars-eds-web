Ext.define("App.view.classic.p_show.customer.WindowEdit", { 
    extend: "App.view.classic.p_show.customer.Window",
    alias: "widget.classicp_showcustomerWindowEdit",
    openValidation:true,
    isGetChangeValue:true,
    buttons: [{name: 'submit', actionUrl: 'pShowCustomerCtl/update'}, 'close']
});
