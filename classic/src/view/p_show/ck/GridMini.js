Ext.define("App.view.classic.p_show.ck.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicp_showckGridMini',
    _mainConfs: App.view.classic.p_show.ck.Datas.Main,
    url:'pShowCkCtl/pageList',//tree的查询地址
    filterName: 'name',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name",flex:1},
            {name:"code",flex:1},
            {name:"volumne",flex:1},
            {name:"orgName",flex:1},
            {name:"userName",flex:1,minWidth:100}
        ]
    }
});
