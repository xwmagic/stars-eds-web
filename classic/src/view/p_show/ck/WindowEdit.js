Ext.define("App.view.classic.p_show.ck.WindowEdit", { 
    extend: "App.view.classic.p_show.ck.Window",
    alias: "widget.classicp_showckWindowEdit",
    openValidation:true,
    isGetChangeValue:true,
    buttons: [{name: 'submit', actionUrl: 'pShowCkCtl/update'}, 'close']
});
