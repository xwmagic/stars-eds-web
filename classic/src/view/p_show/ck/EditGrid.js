Ext.define("App.view.classic.p_show.ck.EditGrid", { 
    extend: "App.view.common.EditGrid",
    xtype: 'classicp_showckEditGrid',
    _mainConfs: App.view.classic.p_show.ck.Datas.Main,
    url:'pShowCkCtl/pageList',//grid的查询地址
    selType: "checkboxmodel",
    editColIndex: 2,
    autoLoad: true,
    tools: ["multiEdit","plus","minus"],
    paramName: {//编辑列表获取参数的参数名配置。提交时将会按照配置的参数名提交
        add: "addJsonPShowCk",
        update: "updateJsonPShowCk",
        del: "delJsonPShowCk"
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name",flex:1},
            {name:"code",flex:1},
            {name:"volumne",flex:1},
            {name:"orgName",flex:1},
            {name:"userName",flex:1,minWidth:100}
        ]
    }
});
