Ext.define("App.view.classic.p_show.ck.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicp_showckPicker',
    gridClassName:'App.view.classic.p_show.ck.GridMini',
    displayField: 'name',
    valueField:'id'
});
