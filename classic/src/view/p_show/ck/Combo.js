Ext.define("App.view.classic.p_show.ck.Combo", { 
    extend: "App.view.BaseCombo",
    xtype: 'classicp_showckCombo',
    dynamicDataUrl:'pShowCkCtl/pageList',//数据的查询地址
    valueField: 'name',
    displayField: 'name'
});
