Ext.define("App.view.classic.p_show.ck.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicp_showckGrid',
    openRowButtons:true,
    columnsDefault:{autoLinefeed:true},
    _mainConfs: App.view.classic.p_show.ck.Datas.Main,
    url:'pShowCkCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name",flex:1,hyperlinkMode:true,hyperlinkWindow:"classicp_showckWindowView"},
            {name:"code",flex:1},
            {name:"volumne",flex:1},
            {name:"orgName",flex:1},
            {name:"userName",flex:1,minWidth:100}
        ]
    }
});
