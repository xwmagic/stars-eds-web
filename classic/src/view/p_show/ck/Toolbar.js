Ext.define("App.view.classic.p_show.ck.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicp_showckToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name:'c',tooltip:'新增',actionObject:'classicp_showckWindowAdd'},
            {name:'u',tooltip:'编辑',actionObject:'classicp_showckWindowEdit'},
            {name:'d',actionUrl:'pShowCkCtl/delById',tooltip:'删除'},
            {name:'export',actionUrl:'pShowCkCtl/exportList',tooltip:'导出Excel'},
            {name:'import',actionUrl:'pShowCkCtl/importExcel',tooltip:'导入Excel'}
        ]
    }
});
