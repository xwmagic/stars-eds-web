Ext.define("App.view.classic.p_show.ck.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'name', type:'string', text:'仓库名称'}
            ,{ name:'code', type:'string', text:'仓库编码'}
            ,{ name:'volumne', type:'int', text:'容量'}
            ,{ name:'orgId', type:'string', text:'所属组织'}
            ,{ name:'orgName', type:'string', text:'所属组织'}
            ,{ name:'orgCode', type:'string', text:'所属组织代码'}
            ,{ name:'userName', type:'string', text:'管理员'}
            ,{ name:'userId', type:'string', text:'管理员id'}
        ]
    }
});
