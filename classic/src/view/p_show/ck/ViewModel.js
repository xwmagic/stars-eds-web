Ext.define("App.view.classic.p_show.ck.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicp_showckViewModel',
    data: {
        pageName: 'classicp_showckMain',
        add: true,
        update: true,
        del: true
    }
});
