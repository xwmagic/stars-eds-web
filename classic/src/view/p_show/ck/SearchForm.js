Ext.define("App.view.classic.p_show.ck.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicp_showckSearchForm',
    _mainConfs: App.view.classic.p_show.ck.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"name",maxLength:100}
                ,{name:"orgName",maxLength:100}
                ,{name:"userName",maxLength:100}
                ,"sr","ec"
            ],
            [
                {name:"code",maxLength:10}
                ,{name:"volumne",maxLength:10}
            ],
            [
                {name:"userId",maxLength:100}
            ]
        ]
    }
});
