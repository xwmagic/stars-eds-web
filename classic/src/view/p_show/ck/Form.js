Ext.define("App.view.classic.p_show.ck.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicp_showckForm',
    _mainConfs: App.view.classic.p_show.ck.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        name:{allowBlank:false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true}
            ],
            [
                {name:"name",maxLength:100}
                ,{name:"code",maxLength:10}
            ],
            [
                {name:"volumne",maxLength:10}
            ],
            [
                {name:"orgName",xtype:"classicp_showorgPicker",maxLength:100,keyName:'orgId'}
                ,{name:"userName",xtype:"classicp_showuserCombo",maxLength:100,keyName:'userId'}
            ],
            [
                {name:"orgId",maxLength:36}
            ],
            [
                {name:"orgCode",maxLength:20}
            ],
            [
                {name:"userId",maxLength:100}
            ]
        ]
    }
});
