Ext.define("App.view.classic.p_show.org.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'orgName', type:'string', text:'组织名称'}
            ,{ name:'orgCode', type:'string', text:'组织编码'}
            ,{ name:'pid', type:'string', text:'父ID'}
            ,{ name:'layer', type:'int', text:'层级'}
            ,{ name:'sortNum', type:'int', text:'排序号'}
        ]
    }
});
