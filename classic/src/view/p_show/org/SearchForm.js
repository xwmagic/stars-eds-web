Ext.define("App.view.classic.p_show.org.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicp_showorgSearchForm',
    _mainConfs: App.view.classic.p_show.org.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"orgName",maxLength:100}
                ,"sr","ec"
            ],
            [
                {name:"orgCode",maxLength:100}
            ]
        ]
    }
});
