Ext.define("App.view.classic.p_show.org.TreeEditGrid", { 
    extend: "App.view.common.EditTreeGridDrag",
    xtype: 'classicp_showorgTreeEditGrid',
    _mainConfs: App.view.classic.p_show.org.Datas.Main,
    url:'pShowOrgCtl/findTree',//tree的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"id",hidden:true},
            {name:"orgName",xtype:"treecolumn",width:250,flex:1},
            {name:"orgCode",flex:1},
            {name:"layer",flex:1}
        ]
    }
});
