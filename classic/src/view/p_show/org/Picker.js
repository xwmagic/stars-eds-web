Ext.define("App.view.classic.p_show.org.Picker", { 
    extend: "App.view.common.TreeMuiltiPicker",
    xtype: 'classicp_showorgPicker',
    gridClassName:'App.view.classic.p_show.org.TreeGridMini',
    checkModel:'single',//选择模式
    onlyLeaf:false,
    displayField: 'orgName',
    valueField: 'id'
});
