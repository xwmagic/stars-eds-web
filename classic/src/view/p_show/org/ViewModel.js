Ext.define("App.view.classic.p_show.org.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicp_showorgViewModel',
    data: {
        pageName: 'classicp_showorgMain',
        lock: true,
        add: true,
        addChild: true,
        save: true,
        del: true
    }
});
