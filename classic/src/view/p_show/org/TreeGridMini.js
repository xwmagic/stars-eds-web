Ext.define("App.view.classic.p_show.org.TreeGridMini", { 
    extend: "App.view.common.EditTreeGridMini",
    xtype: 'classicp_showorgTreeGridMini',
    _mainConfs: App.view.classic.p_show.org.Datas.Main,
    url:'pShowOrgCtl/findTree',//tree的查询地址
    filterName: 'orgName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"orgName",xtype:"treecolumn",width:250,flex:1}
        ]
    }
});
