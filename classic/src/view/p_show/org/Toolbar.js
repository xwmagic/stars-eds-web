Ext.define("App.view.classic.p_show.org.Toolbar", { 
    extend: "App.view.common.TreeToolbar",
    xtype: 'classicp_showorgToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            'lock',//处理方法_create
            'c',//处理方法_create
            'addChild',
            'd',//处理方法_delete
            {name: 'save',actionUrl:'pShowOrgCtl/add'}//处理方法_save
        ]
    }
});
