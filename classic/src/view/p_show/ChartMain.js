Ext.define("App.classic.view.pas.ChartMain", {
    extend: 'App.ux.BasePanel',
    xtype: 'pasChartMain',
    layout:'column',
    defaults:{
        height:300
        ,border:true
        ,margin:10
        ,columnWidth:1
    },
    foreSetColumnWidth:function (columnWidth) {
        for(var k in this.items){
            this.items[k].columnWidth = columnWidth;
        }
    },
    listeners:{
        beforerender:function () {
            if(this.down('commonSearchForm')){
                this.down('commonSearchForm')._targetObject = this;
            }
        }
    },
    _search:function (param) {
        var echars = this.query('baseECharts');
        for(var i in echars){
            echars[i].load(param);
        }
    },
    initComponent:function () {
        if(!Ext.platformTags.desktop) {
            this.foreSetColumnWidth(1);
        }else{
            if(window.innerWidth < 700){
                this.foreSetColumnWidth(1);
            }else if(window.innerWidth < 1500){
                this.defaults.columnWidth = 0.5;
            }else if(window.innerWidth < 2000){
                this.defaults.columnWidth = 0.33;
            }else {
                this.defaults.columnWidth = 0.25;
            }
        }

        this.callParent();
    }
});
