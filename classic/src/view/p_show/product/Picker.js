Ext.define("App.view.classic.p_show.product.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicp_showproductPicker',
    gridClassName:'App.view.classic.p_show.product.GridMini',
    displayField: 'name',
    valueField:'id'
});
