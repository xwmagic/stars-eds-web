Ext.define("App.view.classic.p_show.product.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicp_showproductSearchForm',
    _mainConfs: App.view.classic.p_show.product.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"name",maxLength:100}
                ,{name:"code",maxLength:20}
                ,{name:"size",maxLength:20}
                ,"sr"
            ]
        ]
    }
});
