Ext.define("App.view.classic.p_show.product.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicp_showproductToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name:'c',tooltip:'新增',actionObject:'classicp_showproductWindowAdd'},
            {name:'u',tooltip:'编辑',actionObject:'classicp_showproductWindowEdit'},
            {name:'d',actionUrl:'pShowProductCtl/delById',tooltip:'删除'},
            {name:'export',actionUrl:'pShowProductCtl/exportList',tooltip:'导出Excel'},
            {name:'import',actionUrl:'pShowProductCtl/importExcel',tooltip:'导入Excel'}
        ]
    }
});
