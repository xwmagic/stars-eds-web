Ext.define("App.view.classic.p_show.product.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicp_showproductViewModel',
    data: {
        pageName: 'classicp_showproductMain',
        add: true,
        update: true,
        del: true
    }
});
