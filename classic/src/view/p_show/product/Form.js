Ext.define("App.view.classic.p_show.product.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicp_showproductForm',
    _mainConfs: App.view.classic.p_show.product.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        name:{allowBlank:false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true}
            ],
            [
                {name:"name",maxLength:100}
                ,{name:"code",maxLength:20}
            ],
            [
                {name:"size",maxLength:20}
                ,{name:"price",maxLength:36}
            ],
            [
                {name:"num",maxLength:100}
                ,{name:"unit",maxLength:100}
            ],
            [
                {name:"ckName",xtype:"classicp_showckPicker",maxLength:100}
            ]
        ]
    }
});
