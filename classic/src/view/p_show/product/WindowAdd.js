Ext.define("App.view.classic.p_show.product.WindowAdd", { 
    extend: "App.view.classic.p_show.product.Window",
    alias: "widget.classicp_showproductWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'pShowProductCtl/add'}, 'close']
});
