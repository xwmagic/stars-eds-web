Ext.define("App.view.classic.p_show.product.WindowEdit", { 
    extend: "App.view.classic.p_show.product.Window",
    alias: "widget.classicp_showproductWindowEdit",
    openValidation:true,
    isGetChangeValue:true,
    buttons: [{name: 'submit', actionUrl: 'pShowProductCtl/update'}, 'close']
});
