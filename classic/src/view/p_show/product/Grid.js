Ext.define("App.view.classic.p_show.product.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicp_showproductGrid',
    openRowButtons:true,
    columnsDefault:{autoLinefeed:true},
    _mainConfs: App.view.classic.p_show.product.Datas.Main,
    url:'pShowProductCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name",hyperlinkMode:true,hyperlinkWindow:"classicp_showproductWindowView"},
            {name:"code"},
            {name:"size"},
            {name:"price"},
            {name:"num"},
            {name:"unit"},
            {name:"ckName",minWidth:100}
        ]
    }
});
