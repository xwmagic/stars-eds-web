Ext.define("App.view.classic.p_show.product.Combo", { 
    extend: "App.view.BaseCombo",
    xtype: 'classicp_showproductCombo',
    dynamicDataUrl:'pShowProductCtl/pageList',//数据的查询地址
    valueField: 'name',
    displayField: 'name'
});
