Ext.define("App.view.classic.p_show.product.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'name', type:'string', text:'产品名称'}
            ,{ name:'code', type:'string', text:'产品编码'}
            ,{ name:'size', type:'string', text:'产品规格'}
            ,{ name:'price', type:'double', text:'单价'}
            ,{ name:'num', type:'int', text:'数量'}
            ,{ name:'unit', type:'string', text:'单位'}
            ,{ name:'ckName', type:'string', text:'所属仓库'}
            ,{ name:'ckId', type:'string', text:'所属仓库'}
        ]
    }
});
