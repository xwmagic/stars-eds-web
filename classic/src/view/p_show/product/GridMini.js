Ext.define("App.view.classic.p_show.product.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicp_showproductGridMini',
    _mainConfs: App.view.classic.p_show.product.Datas.Main,
    url:'pShowProductCtl/pageList',//tree的查询地址
    filterName: 'name',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name"},
            {name:"code"},
            {name:"size"},
            {name:"price"},
            {name:"num"},
            {name:"unit"},
            {name:"ckName",minWidth:100}
        ]
    }
});
