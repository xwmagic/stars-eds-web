Ext.define("App.view.classic.p_show.product.EditGrid", { 
    extend: "App.view.common.EditGrid",
    xtype: 'classicp_showproductEditGrid',
    _mainConfs: App.view.classic.p_show.product.Datas.Main,
    url:'pShowProductCtl/pageList',//grid的查询地址
    selType: "checkboxmodel",
    editColIndex: 2,
    autoLoad: true,
    tools: ["multiEdit","plus","minus"],
    paramName: {//编辑列表获取参数的参数名配置。提交时将会按照配置的参数名提交
        add: "addJsonPShowProduct",
        update: "updateJsonPShowProduct",
        del: "delJsonPShowProduct"
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"name"},
            {name:"code"},
            {name:"size"},
            {name:"price"},
            {name:"num"},
            {name:"unit"},
            {name:"ckName",minWidth:100}
        ]
    }
});
