Ext.define("App.view.classic.p_show.product.SelectWindow", { 
    extend: "App.classic.view.common.GridSelectWindow",
    xtype: 'classicp_showproductSelectWindow',
    formXtype: 'classicp_showproductSearchForm',
    gridXtype: 'classicp_showproductGridSelect'
});
