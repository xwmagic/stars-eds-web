Ext.define("App.view.classic.p_show.ordersinfo.EditGrid", { 
    extend: "App.view.common.EditGrid",
    xtype: 'classicp_showordersinfoEditGrid',
    _mainConfs: App.view.classic.p_show.ordersinfo.Datas.Main,
    url:'pShowOrdersInfoCtl/pageList',//grid的查询地址
    selType: "checkboxmodel",
    editColIndex: 2,
    autoLoad: true,
    tools: ["multiEdit","plus","minus"],
    paramName: {//编辑列表获取参数的参数名配置。提交时将会按照配置的参数名提交
        add: "addJsonPShowOrdersInfo",
        update: "updateJsonPShowOrdersInfo",
        del: "delJsonPShowOrdersInfo"
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"productName",flex:2,editor:{xtype:'classicp_showproductCombo',listeners:{select:function(th,rec){th.ownerCt.context.record.set('productId',rec.get('id'));}}}},
            {name:"number",flex:1},
            {name:"price",flex:1},
            {name:"discount",flex:1},
            {name:"discountPrice",flex:1},
            {name:"sumPrice",flex:1},
            {name:"doneNumber",flex:1},
            {name:"status",flex:1,editor:{xtype:'pascommonOrderStatus'},valueToName:'pascommonOrderStatus',minWidth:100}
        ]
    }
});
