Ext.define("App.view.classic.p_show.ordersinfo.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'pShowOrdersId', type:'string', text:'订单ID'}
            ,{ name:'productId', type:'string', text:'产品ID'}
            ,{ name:'number', type:'long', text:'数量'}
            ,{ name:'price', type:'double', text:'单价（元）'}
            ,{ name:'discount', type:'double', text:'折扣'}
            ,{ name:'discountPrice', type:'double', text:'折扣价（元）'}
            ,{ name:'sumPrice', type:'double', text:'总价'}
            ,{ name:'doneNumber', type:'long', text:'完成数量'}
            ,{ name:'status', type:'int', text:'状态'}
            ,{ name:'productName', type:'string', text:'产品名称'}
            ,{ name:'productGg', type:'string', text:'产品规格'}
        ]
    }
});
