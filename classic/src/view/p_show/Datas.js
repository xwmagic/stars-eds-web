Ext.define("App.view.classic.pas.Datas", {
    statics: {
        Main: [
            { name:'year', type:'number', text:'年'}
            ,{ name:'month', type:'number', text:'月'}
            ,{ name:'date', type:'number', text:'日期'}
            ,{ name:'dateStart', type:'number', text:'起始日期'}
            ,{ name:'dateEnd', type:'number', text:'截止日期'}
            ,{ name:'yearMonth', type:'number', text:'年月'}
            ,{ name:'yearMonthStart', type:'number', text:'起始年月'}
            ,{ name:'yearMonthEnd', type:'number', text:'截止年月'}
            ,{ name:'supplierName', type:'number', text:'供应商'}
            ,{ name:'supplierId', type:'number', text:'供应商'}
            ,{ name:'customerName', type:'number', text:'客户'}
            ,{ name:'customerId', type:'number', text:'客户'}
        ]
    }
});
