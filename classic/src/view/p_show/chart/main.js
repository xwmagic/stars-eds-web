Ext.define("App.classic.view.pas.chart.main", {
    extend: 'App.classic.view.pas.ChartMain',
    xtype: 'passalechartmain',
    items:[
        {xtype:'classicpasorderchartSearchForm',columnWidth:1,height:37},
        {xtype:'pasorderchartOrderStatus',columnWidth:0.5},
        {xtype:'pasBaseBar',title:'客户销售量(个)',url:'pShowOrdersCtl/chartCustomerOrderNumAll',columnWidth:0.5},
        {xtype:'pasBaseBar',title:'用户(个)',url:'pShowUserCtl/chartUserOrg',columnWidth:1}
    ]
});
