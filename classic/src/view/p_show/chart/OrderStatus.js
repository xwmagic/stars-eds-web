Ext.define("App.classic.view.pas.orderchart.OrderStatus", {
    extend: "App.view.ux.chart.Pie",
    xtype: 'pasorderchartOrderStatus',
    url: 'pShowOrdersCtl/chartOrdersStatus',
    valueToName:App.view.pas.common.OrderStatus.Datas,
    /*defaultParams: {
        years: 2019,
        month: 11
    },*/
    series:{
        name:'订单状态',
        color:["#8d98b3","#c14089","#07a2a4"]
    },
    option:{
        title: {
            text: '订单状态统计',
            x: 'center'
        },
        grid: [{
            left: 50,
            right: 50,
            height: '70%'
        }]
    }
});
