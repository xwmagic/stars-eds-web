Ext.define("App.view.classic.pas.orderchart.SearchForm", {
    extend: "App.view.common.SearchForm",
    xtype: 'classicpasorderchartSearchForm',
    _mainConfs: App.view.classic.pas.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"yearMonthStart",xtype:'commondateMonthField'}
                ,{name:"yearMonthEnd",xtype:'commondateMonthField'}
                ,"sr","ec"
            ]
        ]
    }
});
