Ext.define("App.classic.view.pas.BaseBar", {
    extend: "App.view.ux.chart.Bar",
    xtype: 'pasBaseBar',
    url: '',
    title:'',
    /*defaultParams: {
        years: 2019,
        month: 11
    },*/
    option:{
        title: {
            text: '',
            x: 'center'
        },
        grid: [{
            left: 65,
            right: 5,
            height: '65%',
            bottom:'25%'
        }],
        xAxis:{
            axisLabel: {interval:0,rotate:15 }
        }
    },
    initComponent:function () {
        this.option.title.text = this.title;
        this.callParent();
    },
    requestSuccess: function (data) {
        var option = this.option;
        option.series = [];
        option.xAxis.data = data.xaxisData;
        if(data.xaxisData.length > this.showZoomNumber && !this.option.dataZoom){
            this.option.dataZoom = [{
                type: 'slider'
            }];
        }
        if (this.seriesName) {
            var serie;
            for(var i in data.seriesData){
                serie = this.createSeries();
                serie.data = data.seriesData[i].data;
                if(data.legendData) {
                    serie.name = data.legendData[i];
                }
                option.series.push(serie);
            }
        }
        if(data.legendData){
            option.legend.data = data.legendData;
        }else {
            this.setLegend(data.xaxisData);
        }

        this.setOption(option);
    }
});
