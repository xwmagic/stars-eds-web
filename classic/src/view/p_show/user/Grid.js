Ext.define("App.view.classic.p_show.user.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicp_showuserGrid',
    openRowButtons:true,
    columnsDefault:{autoLinefeed:true},
    _mainConfs: App.view.classic.p_show.user.Datas.Main,
    url:'pShowUserCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"uname",flex:1,hyperlinkMode:true,hyperlinkWindow:"classicp_showuserWindowView"},
            {name:"uage",flex:1},
            {name:"usex",flex:1,valueToName:'booleanYNSex',align:'center'},
            {name:"orgName",flex:1,minWidth:100}
        ]
    }
});
