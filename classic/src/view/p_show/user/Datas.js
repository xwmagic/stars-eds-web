Ext.define("App.view.classic.p_show.user.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'uname', type:'string', text:'姓名'}
            ,{ name:'uage', type:'int', text:'年龄'}
            ,{ name:'usex', type:'int', text:'性别'}
            ,{ name:'orgId', type:'string', text:'所属组织'}
            ,{ name:'orgName', type:'string', text:'所属组织'}
            ,{ name:'orgCode', type:'string', text:'所属组织代码'}
        ]
    }
});
