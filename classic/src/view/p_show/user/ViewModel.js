Ext.define("App.view.classic.p_show.user.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicp_showuserViewModel',
    data: {
        pageName: 'classicp_showuserMain',
        add: true,
        update: true,
        del: true
    }
});
