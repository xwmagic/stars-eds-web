Ext.define("App.view.classic.p_show.user.WindowEdit", { 
    extend: "App.view.classic.p_show.user.Window",
    alias: "widget.classicp_showuserWindowEdit",
    openValidation:true,
    isGetChangeValue:true,
    buttons: [{name: 'submit', actionUrl: 'pShowUserCtl/update'}, 'close']
});
