Ext.define("App.view.classic.p_show.user.Combo", { 
    extend: "App.view.BaseCombo",
    xtype: 'classicp_showuserCombo',
    dynamicDataUrl:'pShowUserCtl/pageList',//数据的查询地址
    valueField: 'uname',
    displayField: 'uname'
});
