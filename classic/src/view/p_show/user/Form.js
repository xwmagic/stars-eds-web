Ext.define("App.view.classic.p_show.user.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicp_showuserForm',
    _mainConfs: App.view.classic.p_show.user.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        uname:{allowBlank:false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true}
            ],
            [
                {name:"uname",maxLength:100}
            ],
            [
                {name:"uage",maxLength:3}
                ,{name:"usex",xtype:"booleanYNSex",maxLength:1}
            ],
            [
                {name:"orgName",xtype:"classicp_showorgPicker",maxLength:36,hidden:false,keyName:'orgId'}
            ]
        ]
    }
});
