Ext.define("App.view.classic.p_show.user.EditGrid", { 
    extend: "App.view.common.EditGrid",
    xtype: 'classicp_showuserEditGrid',
    _mainConfs: App.view.classic.p_show.user.Datas.Main,
    url:'pShowUserCtl/pageList',//grid的查询地址
    selType: "checkboxmodel",
    editColIndex: 2,
    autoLoad: true,
    tools: ["multiEdit","plus","minus"],
    paramName: {//编辑列表获取参数的参数名配置。提交时将会按照配置的参数名提交
        add: "addJsonPShowUser",
        update: "updateJsonPShowUser",
        del: "delJsonPShowUser"
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"uname",flex:1},
            {name:"uage",flex:1},
            {name:"usex",flex:1,valueToName:'booleanYNSex',align:'center'},
            {name:"orgName",flex:1,minWidth:100}
        ]
    }
});
