Ext.define("App.view.classic.p_show.user.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicp_showuserMain',
    viewModel: 'classicp_showuserViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicp_showuserSearchForm',hidden: false},
        {xtype: 'classicp_showuserToolbar',hidden: false},
        {xtype: 'classicp_showuserGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicp_showuserToolbar');
            var form = th.down('classicp_showuserSearchForm');
            var grid = th.down('classicp_showuserGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
            grid._toolbar = toolbar;//给grid添加工具栏对象引用
            grid.actionControlLogic();//添加选择列表数据控制操作按钮的逻辑
        }
    }
});
