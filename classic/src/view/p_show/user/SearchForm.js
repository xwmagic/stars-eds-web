Ext.define("App.view.classic.p_show.user.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicp_showuserSearchForm',
    _mainConfs: App.view.classic.p_show.user.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"uname",maxLength:100}
                ,{name:"uage",maxLength:3}
                ,{name:"usex",xtype:"booleanYNSex",maxLength:1}
                ,"sr"
            ]
        ]
    }
});
