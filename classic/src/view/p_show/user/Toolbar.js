Ext.define("App.view.classic.p_show.user.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicp_showuserToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name:'c',tooltip:'新增',actionObject:'classicp_showuserWindowAdd'},
            {name:'u',tooltip:'编辑',actionObject:'classicp_showuserWindowEdit'},
            {name:'d',actionUrl:'pShowUserCtl/delById',tooltip:'删除'},
            {name:'export',actionUrl:'pShowUserCtl/exportList',tooltip:'导出Excel'},
            {name:'import',actionUrl:'pShowUserCtl/importExcel',tooltip:'导入Excel'}
        ]
    }
});
