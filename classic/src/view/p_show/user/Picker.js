Ext.define("App.view.classic.p_show.user.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicp_showuserPicker',
    gridClassName:'App.view.classic.p_show.user.GridMini',
    displayField: 'uname',
    valueField:'id'
});
