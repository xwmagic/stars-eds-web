Ext.define("App.view.classic.p_show.user.WindowAdd", { 
    extend: "App.view.classic.p_show.user.Window",
    alias: "widget.classicp_showuserWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'pShowUserCtl/add'}, 'close']
});
