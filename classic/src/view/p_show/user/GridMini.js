Ext.define("App.view.classic.p_show.user.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicp_showuserGridMini',
    _mainConfs: App.view.classic.p_show.user.Datas.Main,
    url:'pShowUserCtl/pageList',//tree的查询地址
    filterName: 'uname',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"uname",flex:1},
            {name:"uage",flex:1},
            {name:"usex",flex:1,valueToName:'booleanYNSex',align:'center'},
            {name:"orgName",flex:1,minWidth:100}
        ]
    }
});
