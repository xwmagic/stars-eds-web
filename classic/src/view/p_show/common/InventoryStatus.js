/**
 * 盘点状态
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.InventoryStatus', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pasCommonInventoryStatus',
	checkObject: {columns: 4},
    Empty:1,
    statics:{
	    Datas:{
	        1:{text:'未完成',width:80,color:'#1b1d29'},
	        2:{text:'已完成',width:80,color:'#20d69e'}
        }
    }
});

