/**
 * 盘点同步状态
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.InventorySycStatus', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pasCommonInventorySycStatus',
	checkObject: {columns: 4},
    Empty:1,
    statics:{
	    Datas:{
	        1:{text:'未同步',width:80,color:'#1e1e29'},
	        2:{text:'已同步',width:80,color:'#067dd6'}
        }
    }
});

