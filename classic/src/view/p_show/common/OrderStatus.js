/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.OrderStatus', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pascommonOrderStatus',
	checkObject: {columns: 4},
    statics:{
	    Datas:{
	        1:{text:'未完成',width:80,color:'#49484b'},
	        2:{text:'进行中',width:80,color:'#1b83d6'},
	        3:{text:'已完成',width:80,color:'#54d624'}
        }
    },
    valueToName: function(value,metaData, record){
        if(Ext.isEmpty(value)){
            return '';
        }
        if(value){
            value = Number(value);
        }
        return Ext.Component.getColorView(this._Class.Datas[value].text,this._Class.Datas[value].color);
    }
});

