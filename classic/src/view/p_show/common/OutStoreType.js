/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.OutStoreType', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pascommonOutStoreType',
	checkObject: {columns: 5},
    statics:{
        Datas:{
            '1':{text:'销售出库',width:80,color:'#641dce'},
            '5':{text:'样品出库',width:80,color:'#4cd030'},
            '2':{text:'退货出库',width:80,color:'#1b83d6'},
            '3':{text:'盘点出库',width:80,color:'#d6b533',hidden:true},
            '4':{text:'其他出库',width:80,color:'#43ce92'}
        }
    }
});

