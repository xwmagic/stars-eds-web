/**
 * 月结状态
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.MonthStatus', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pascommonMonthStatus',
	checkObject: {columns: 4},
    statics:{
	    Datas:{
	        7:{text:'部分月结',width:80,color:'#2bd6ab'},
	        6:{text:'已月结',width:80,color:'#4f42d6'}
        }
    }
});

