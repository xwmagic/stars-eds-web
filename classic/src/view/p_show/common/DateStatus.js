/**
 * 时间类型
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.DateStatus', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pascommonDateStatus',
	checkObject: {columns: 4},
    statics:{
	    Datas:{
	        4:{text:'将到期',width:80,color:'#c6b81c'},
	        5:{text:'已过期',width:80,color:'#d63d16'}
        }
    }
});

