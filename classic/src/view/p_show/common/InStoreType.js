/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.InStoreType', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pascommonInStoreType',
	checkObject: {columns: 4},
    statics:{
        Datas:{
            '1':{text:'采购入库',width:80,color:'#641dce'},
            '2':{text:'退货入库',width:80,color:'#1b83d6'},
            '3':{text:'盘点入库',width:80,color:'#d6b533',hidden:true},
            '4':{text:'其他入库',width:80,color:'#43ce92'}
        }
    }
});

