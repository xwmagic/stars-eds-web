/**
 * 打印状态
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.PrintStatus', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pascommonPrintStatus',
	checkObject: {columns: 4},
    statics:{
	    Datas:{
	        3:{text:'已打印',width:80,color:'#d68824'}
        }
    }
});

