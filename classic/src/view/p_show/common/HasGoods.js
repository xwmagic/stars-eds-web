/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.HasGoods', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pascommonHasGoods',
	checkObject: {columns: 4},
    statics:{
        Datas:{
            1:{text:'无货',width:80,color:'#302f32'},
            2:{text:'部分',width:80,color:'#1ad6bd'},
            3:{text:'有货',width:80,color:'#12d620'}
        }
    }
});

