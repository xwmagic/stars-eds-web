Ext.define("App.view.classic.p_show.orders.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'主键'}
            ,{ name:'orderName', type:'string', text:'订单名称'}
            ,{ name:'orderCode', type:'string', text:'订单编号'}
            ,{ name:'orderDate', type:'date', text:'订单日期'}
            ,{ name:'orderEndDate', type:'date', text:'截止日期'}
            ,{ name:'customerName', type:'string', text:'客户名称'}
            ,{ name:'customerId', type:'string', text:'客户ID'}
            ,{ name:'orderNum', type:'long', text:'订单总数'}
            ,{ name:'orderPrice', type:'double', text:'订单总价(元)'}
            ,{ name:'orderSite', type:'string', text:'收货地址'}
            ,{ name:'orderUser', type:'string', text:'收货人'}
            ,{ name:'orderPhone', type:'string', text:'收货人电话'}
            ,{ name:'orderStatus', type:'string', text:'订单状态'}
            ,{ name:'orderDoneNum', type:'long', text:'完成总数'}
            ,{ name:'orderDonePrice', type:'double', text:'完成总价(元)'}
            ,{ name:'hasGoods', type:'int', text:'是否有货'}
        ]
    }
});
