Ext.define("App.view.classic.p_show.orders.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classicp_showordersPicker',
    gridClassName:'App.view.classic.p_show.orders.GridMini',
    displayField: 'orderName',
    valueField:'id'
});
