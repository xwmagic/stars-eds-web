Ext.define("App.view.classic.p_show.orders.WindowEdit", { 
    extend: "App.view.classic.p_show.orders.Window",
    alias: "widget.classicp_showordersWindowEdit",
    openValidation:true,
    isGetChangeValue:true,
    buttons: [{name: 'submit', actionUrl: 'pShowOrdersCtl/add'}, 'close']
});
