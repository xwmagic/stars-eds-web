Ext.define("App.view.classic.p_show.orders.WindowAdd", { 
    extend: "App.view.classic.p_show.orders.Window",
    alias: "widget.classicp_showordersWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'pShowOrdersCtl/add'}, 'close']
});
