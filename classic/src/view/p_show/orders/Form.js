Ext.define("App.view.classic.p_show.orders.Form", { 
    extend: "App.view.common.Form",
    xtype: 'classicp_showordersForm',
    _mainConfs: App.view.classic.p_show.orders.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        orderName:{allowBlank:false}
        ,orderDate:{allowBlank:false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"id",maxLength:36,hidden:true}
                ,{name:"orderName",maxLength:100}
                ,{name:"orderCode",maxLength:20}
            ],
            [
                {name:"orderDate"}
                ,{name:"orderEndDate"}
            ],
            [
                {name:"customerName",xtype:"classicp_showcustomerPicker",maxLength:100,keyName:'customerId'}
                ,{name:"orderNum",maxLength:20}
            ],
            [
                {name:"orderPrice",maxLength:20}
                ,{name:"orderSite",maxLength:100}
            ],
            [
                {name:"orderUser",maxLength:50}
                ,{name:"orderPhone",maxLength:30}
            ],
            [
                {name:"orderStatus",xtype:"pascommonOrderStatus",maxLength:30}
                ,{name:"orderDoneNum",maxLength:100}
            ],
            [
                {name:"orderDonePrice",maxLength:100}
                ,{name:"hasGoods",xtype:"booleanYNC",maxLength:2}
            ]
        ]
    }
});
