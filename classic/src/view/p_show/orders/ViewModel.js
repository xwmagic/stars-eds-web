Ext.define("App.view.classic.p_show.orders.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classicp_showordersViewModel',
    data: {
        pageName: 'classicp_showordersMain',
        add: true,
        update: true,
        del: true
    }
});
