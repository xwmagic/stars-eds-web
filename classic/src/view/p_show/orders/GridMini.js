Ext.define("App.view.classic.p_show.orders.GridMini", { 
    extend: "App.view.common.CRUDGridMini",
    xtype: 'classicp_showordersGridMini',
    _mainConfs: App.view.classic.p_show.orders.Datas.Main,
    url:'pShowOrdersCtl/pageList',//tree的查询地址
    filterName: 'orderName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"orderStatus",valueToName:'pascommonOrderStatus'},
            {name:"orderName"},
            {name:"orderCode"},
            {name:"orderDate"},
            {name:"orderEndDate"},
            {name:"customerName"},
            {name:"orderNum"},
            {name:"orderPrice"},
            {name:"orderSite"},
            {name:"orderUser"},
            {name:"orderPhone"},
            {name:"orderDoneNum"},
            {name:"orderDonePrice"},
            {name:"hasGoods",valueToName:'booleanYNC',minWidth:100}
        ]
    }
});
