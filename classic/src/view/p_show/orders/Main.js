Ext.define("App.view.classic.p_show.orders.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classicp_showordersMain',
    viewModel: 'classicp_showordersViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classicp_showordersSearchForm',hidden: false},
        {xtype: 'classicp_showordersToolbar',hidden: false},
        {xtype: 'classicp_showordersGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classicp_showordersToolbar');
            var form = th.down('classicp_showordersSearchForm');
            var grid = th.down('classicp_showordersGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
            grid._toolbar = toolbar;//给grid添加工具栏对象引用
            grid.actionControlLogic();//添加选择列表数据控制操作按钮的逻辑
        }
    }
});
