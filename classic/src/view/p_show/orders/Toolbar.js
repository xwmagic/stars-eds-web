Ext.define("App.view.classic.p_show.orders.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classicp_showordersToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name:'c',tooltip:'新增',actionObject:'classicp_showordersWindowAdd'},
            {name:'u',tooltip:'编辑',actionObject:'classicp_showordersWindowEdit'},
            {name:'d',actionUrl:'pShowOrdersCtl/delById',tooltip:'删除'}
        ]
    }
});
