Ext.define("App.view.classic.p_show.orders.Window", { 
    extend: "App.view.common.Window",
    alias: "widget.classicp_showordersWindow",
    width: 800, // 窗口宽度
    bodyPadding: '15px',
    items: [// 当items中对象配置的isParam为true时，当前window的提交操作会自动调用该对象的获取参数方法和isValid方法验证数据合法性
        //如果当前窗口对象的isGetChangeValue属性值为false（默认），items中form表单，调用getValues方法获取参数。如果是grid编辑列表，调用getEncodeValues方法获取参数
        //如果当前窗口对象的isGetChangeValue属性值为true，items中的form表单，调用getChangeValues方法获取参数。grid列表，getEncodeChangeValues方法获取参数
        //当items中对象配置autoSetSelect: true,如果窗口对象的_record属性存在，将会自动将_record中的数据赋值给items的对象
        {name:"baseFrom",xtype:"classicp_showordersForm",isParam:true,autoSetSelect:true}
        ,{name:"details",xtype:"classicp_showordersinfoEditGrid",isParam:true,autoSetSelect:true,mainKeyName:'pShowOrdersId',border:true,title:'明细列表'}
    ]
});
