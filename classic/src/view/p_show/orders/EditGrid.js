Ext.define("App.view.classic.p_show.orders.EditGrid", { 
    extend: "App.view.common.EditGrid",
    xtype: 'classicp_showordersEditGrid',
    _mainConfs: App.view.classic.p_show.orders.Datas.Main,
    url:'pShowOrdersCtl/pageList',//grid的查询地址
    selType: "checkboxmodel",
    editColIndex: 2,
    autoLoad: true,
    tools: ["multiEdit","plus","minus"],
    paramName: {//编辑列表获取参数的参数名配置。提交时将会按照配置的参数名提交
        add: "addJsonPShowOrders",
        update: "updateJsonPShowOrders",
        del: "delJsonPShowOrders"
    },
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"orderStatus",valueToName:'pascommonOrderStatus'},
            {name:"orderName"},
            {name:"orderCode"},
            {name:"orderDate"},
            {name:"orderEndDate"},
            {name:"customerName"},
            {name:"orderNum"},
            {name:"orderPrice"},
            {name:"orderSite"},
            {name:"orderUser"},
            {name:"orderPhone"},
            {name:"orderDoneNum"},
            {name:"orderDonePrice"},
            {name:"hasGoods",valueToName:'booleanYNC',minWidth:100}
        ]
    }
});
