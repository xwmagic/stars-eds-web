Ext.define("App.view.classic.p_show.orders.Combo", { 
    extend: "App.view.BaseCombo",
    xtype: 'classicp_showordersCombo',
    dynamicDataUrl:'pShowOrdersCtl/pageList',//数据的查询地址
    valueField: 'orderName',
    displayField: 'orderName'
});
