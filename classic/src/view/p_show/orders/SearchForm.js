Ext.define("App.view.classic.p_show.orders.SearchForm", { 
    extend: "App.view.common.SearchForm",
    xtype: 'classicp_showordersSearchForm',
    _mainConfs: App.view.classic.p_show.orders.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"orderName",maxLength:100}
                ,{name:"orderCode",maxLength:20}
                ,"sr","ec"
            ],
            [
                {name:"orderDate"}
                ,{name:"orderEndDate"}
            ],
            [
                {name:"customerName",maxLength:100}
                ,{name:"orderUser",maxLength:50}
            ],
            [
                {name:"orderStatus",xtype:"pascommonOrderStatus",maxLength:30}
            ]
        ]
    }
});
