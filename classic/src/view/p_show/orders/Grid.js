Ext.define("App.view.classic.p_show.orders.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classicp_showordersGrid',
    openRowButtons:true,
    columnsDefault:{autoLinefeed:true},
    _mainConfs: App.view.classic.p_show.orders.Datas.Main,
    url:'pShowOrdersCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"orderStatus",valueToName:'pascommonOrderStatus'},
            {name:"orderName",hyperlinkMode:true,hyperlinkWindow:"classicp_showordersWindowView"},
            {name:"orderCode"},
            {name:"orderDate"},
            {name:"orderEndDate"},
            {name:"customerName"},
            {name:"orderNum"},
            {name:"orderPrice"},
            {name:"orderSite"},
            {name:"orderUser"},
            {name:"orderPhone"},
            {name:"orderDoneNum"},
            {name:"orderDonePrice"},
            {name:"hasGoods",valueToName:'booleanYNC',minWidth:100}
        ]
    }
});
