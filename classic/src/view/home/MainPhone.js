Ext.define("App.classic.view.home.MainPhone", {
    extend: 'App.ux.BasePanel',
    xtype: 'viewhomeMainPhone',
    layout: {type: 'hbox', align: 'stretch'},
    items: [
        {
            xtype: 'mainMenuPhone', flex: 1
        }
    ]
});
