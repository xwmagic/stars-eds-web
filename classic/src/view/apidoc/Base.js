Ext.define("App.classic.view.apidoc.Base", {
    extend: 'App.ux.BasePanel',
    xtype: 'apidocBase',
    layout: {type: 'vbox', align: 'stretch'},
    STARS_MENU_URL:'',
    initComponent:function () {
        this.tools = [{
            itemId: 'refresh',
            type: 'refresh',
            tooltip: '刷新',
            callback: function (owner) {
                owner.removeAll();
                owner.setHtml(owner.getOrginHtml());
            }
        }];
        var me = this;
        setTimeout(function () {
            var h = me.getHeight();
            me.setHtml(me.getOrginHtml());
        },100);

        this.callParent();
    },
    getOrginHtml:function () {
        return '<iframe style="width: 100%;height: '+this.getBHeight()+'px;position: relative;top:0px;" src="'+this.STARS_MENU_URL+'"></iframe>';
    },
    getBHeight:function(){
        return this.getHeight() - 30;
    }
});
