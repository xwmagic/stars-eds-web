Ext.define("App.classic.view.apidoc.Beetl", {
    extend: 'Ext.container.Container',
    xtype: 'apidocBeetl',
    layout: {type: 'vbox', align: 'stretch'},
    items: [
        {
            xtype: 'panel',
            title: 'Beetl模版',
            height: window.innerHeight,
            tools:[{
                itemId: 'refresh',
                type: 'refresh',
                tooltip: '刷新',
                callback: function (owner) {
                    owner.removeAll();
                    owner.setHtml('<iframe style="width: 100%;height: 100%;position: relative;" src="http://ibeetl.com/beetlonline/"></iframe>');
                }
            }],
            html: '<iframe style="width: 100%;height: 100%;position: relative;" src="http://ibeetl.com/beetlonline/"></iframe>'
        }
    ]
});
