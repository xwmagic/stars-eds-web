Ext.define("App.classic.view.org.MainToolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'vieworgMainToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'vieworgMainWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'vieworgMainWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:''}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
