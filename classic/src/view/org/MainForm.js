Ext.define("App.classic.view.org.MainForm", { 
    extend: "App.view.common.Form",
    xtype: 'vieworgMainForm',
    _mainConfs: App.classic.view.org.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'id',hidden:true}
            ],
            [
                {name: 'orgName'},
                {name: 'orgEn'}
            ],
            [
                {name: 'orgCode'},
                {name: 'orgType'}
            ],
            [
                {name: 'pid'},
                {name: 'companyId'}
            ],
            [
                {name: 'logo'},
                {name: 'layer'}
            ],
            [
                {name: 'info'},
                {name: 'sortNum'}
            ]
        ]
    }
});
