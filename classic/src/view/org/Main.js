Ext.define("App.classic.view.org.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'vieworgMain',
    requires: [ 
        "App.classic.view.org.MainForm",
        "App.classic.view.org.MainGrid",
        "App.classic.view.org.MainSearchForm",
        "App.classic.view.org.MainToolbar",
        "App.classic.view.org.MainViewModel",
        "App.classic.view.org.MainWindow",
        "App.classic.view.org.MainWindowAdd",
        "App.classic.view.org.MainWindowEdit"
    ],
    viewModel: 'vieworgMainViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'vieworgMainSearchForm'},
        {xtype: 'vieworgMainToolbar'},
        {xtype: 'vieworgMainGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('vieworgMainToolbar');
            var form = th.down('vieworgMainSearchForm');
            var grid = th.down('vieworgMainGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
