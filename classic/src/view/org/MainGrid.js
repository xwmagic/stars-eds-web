Ext.define("App.classic.view.org.MainGrid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'vieworgMainGrid',
    _mainConfs: App.classic.view.org.Datas.Main,
    url:'',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'orgName'},
            {name: 'orgEn'},
            {name: 'orgCode'},
            {name: 'orgType'},
            {name: 'pid'},
            {name: 'companyId'},
            {name: 'logo'},
            {name: 'layer'},
            {name: 'info'},
            {name: 'sortNum'}
        ]
    }
});
