Ext.define("App.classic.view.org.MainSearchForm", { 
    extend: "App.view.common.Form",
    xtype: 'vieworgMainSearchForm',
    openEnterSearch:true,
    _mainConfs: App.classic.view.org.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'orgName'}, //配置需要查询的字段名
                {name: 'companyId'}, //配置需要查询的字段名
                'sr' //s代表查询按钮，r代表重置按钮
            ]
        ]
    }
});
