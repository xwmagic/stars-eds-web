Ext.define("App.classic.view.org.Datas", {
    statics: {
        Main:[
             {name: "id", type: "string", text: "主键ID"}
            ,{name:"orgName", type:"string", text:"组织名称"}
            ,{name:"orgEn", type:"string", text:"组织英文名称"}
            ,{name:"orgCode", type:"string", text:"组织代码"}
            ,{name:"orgType", type:"string", text:"组织类型"}
            ,{name:"pid", type:"string", text:"父组织ID"}
            ,{name:"companyId", type:"string", text:"所属公司Id"}
            ,{name:"logo", type:"string", text:"组织logo"}
            ,{name:"layer", type:"string", text:"是否启用"}
            ,{name:"info", type:"string", text:"描述"}
            ,{name:"sortNum", type:"string", text:"层级"}
        ]
    }
});