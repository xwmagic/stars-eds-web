Ext.define("App.classic.view.code.GenGridMini", {
    extend: "App.view.common.CRUDGridMini",
    xtype: 'viewcodeGenGridMini',
    _mainConfs: App.classic.view.code.Datas.Gen,
    url:'myGenerate/list',//grid的查询地址
    filterName: 'tableName',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'tableName',width:150,align:'left'},
            {name: 'entityName',width:150,align:'left'},
            {name: 'moduleName',width:100},
            {name: 'tableType',width:100,valueToName:'codeTableTypeCombo',align:'center'},
            {name: 'tableInfoNames',width:100,align:'left'},
            {name: 'content',width:150,align:'left'}
        ]
    }
});
