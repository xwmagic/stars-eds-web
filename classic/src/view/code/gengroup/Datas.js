Ext.define("App.view.classic.code.gengroup.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'id'}
            ,{ name:'name', type:'string', text:'模板名称'}
            ,{ name:'createName', type:'string', text:'创建人'}
            ,{ name:'createId', type:'string', text:'创建人id'}
            ,{name:"projectPack", type:"string", text:"项目地址"}
            ,{ name:'groupNote', type:'string', text:'备注'}
        ]
    }
});
