Ext.define("App.view.classic.code.gengroup.WindowEdit", { 
    extend: "App.view.classic.code.gengroup.Window",
    alias: "widget.classiccodegengroupWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'cgGroupCtl/update'}, 'close']
});
