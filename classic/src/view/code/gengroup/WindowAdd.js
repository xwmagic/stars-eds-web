Ext.define("App.view.classic.code.gengroup.WindowAdd", { 
    extend: "App.view.classic.code.gengroup.Window",
    alias: "widget.classiccodegengroupWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'cgGroupCtl/add'}, 'close']
});
