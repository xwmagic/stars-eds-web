Ext.define("App.view.classic.code.gengroup.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classiccodegengroupToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'classiccodegengroupWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'classiccodegengroupWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'cgGroupCtl/delById'}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
