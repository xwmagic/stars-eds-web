Ext.define("App.classic.view.code.Gen", { 
    extend: 'App.ux.BasePanel',
    xtype: 'viewcodeGen',
    requires: [ 
        "App.classic.view.code.GenForm",
        "App.classic.view.code.GenGrid",
        "App.classic.view.code.GenSearchForm",
        "App.classic.view.code.GenToolbar",
        "App.classic.view.code.GenViewModel",
        "App.classic.view.code.GenWindow",
        "App.classic.view.code.GenWindowAdd",
        "App.classic.view.code.GenWindowEdit"
    ],
    viewModel: 'viewcodeGenViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'viewcodeGenSearchForm'},
        {xtype: 'viewcodeGenToolbar'},
        {xtype: 'viewcodeGenGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('viewcodeGenToolbar');
            var form = th.down('viewcodeGenSearchForm');
            var grid = th.down('viewcodeGenGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
