Ext.define("App.view.classic.code.gentemp.Main", { 
    extend: 'App.ux.BasePanel',
    xtype: 'classiccodegentempMain',
    viewModel: 'classiccodegentempViewModel',
    layout: {type: 'vbox',align: 'stretch'},
    items: [
        {xtype: 'classiccodegentempSearchForm',hidden: false},
        {xtype: 'classiccodegentempToolbar',hidden: false},
        {xtype: 'classiccodegentempGrid', flex: 1}
    ],
    listeners: {
        beforerender: function (th) {
            var toolbar = th.down('classiccodegentempToolbar');
            var form = th.down('classiccodegentempSearchForm');
            var grid = th.down('classiccodegentempGrid');
            form._targetObject = grid;//给查询框关联操作对象
            toolbar._targetObject = grid;//给工具栏添加操作对象
        }
    }
});
