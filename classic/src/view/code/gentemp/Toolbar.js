Ext.define("App.view.classic.code.gentemp.Toolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'classiccodegentempToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'classiccodegentempWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'classiccodegentempWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'cgTemplateCtl/delById'}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
