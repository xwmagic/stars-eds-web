Ext.define("App.view.classic.code.gentemp.Form", {
    extend: "App.view.common.Form",
    xtype: 'classiccodegentempForm',
    _mainConfs: App.view.classic.code.gentemp.Datas.Main,
    defaults: {flex: 1},
    _validator: {//数据的验证，最大值，最小值在这里配置
        //orgName: {allowBlank: false}
        tempName: {allowBlank: false}
        , lastName: {allowBlank: false}
        , tempSuffix: {allowBlank: false}
        , codePack: {allowBlank: false}
    },
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: "id", maxLength: 36, hidden: true}
            ],
            [
                {name: "tempName", maxLength: 100}
                , {name:'codePack', maxLength: 200}
                , {name: "lastName", maxLength: 100,xtype:'commonTempTypeCombo'}
            ],
            [
                 {name: 'codePath'}
                , {name: "groupTypeName", maxLength: 36, xtype: 'classiccodegengroupPicker',keyName:'groupType'}
                ,{name: "tempSuffix", maxLength: 50, value: '.java'}
            ], [
                {name: "tempNote", maxLength: 100,flex:2}
                ,{name:"isEnable",xtype:'booleanYNC',comType:'radiogroup'}
            ],
            [
                {name: "tempCode", xtype: 'textarea', height: 600}
            ]
        ]
    }
});
