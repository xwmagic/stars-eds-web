Ext.define("App.view.classic.code.gentemp.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classiccodegentempGrid',
    _mainConfs: App.view.classic.code.gentemp.Datas.Main,
    url:'cgTemplateCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"id",hidden:true},
            {name:"tempName",hyperlinkMode:true,hyperlinkWindow:"classiccodegentempWindowView",width:180},
            {name:"tempNote"},
            {name:"lastName",valueToName:'commonTempTypeCombo',align:'center'},
            {name:"tempSuffix"},
            {name:"groupTypeName"},
            {name:"codePath"},
            {name:"codePack"},
            {name:"isEnable",width:110,valueToName:'booleanYNC',align:'center'},
            {name:"tempCode", flex: 1}
        ]
    }
});
