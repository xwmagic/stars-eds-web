Ext.define("App.view.classic.code.gentemp.Picker", { 
    extend: "App.view.common.GridPicker",
    xtype: 'classiccodegentempPicker',
    gridClassName:'App.view.classic.code.gentemp.GridMini',
    displayField: 'tempName',
    valueField:'id'
});
