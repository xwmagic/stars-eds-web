Ext.define("App.view.classic.code.gentemp.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classiccodegentempViewModel',
    data: {
        pageName: 'classiccodegentempMain',
        add: true,
        update: true,
        del: true
    }
});
