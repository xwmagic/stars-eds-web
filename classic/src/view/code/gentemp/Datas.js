Ext.define("App.view.classic.code.gentemp.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'id'}
            ,{ name:'tempName', type:'string', text:'模板名称'}
            ,{ name:'tempNote', type:'string', text:'模板备注'}
            ,{ name:'lastName', type:'string', text:'文件后缀'}
            ,{ name:'tempSuffix',type:'string', text:'文件类型'}
            ,{ name:'groupType', type:'string', text:'模板分组'}
            ,{ name:'groupTypeName', type:'string', text:'模板分组'}
            ,{ name:'tempCode', type:'string', text:'模板代码'}
            ,{ name:'codePath', type:'string', text:'项目模块'}
            ,{ name:'codePack', type:'string', text:'代码包名'}
            ,{ name:'isEnable', type:'string', text:'是否启用'}
        ]
    }
});
