Ext.define("App.classic.view.code.GenToolbar", {
    extend: "App.view.common.Toolbar",
    xtype: 'viewcodeGenToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c', actionObject: 'viewcodeGenWindowAdd'},//处理方法_create
            {name: 'u', actionObject: 'viewcodeGenWindowEdit'},//处理方法_update
            {name: 'd', actionUrl: 'myGenerate/delById'},//处理方法_delete，actionUrl配置删除的请求地址
            {name: 'copy', actionObject: 'viewcodeGenWindowAdd'},
            {
                name: 'codeGen', actionUrl: 'auto/generate', text: '生成后端代码',
                handler: function (btn) {
                    btn.up('viewcodeGenToolbar').codeGen(btn);
                }
            },
            {
                name: 'codeGenTable', actionUrl: 'auto/generateDb', text: '同步数据库',
                handler: function (btn) {
                    btn.up('viewcodeGenToolbar').codeGenTable(btn);
                }
            },
            {
                name: 'codeGenWeb', text: '生成前端代码',
                handler: function (btn) {
                    btn.up('viewcodeGenToolbar').codeGenWeb(btn);
                }
            },
            {
                name: 'codeGenWeb', text: '生成报表',
                handler: function (btn) {
                    btn.up('viewcodeGenToolbar').codeGenReport(btn);
                }
            }
        ]
    },
    codeGenWeb: function (btn) {
        var me = this;
        var grid = me._targetObject,data = {}, win;

        data = grid.getSelectOnlyOne();

        if(!data){
            return;
        }

        win = Ext.create('App.ux.CodeGenWindow',{
            codeDatas:data,
            _targetObject:grid
        });
        if(data.pageConfig){
            win.setValues(Ext.decode(data.pageConfig));
        }
        win.setBaseDatas(data);
        win.show();
    },
    codeGen: function (btn) {
        if (App && App.Application && App.Application.forbidRepeatClick) {
            App.Application.forbidRepeatClick(btn);
        }
        //拿到列表选中数据
        var me = btn.up('commontoolbar'),
            params = me._deleteValid(btn),
            url = btn.actionUrl;

        if (!params) {
            return;
        }

        Ext.MessageBox.confirm('确认生成', '您确认要生成代码吗?', function (btn) {
            if (btn == 'yes') {
                //父节判断
                App.ux.Ajax.request({
                    url: url,
                    params: {
                        ids: params.toString()
                    },

                    success: function (obj) {
                        var grid = me._targetObject;
                        App.ux.Toast.show('提示', '后端代码生成成功！','s');
                        grid._search();
                        //将接口地址加入URLS表中
                        Ext.create('App.view.classic.system.urls.Toolbar')._createUrls();
                    },
                    failure: function () {
                    }
                });

            }
        });

    },
    codeGenTable: function (btn) {
        if (App && App.Application && App.Application.forbidRepeatClick) {
            App.Application.forbidRepeatClick(btn);
        }
        //拿到列表选中数据
        var me = btn.up('commontoolbar'),
            params = me._deleteValid(btn),
            url = btn.actionUrl;

        if (!params) {
            return;
        }

        Ext.MessageBox.confirm('确认生成', '您确认要生成表吗?', function (btn) {
            if (btn == 'yes') {
                //父节判断
                App.ux.Ajax.request({
                    url: url,
                    params: {
                        ids: params.toString()
                    },

                    success: function (obj) {
                        var grid = me._targetObject;
                        App.ux.Toast.show('提示', '生成成功！','s');
                        grid._search();
                    },
                    failure: function () {
                    }
                });

            }
        });

    },
    codeGenReport:function () {
        
    }
    
});
