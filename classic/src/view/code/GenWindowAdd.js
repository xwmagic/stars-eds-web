Ext.define("App.classic.view.code.GenWindowAdd", { 
    extend: "App.classic.view.code.GenWindow",
    alias: "widget.viewcodeGenWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'myGenerate/add'}, 'close']
});
