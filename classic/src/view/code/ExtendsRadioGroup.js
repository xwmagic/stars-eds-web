/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.code.ExtendsRadioGroup', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.ExtendsRadioGroup',
	checkObject: {columns: 5},
    comType: 'radiogroup',//checkboxgroup/radiogroup/combo/combobox
	fieldsName: '',
	items: [
		{
			boxLabel  : 'BaseEntity',
			width: 120,
			inputValue: '1'
		}, {
			boxLabel  : 'BaseCompany',
			width: 140,
			inputValue: '2'
		}, {
			boxLabel  : 'BaseOrg',
			width: 120,
			inputValue: '3'
		}, {
			boxLabel  : 'BaseApp',
			width: 120,
			inputValue: '4'
		}
	]
});

