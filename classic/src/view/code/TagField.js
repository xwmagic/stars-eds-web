Ext.define("App.view.code.TagField", {
    extend: "App.view.BaseTagField",
    xtype: 'codeTagField',
    dynamicDataUrl:'myGenerate/list',//远程请求地址
    displayField: 'tableName',
    valueField:'id'
});
