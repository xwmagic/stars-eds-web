/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 *lt （less than）               小于
 le （less than or equal to）   小于等于
 eq （equal to）                等于
 ne （not equal to）            不等于
 ge （greater than or equal to）大于等于
 gt （greater than）            大于
 ————————————————
 */
Ext.define('App.view.code.OperatorCombo', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.codeOperatorCombo',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '=',
			width: 50,
			inputValue: '='
		}, {
			boxLabel  : 'like',
			width: 50,
			inputValue: 'like'
		}, {
			boxLabel  : '!=',
			width: 50,
			inputValue: '<>'
		}, {
			boxLabel  : '>',
			width: 50,
			inputValue: '&gt;'
		}, {
			boxLabel  : '<',
			width: 50,
			inputValue: '&lt;'
		}, {
			boxLabel  : '>=',
			width: 50,
			inputValue: '&gt;='
		}, {
			boxLabel  : '<=',
			width: 50,
			inputValue: '&lt;='
		}
	]
});

