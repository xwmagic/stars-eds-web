/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.classic.view.code.Grid',{
    extend: 'App.ux.code.Grid',
	xtype: 'viewcodeGrid',
    _mainConfs: App.classic.view.code.Datas.Fields,
    defaultDatas:[{type:'string',notEmpty:0,isQuery:1,operate:'=',length:100}],
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'name',width:150},
            {name: 'text',width:150},
            {name: 'type',width:100,align:'center',editor:{xtype:'commonDataType'}},
            {name: 'length',width:120,editor:{xtype:'numberfield',value:255}},
            {name: 'default',width:80},
            {name: 'isQuery',width:80,editor:{xtype:'booleanYNC'},valueToName:'booleanYNC',align:'center'},
            {name: 'operate',width:100,htmlEscape:true,editor:{xtype:'codeOperatorCombo'},align:'center'},
            {name: 'isExport',width:80,editor:{xtype:'booleanYNC'},valueToName:'booleanYNC',align:'center'},
            {name: 'pk',width:80,editor:{xtype:'booleanYNC'},valueToName:'booleanYNC',align:'center'},
            {name: 'notEmpty',width:80,editor:{xtype:'booleanYNC'},valueToName:'booleanYNC',align:'center'},
            {name: 'fieldValidType',width:110,htmlEscape:true,editor:{xtype:'codeFieldValidTypeCombo'},align:'center',valueToName:'codeFieldValidTypeCombo'},
            {name: 'dictTable',width:110,editor:{xtype:'classiccodePicker',listeners:{
                select:function (th,val,rec,d,e) {
                    th.up('viewcodeGrid').getColumnByName('dictText').getEditor().getStore().loadData(Ext.decode(rec.data.dataJson));
                }
            }}},
            {name: 'dictText',width:110,editor:{xtype:'classiccodeFieldCombo'}},
            {name: 'dictCode',width:110},
            {name: 'info',flex:1,htmlEscape:true,minWidth:110}
        ]
    },
    tools: [
        'multiEdit',
        {
            xtype: 'button', text: '选择删除字段',
            handler: function (btn) {
                var me = btn.up('uxcodeGrid');
                var win = Ext.create('Ext.window.Window',{
                    xtype: 'window',
                    layout: 'fit',
                    width: 1000,
                    height: 600,
                    items: [{xtype: 'uxcodeGrid'}],
                    buttons: [{
                        text: '确定', handler: function () {
                            var winGrid = this.up('window').down('uxcodeGrid');
                            var selections = winGrid.getSelectionModel().getSelection();
                            var datas = [];
                            for (var i = 0; i < selections.length; i++) {
                                datas.push(Ext.clone(selections[i].data));
                            }
                            winGrid.getStore().remove(selections);
                            me.historyRemove = winGrid.getDatas();
                            me.getStore().loadData(datas, true);
                            win.close();
                        }
                    }, {
                        text: '取消',
                        handler: function () {
                            win.close();
                        }
                    }]
                });
                win.down('uxcodeGrid').getStore().loadData(me.historyRemove);
                win.show();
            }
        },
        'plus', 'minus'
    ],
    isCanEdit: function (index, record) {
        //阶段不可编辑实际结束时间
        if (record.get('notEdit') === true) {
            return false;
        }
        return true;
    },
    /**
     * isEncode为ture将encode数据
     * @param isEncode
     * @returns {{}}
     */
    getVals:function () {
        var store = this.getStore();
        var values = [], me = this, temp;

        store.each(function(record,i){
            temp = Ext.copy({}, record.data,me._BaseDataFields);

            if(temp.id && Ext.isString(temp.id) &&temp.id.substring(0,4) == "extM"){
                temp.id = '';
            }
            //转换驼峰命名为数据库字段设置样式
            temp.name = Ext.Component.camelToField(temp.name);
            values.push(temp);

        });
        return values;
    }
});