Ext.define("App.classic.view.code.GenSearchForm", { 
    extend: "App.view.common.Form",
    xtype: 'viewcodeGenSearchForm',
    openEnterSearch:true,
    _mainConfs: App.classic.view.code.Datas.Gen,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'tableName'}, //配置需要查询的字段名
                {name: 'moduleName'}, //配置需要查询的字段名
                'sr' //s代表查询按钮，r代表重置按钮
            ]
        ]
    }
});
