Ext.define("App.view.classic.code.genconfig.Datas", { 
    statics: {
        Main: [
            { name:'id', type:'string', text:'id'}
            ,{ name:'genName', type:'string', text:'作者'}
            ,{ name:'codePath', type:'string', text:'后台路径'}
            ,{ name:'jsPath', type:'string', text:'前端路径'}
            ,{ name:'createName', type:'string', text:'创建人'}
            ,{ name:'createDate', type:'string', text:'创建时间'}
            ,{ name:'version', type:'string', text:'版本'}
            ,{ name:'lastUpdateName', type:'string', text:'修改人'}
            ,{ name:'lastUpdateDate', type:'string', text:'修改时间'}
            ,{ name:'configName', type:'string', text:'配制名称'}
            ,{ name:'tempId', type:'string', text:'模板组'}
            ,{ name:'tempName', type:'string', text:'模板组'}
            ,{ name:'genDefault', type:'string', text:'默认'}
        ]
    }
});
