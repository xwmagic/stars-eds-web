Ext.define("App.view.classic.code.genconfig.Grid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'classiccodegenconfigGrid',
    _mainConfs: App.view.classic.code.genconfig.Datas.Main,
    url:'cgConfigCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name:"id",hidden:true,hyperlinkMode:true,hyperlinkWindow:"classiccodegenconfigWindowView"},
            {name:"genName"},
            {name:"tempName"},
            {name:"codePath"},
            {name:"jsPath"},
            {name:"configName"},
            {name:"createDate"},
            {name:"createName"},
            {name:"lastUpdateName"},
            {name:"lastUpdateDate"},
            {name:"version"}
        ]
    }
});
