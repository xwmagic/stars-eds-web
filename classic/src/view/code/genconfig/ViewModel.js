Ext.define("App.view.classic.code.genconfig.ViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.classiccodegenconfigViewModel',
    data: {
        pageName: 'classiccodegenconfigMain',
        add: true,
        update: true,
        del: true
    }
});
