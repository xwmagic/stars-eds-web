Ext.define("App.view.classic.code.genconfig.WindowAdd", { 
    extend: "App.view.classic.code.genconfig.Window",
    alias: "widget.classiccodegenconfigWindowAdd",
    buttons: [{name: 'submit', actionUrl: 'cgConfigCtl/add'}, 'close']
});
