Ext.define("App.view.classic.code.genconfig.SearchForm", { 
    extend: "App.view.common.Form",
    xtype: 'classiccodegenconfigSearchForm',
    openEnterSearch:true,
    height:45,
    _mainConfs: App.view.classic.code.genconfig.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name:"genName",maxLength:100}
                ,"sr","ec"
            ],
            [
                {name:"codePath",maxLength:520}
            ],
            [
                {name:"jsPath",maxLength:520}
            ]
        ]
    }
});
