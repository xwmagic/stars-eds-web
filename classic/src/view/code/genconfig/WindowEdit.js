Ext.define("App.view.classic.code.genconfig.WindowEdit", { 
    extend: "App.view.classic.code.genconfig.Window",
    alias: "widget.classiccodegenconfigWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'cgConfigCtl/update'}, 'close']
});
