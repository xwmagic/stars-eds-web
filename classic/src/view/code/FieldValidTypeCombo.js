/**
 * 验证
 */
Ext.define('App.view.code.FieldValidTypeCombo', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.codeFieldValidTypeCombo',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '唯一',
			width: 50,
			inputValue: 'UNIQUE_CONSTRAINTS'
		}, {
			boxLabel  : '邮箱',
			width: 50,
			inputValue: 'EMAIL'
		}, {
			boxLabel  : '手机号',
			width: 50,
			inputValue: 'PHONE'
		}, {
			boxLabel  : '整数',
			width: 50,
			inputValue: 'INTEGER'
		}, {
			boxLabel  : '字母',
			width: 50,
			inputValue: 'ABC'
		}, {
			boxLabel  : '邮编',
			width: 50,
			inputValue: 'ZIP_CODE'
		}, {
			boxLabel  : '金额',
			width: 50,
			inputValue: 'MONEY'
		}, {
			boxLabel  : '自动编号',
			width: 50,
			inputValue: 'AUTO_CODE'
		}
	]
});

