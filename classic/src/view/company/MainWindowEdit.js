Ext.define("App.classic.view.company.MainWindowEdit", { 
    extend: "App.classic.view.company.MainWindow",
    alias: "widget.viewcompanyMainWindowEdit",
    buttons: [{name: 'submit', actionUrl: 'companyCtl/update'}, 'close']
});
