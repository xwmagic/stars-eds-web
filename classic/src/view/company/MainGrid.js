Ext.define("App.classic.view.company.MainGrid", { 
    extend: "App.view.common.CRUDGrid",
    xtype: 'viewcompanyMainGrid',
    _mainConfs: App.classic.view.company.Datas.Main,
    url:'companyCtl/pageList',//grid的查询地址
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'companyName'},
            {name: 'companyEn'},
            {name: 'companyCode'},
            {name: 'companyType'},
            {name: 'logo'},
            {name: 'layer'},
            {name: 'info'},
            {name: 'pid'},
            {name: 'sortNum'}
        ]
    }
});
