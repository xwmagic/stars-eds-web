Ext.define("App.classic.view.company.Datas", {
    statics: {
        Main:[
             {name: "id", type: "string", text: "主键ID"}
            ,{name:"companyName", type:"string", text:"公司名称"}
            ,{name:"companyEn", type:"string", text:"公司英文名称"}
            ,{name:"companyCode", type:"string", text:"公司代码"}
            ,{name:"companyType", type:"string", text:"公司类型"}
            ,{name:"logo", type:"string", text:"公司logo"}
            ,{name:"layer", type:"string", text:"层级"}
            ,{name:"info", type:"string", text:"描述"}
            ,{name:"pid", type:"string", text:"父公司"}
            ,{name:"sortNum", type:"string", text:"排序号"}
        ]
    }
});