Ext.define("App.classic.view.company.MainViewModel", { 
    extend: "App.view.BaseViewModel",
    alias: 'viewmodel.viewcompanyMainViewModel',
    data: {
        pageName: 'Main',
        'Main-btnCreate': true,
        'Main-btnUpdate': true,
        'Main-btnDelete': true
    }
});
