Ext.define("App.classic.view.company.MainSearchForm", { 
    extend: "App.view.common.Form",
    xtype: 'viewcompanyMainSearchForm',
    openEnterSearch:true,
    _mainConfs: App.classic.view.company.Datas.Main,
    _confs: {
        components: [//表单字段在这里配置
            [
                {name: 'companyType'}, //配置需要查询的字段名
                {name: 'companyName'}, //配置需要查询的字段名
                {name: 'companyCode'}, //配置需要查询的字段名
                'sr' //s代表查询按钮，r代表重置按钮
            ]
        ]
    }
});
