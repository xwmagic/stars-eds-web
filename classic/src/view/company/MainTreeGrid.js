Ext.define("App.classic.view.company.MainTreeGrid", {
    extend: "App.view.common.EditTreeGrid",
    xtype: 'companyMainTreeGrid',
    _mainConfs: App.classic.view.company.Datas.Main,
    url: 'companyCtl/getTree',//grid的查询地址
    _autoLoad: true,//是否自动加载
    isOneLoad:true,//一次性加载
    rowLines: false,//是否显示行线条
    rootVisible: false,
    readOnly: true,
    filterName: 'companyName',
    rownumberer: false,
    hideHeaders: true,
    isShowSearchBar: true,
    reserveScrollbar: false,
    forceNoCheckTree:true,

    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {
                name: 'companyName', xtype: 'treecolumn', flex: 1,
                renderer: function (v) {
                    if (!v) {
                        v = '';
                    } else {
                        v = Ext.Component.htmlEscape(v);
                    }
                    return v;
                }
            }
        ]
    }

});
