Ext.define("App.classic.view.company.MainToolbar", { 
    extend: "App.view.common.Toolbar",
    xtype: 'viewcompanyMainToolbar',
    _confs: {
        components: [//toolbar的组件在这里配置
            {name: 'c',actionObject:'viewcompanyMainWindowAdd'},//处理方法_create
            {name: 'u',actionObject:'viewcompanyMainWindowEdit'},//处理方法_update
            {name: 'd',actionUrl:'companyCtl/delById'}//处理方法_delete，actionUrl配置删除的请求地址
        ]
    }
});
