Ext.define('Admin.view.user.ResetPassword', {
    extend: 'Ext.button.Button',
    xtype: 'resetpassword',
	listeners: {
		click: function(th,e,opts){
			var  grid = this.up('grid');
			if(grid){
				var select = grid.getSelectionModel().getSelection();
				
				var params = {};
				if(!select || select.length < 1){
					Ext.Msg.alert('提示','请选中需要重置密码的用户！</br><span style="color:red;">*</span>支持多选');
					return;
				}
				Ext.MessageBox.confirm('重置确认', '共选中<span style="color:red;">'+select.length+'</span>行！</br>您是否确认重置选中用户的密码?',function(btn){
					if (btn == 'yes'){
						
						var arr = [];
						for(var i=0; i<select.length; i++){
							arr.push(select[i].id);
						}
						params.ids = arr.toString();
						LRAjax.request({
							url: th.myaction,
							params: params,

							success: function(obj) {
								Ext.Msg.alert('成功','重置密码成功！');
							}
						});
					}
				});
			}
		}
	}
});
