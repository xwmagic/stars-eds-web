Ext.define('Admin.view.authority.AuthorityGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'authoritygrid',
	plugins: {
		ptype: 'gridfilters',
		menuFilterText: '过滤'
	},
	initComponent: function(){
		this.columns = [{text: '角色名称', dataIndex: 'name',flex: 1}];
		this.setMyStore();
		this.setMyTools();
		this.callParent();
	},
	refreshDatas: function(params){
		var store
		,proxy
		;
		
		store = this.getStore();
		
		if(params){
			proxy = store.getProxy();
			proxy.extraParams=params;
		}
		
		store.load();
	},
	setMyTools: function(){
		var me = this;
		this.tools = [ {
			itemId: 'refresh',
			type: 'refresh',
			tooltip: '刷新',
			callback: function() {
				me.refreshDatas();
			}
		}];
	},
	setMyStore: function(){
		var me = this;
		this.store = Ext.create('Ext.data.Store', {
			fields:['id','name'],
			pageSize: 0,
			proxy: {
				type: 'ajax',
				url: this.lrAction,
				reader: {
					type: 'json',
					rootProperty: 'list'
				}
			},
			autoLoad: true
		});
		
		
		this.store.on({
			load: function(th, records, successful, operation, eOpts){
				if(successful){
					var selectModel = me.getSelectionModel();
					if(records[0]){
						selectModel.select(records[0]);
					}
					me.relationsRun(records[0]);
				}
			}
		});
		
	},
	relationsRun: function(record){
		var ath = this.up('actionauthority');
		var params = {
			'role.id' : record.get('id')
		};
		var has = ath.down('authoritytree[name=has]');
		var none = ath.down('authoritytree[name=none]');
		var name = record.get('name');
		
		has.params = params;
		none.params = params;
		
		has.setTitle('['+name+']-' + has.defaultTitle);
		none.setTitle('['+name+']-' + none.defaultTitle);
		
		has.refreshDatas();
		none.refreshDatas();
	}
});
