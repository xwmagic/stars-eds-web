Ext.define('Admin.view.authority.ActionAuthority', {
    extend: 'Ext.panel.Panel',
    xtype: 'actionauthority',
	layout: {
        type: 'hbox',
        align: 'stretch'
    },
	initComponent: function(){
		this.title = false;
		if(!this.lrAction){
			Ext.Msg.alert('提示','数据请求地址丢失！');
			return;
		}
		
		if(!this.menuConfig){
			Ext.Msg.alert('提示', '表的基本配置信息丢失！');
			return;
		}
		
		
		
		this.items = this.getMyItems();
		
		this.callParent();
	},
	getMyItems: function(){
		var urls = this.lrAction.split(',')
		,style = '-webkit-box-shadow:0 0 10px #666;  -moz-box-shadow: 0 0 10px #666;  box-shadow: 0 0 10px #666;'
		,me = this
		;
		
		if(!urls[1]){
			Ext.Msg.alert('提示','列表数据请求地址丢失！');
			return;
		}
		
		
		var items = [
			{
				xtype: 'authoritygrid',
				lrAction: urls[0],
				title: '功能权限管理',
				defaultTitle: '功能权限管理',
				width: 280,
				margin: '0 10 0 0',
				style: style,
				listeners: {
					itemclick: function(th, record, item, index, e, eOpts){
						this.relationsRun(record);
					}
				}
			},
			{
				xtype: 'authoritytree',
				title: '拥有的权限',
				defaultTitle: '拥有的权限',
				name: 'has',
				lrAction: urls[1],
				flex: 1,
				margin: '0 10 0 0',
				style: style
			},
			{
				xtype: 'authoritytree',
				title: '未分配的权限',
				defaultTitle: '未分配的权限',
				name: 'none',
				isAuthority: false,
				lrAction: urls[2],
				flex: 1,
				style: style
			}
		];
		
		return items;
	}
});
