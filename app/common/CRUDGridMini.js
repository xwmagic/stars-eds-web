/**
 * @author xiaowei
 * @date 2019年4月25日
 */
Ext.define('App.view.common.CRUDGridMini', {
    extend: 'App.view.common.CRUDGrid',
    xtype: 'commonCRUDGridMini',
    rownumberer:false,
    selType: false,
    _autoLoad:false,
    isShowSearchBar:true
});