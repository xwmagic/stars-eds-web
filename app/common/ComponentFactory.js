/**
 * @author xiaowei
 * @date 2017年4月25日
 */
Ext.define('App.view.common.ComponentFactory',{
	_cleanDataMapping:function(){
		if(this._confs && this._confs.dataMapping){
			var mc = this._confs.dataMapping;
			for(var i = 0; i<mc.length; i++){
				if(mc[i].name){
					mc[i].name = mc[i].name.replace(/\s/g,"");
				}
				if(mc[i].type){
					mc[i].type = mc[i].type.toLowerCase().replace(/\s/g,"");
				}
				/*if(mc[i].text){
					mc[i].text = mc[i].text.replace(/\s/g,"");
				}*/
			}
		}
	},
	
	_copyDataMapping: function(obj,confs){
		if(confs){
			if(this._conf && this._conf.dataMapping){
				obj._mainConfs = Ext.clone(this._confs.dataMapping);
			}
			obj._confs = Ext.apply({},confs);
		}
		
		return obj;
	}
	
	
});