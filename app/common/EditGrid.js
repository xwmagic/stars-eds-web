Ext.define('App.view.common.EditGrid',{
    extend: 'App.view.common.CRUDGrid',
	xtype: 'commoneditgrid',
	_autoLoad: false,//自动请求数据
	_isView: false,//是否是查看功能
    hasFileField:false,//编辑列表是否包含文件上传附件，有附件上传时必须设置为true，且column对象配置isFile为true
	isPageGrid: false,
	selType: false,
	sortableColumns: false,
	enableColumnResize: false,
    reserveScrollbar: true,
	
	initComponent: function(){
		if(!this._isView && !this.readOnly){
			this.plugins = this._getPlugins();
		}
		this.on({
			canceledit: function(editor, context){
				var valid = this._gridValid(context.record.data);
				if(!valid.valid){
					App.ux.Toast.show('提示', valid.message ,'i');
				}
			},
            beforecellclick: function (th, td, index, record) {
                //th.ownerGrid.getSelectionModel().select(record);
                return th.ownerGrid.isCanEdit(index, record);
            },
            itemclick:function (tableView) {
                var me = tableView.ownerCt;
                var viewModel = me.getViewModel();
                if(viewModel && me.selectControlButtons){
                    var btnControl = me.selectControlButtons(this.getSelectionModel().getSelection());
                    for(var i in btnControl){
                        viewModel.set(i, !btnControl[i]);
                    }
				}

            }
		});
		
		this.callParent();
	},

    _setTools:function () {
		//如果当前可编辑列表是只读状态,不生成tool工具
		if(this.readOnly){
			this.tools = null;
			return;
		}
		this.callParent();
    },
	
	_getPlugins: function(){
		var plugin;
		plugin = {
			ptype : 'cellediting',
			clicksToEdit : 1
		};
		if(this.plugins){
			plugin = Ext.applyIf(this.plugins, plugin);
		}
		return plugin;
	},
	
	_getColumns: function(){
		var colums = Ext.clone(this.callParent()),
		temp,i, col, j;
		
		for(i = 0; i<colums.length; i++){
			col = colums[i];
			if(col.columns){
				for(j = 0; j<col.columns[j].length; j++){
					this._autoEditor(col.colums[j]);
				}
			}else{
				this._autoEditor(col);
			}
		}
		return colums;
	},
	
	_autoEditor: function(col){
		var name = col.name;
		if(col.dataIndex){
    		name = col.dataIndex;
		}

		var temp = this._getFieldByName(name);

		if(temp && !col.forbidEdit && !col.hasOwnProperty('editor') && !this._isView){

			col.editor = {
				xtype: this._getXtype(temp.type)
			};
		}
		if(temp && temp.validator && col.editor){
            if(this.doNotValidEmpty && this.doNotValidEmpty[temp.name]){
                temp.validator.allowBlank = true;
            }
            if(col.editor.field){
                Ext.applyIf(col.editor.field, temp.validator);
            }else{
                Ext.applyIf(col.editor, temp.validator);
			}
		}
        if(col.editor && !col.forbidEdit && !col.hasOwnProperty('editEffect') && !this.readOnly){
			Ext.applyIf(col,{editEffect:true});
        }
        if(this.readOnly && col.hasOwnProperty('editEffect') && col.editEffect){
            col.editEffect = false;
            //Ext.applyIf(col,{editEffect:true});
		}
	},

    /**
     *
     * @param e 事件e
     * @param rowIndex 行号
     */
    /**
     * 可编辑列表中使用附件组件时，给每行的附件添加独立的name
     * @param e 事件e
     * @param attachmentName 存储附件标识的字段名
     * @param record 列表的record对象
     */
    addFileName:function (e,attachmentName,record) {
        //如果已经存在直接返回
        var name = record.get(attachmentName);
        //生成唯一附件标识
        var targetCmp = Ext.Component.fromElement(e.target);
        if(targetCmp && targetCmp.xtype == 'filebutton'){
            var filefield = targetCmp.up('filefield');
            record.file = filefield;
            filefield.record = record;
            //取控件DOM对象
            var field = document.getElementById(filefield.getId());
            //取控件中的input元素
            var inputs = field.getElementsByTagName('input');
            var il = inputs.length;
            //取出input 类型为file的元素
            for(var i = 0; i < il; i ++){
                if(inputs[i].type == 'file'){
                    if(!name){
                        inputs[i].name = inputs[i].name + '_' +  new Date().getTime();
                        record.set(attachmentName,inputs[i].name);
                    }else {
                        inputs[i].name = name;
                    }
                    break;
                }
            }
        }
    },

    getFileFields:function () {
		if(!this.hasFileField){
			return;
		}
    	var file = {
    		files:[],
            hiddens:[]
		};
        this.getStore().each(function (record, i) {
            if(record.file){
                file.files.push(record.file);
			}
        });
        return file;
    }

});