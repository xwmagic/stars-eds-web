/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.boolea.OKNG', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.booleanOKNG',
	checkObject: {value:'',columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '完成',
			width: 50,
			inputValue: 'OK'
		}, {
			boxLabel  : '未完成',
			width: 50,
			inputValue: 'NG'
		}
	],
    valueToName: function(value,metaData, record){
        switch (value){
            case "NG":
                return '<i class="fa fa-circle" style="color:red;" aria-hidden="false"></i>&nbsp;&nbsp;NG';
            case "OK":
                return '<i class="fa fa-circle" style="color:green;" aria-hidden="false"></i>&nbsp;&nbsp;OK';
			default:
				return value;
        }
    }
});

