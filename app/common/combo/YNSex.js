/**
 * 基本布尔值对象Y/N 男女
 * @author xiaowei
 * @date 2017年5月4日
 */
Ext.define('App.view.common.boolea.YNSex', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.booleanYNSex',
    comType: 'radiogroup',
	checkObject: {value: 1,columns: 2},
    statics:{
        Datas:{
            1:{text:'男',width:80,color:'#20d69e'},
            2:{text:'女',width:80,color:'#d34818'}
        }
    }
});