/**
 * 小区楼层
 * @author liguo01
 * @date 2017年6月7日
 */
Ext.define('App.view.common.combo.HouseLayer', {
    extend: 'App.view.BaseCombo',
    alias: 'widget.HouseLayer',
    editable:false,
    dynamicDataUrl:'houseLayerCtl/findHouseLayer',//远程请求地址
    displayField:'layer',
    valueField:"layer"
});