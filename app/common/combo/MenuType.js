/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.MenuType', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.commonMenuType',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '模块',
			width: 50,
			inputValue: 'menu'
		}, {
			boxLabel  : '菜单',
			width: 50,
			inputValue: 'child'
		}, {
			boxLabel  : '按钮',
			width: 50,
			inputValue: 'btn'
		}, {
			boxLabel  : 'URL',
			width: 80,
			inputValue: 'url'
		}
	],
    valueToName: function(value,metaData, record){
        var color = '#888';
        var name = '';
        value = value + '';
        switch (value){
            case "menu":
                color = '#641dce';
                name = '模块';
                break;
            case "child":
                color = '#1b83d6';
                name = '菜单';
                break;
            case "url":
                color = '#d6b533';
                name = 'URL';
                break;
            case "btn":
                color = '#43ce92';
                name = '按钮';
                break;
        }
        return Ext.Component.getColorView(name,color);
    }
});

