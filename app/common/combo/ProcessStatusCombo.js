/**
 * 审批流程状态
 * @author liguo01
 * @date 2017年6月7日
 */
Ext.define('App.view.common.combo.ProcessStatusCombo', {
    extend: 'App.view.common.BaseCheckGroup',
    alias: 'widget.processStatusCombo',
    checkObject: {value: '',columns: 7, editable: false},
    fieldsName: '',
    items: [
        {
            boxLabel  : '全部',
            width: 50,
            inputValue: ''
        },
        {
            boxLabel  : '草稿',
            width: 50,
            inputValue: '0'
        }, {
            boxLabel  : '待审',
            width: 50,
            inputValue: '1'
        }, {
            boxLabel  : '暂停',
            width: 50,
            inputValue: '2'
        }, {
            boxLabel  : '驳回',
            width: 50,
            inputValue: '3'
        }, {
            boxLabel  : '结束',
            width: 50,
            inputValue: '4'
        }, {
            boxLabel  : '废弃',
            width: 50,
            inputValue: '5'
        }
    ]
});