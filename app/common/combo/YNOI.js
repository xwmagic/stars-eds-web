/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.boolea.YNOI', {
    extend: 'App.view.common.BaseCheckGroup',
    alias: 'widget.booleanYNOI',
    checkObject: {
        columns: 2, 
        listeners: {
            change:function (th,val,oldVal) {

            }
        }
    },
    fieldsName: '',
    items: [
        {
            boxLabel: '内部',
            width: 50,
            inputValue: '0'
        }, {
            boxLabel: '外部',
            width: 50,
            inputValue: '1'
        }
    ],
    valueToName: function(value,metaData, record){
        var color = '#888';
        var name = '';
        value = value + '';
        switch (value){
            case "0":
                color = '#000';
                name = '内部';
                break;
            case "1":
                color = '#5e61dd';
                name = '外部';
                break;
            default:
                color = '#000';
                name = '内部';
                break;
        }
        return Ext.Component.getColorView(name,color);
    }
});

