/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.boolea.ARStatus', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.ARStatus',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : 'open',
			width: 80,
			inputValue: 'open'
		}, {
			boxLabel  : 'close',
			width: 80,
			inputValue: 'close'
		}
	],
	valueToName: function(value,metaData, record){
		var color = '#888';
		var name = '';
		if(!value){
			return;
		}
		value = value + '';
		switch (value){
			case "close":
				color = '#888';
				name = 'close';
				break;
			case "open":
				color = '#5e61dd';
				name = 'open';
				break;
			default:
				color = '#888';
				name = '无';
				break;
		}
		return Ext.Component.getColorView(name,color);
	}
});

