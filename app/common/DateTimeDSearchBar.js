Ext.define('Admin.view.history.DateTimeDSearchBar', {
    extend: 'Ext.panel.Panel',
    xtype: 'datetimedsearchbar',
	layout: 'hbox',
	width: 650,
	initComponent: function(){
		
		this.setMyTbar();
		
		this.callParent();
	},
	setMyTbar: function(){
		var me = this;
		this.tbar = [
			{
				xtype: 'lrdatetimefield',
				format:'Y-m-d H:i:s',
				fieldLabel: '开始时间',
				name: 'beginTime',
				labelWidth: 70,
				value: Ext.Date.clearTime(new Date())
			},
			{
				xtype: 'lrdatetimefield',
				format:'Y-m-d H:i:s',
				fieldLabel: '结束时间',
				name: 'endTime',
				labelWidth: 70,
				value: new Date()
			},
			{
				xtype: 'button',
				text: '查询',
				iconCls:'x-fa fa-search',
				handler: function(){
					me.searchRun();
				}
			}
		];
	},
	getParams: function(){
		var beginTime = this.down('lrdatetimefield[name=beginTime]').getValue()
			,endTime = this.down('lrdatetimefield[name=endTime]').getValue()
			;
		return {
			beginTime: beginTime,
			endTime: endTime
		};
	},
	searchRun: function(){
		var me = this 
			,grid = me.up('grid')
			,params = me.getParams()
			,store = grid.getStore()
			;
		var proxy = store.getProxy();
		proxy.extraParams[grid.objName + '.beginTime'] = params.beginTime;
		proxy.extraParams[grid.objName + '.endTime'] = params.endTime;
		store.load();
	}
	
});
