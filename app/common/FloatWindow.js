Ext.define('App.view.common.FloatWindow', {
    extend: 'Ext.window.Window',
	xtype: 'commonFloatWindow',
	// minHeight: 43,
	maxHeight: 0,
	// height: 43,
	height: 43,
	width: 240,
	draggable: false,
	closable: false,
	resizable: false,
	shadow : false,
	border: false,
	scrollMain: '',//包含滚动条的主体对象的
	sTargets: [],//保存滚动页签的位置值
	bodyStyle: {
		background: 'transparent'
	},
	style: {
		background: 'transparent',
		'border-width': '0px'
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	buttonItems: [
		
	],//菜单对象数组
	initComponent: function(){
		var childItems = this.createChildItems();
		this.width = childItems.length * 120;
		this.items = [{
			xtype: 'tabpanel',
			layout: 'fit',
			name: 'tab',
			height: this.height,
			tabBar: {
				name: 'tab',
				style: {
					background: 'transparent'
				},
				height: this.height,
				defaults: {
					height: this.height,
					minWidth: 100
				}
			},
			listeners: {
				beforerender: function(){
					var tab = this.down('tabbar[name=tab]');
					var btns = tab.query('button');
					for(var i=0; i<btns.length; i++){
						btns[i].setStyle({
							fontSize: '20px',
							fontWeight: 'bold'
						});
					}
				}
			},
			items: childItems
		}];
		this.callParent();
	},
	createChildItems: function(){
		var items = Ext.clone(this.buttonItems), me = this, scrollTarget,scroll, i,scrollY;

		if(!Ext.isEmpty(this.buttonItems)){
			var extendProperty = {
				listeners:{
					activate: function(){
                        scroll = me.scrollMain.getScrollable();
                        scrollTarget = me.scrollMain.down(this.downName);
                        scrollY = scroll.getPosition().y + scrollTarget.getY() - 100;
                        if(scrollY < 0){
                            scrollY = 1;
						}
                        scroll.scrollTo(null, scrollY);
					}
				}
			};
            for(i=0; i<items.length; i++){
                items[i] = Ext.apply(items[i],extendProperty);
            }
		}
		return items;
	},
	_showAt: function(width, height){
		if(this.width + 300 >= width){
			this.showAt(10,height-123);
		}else{
            this.showAt(200,height-123);
        }
        //解决ie下显示跳动的问题
        this.setMaxHeight(43);
    }
	
});