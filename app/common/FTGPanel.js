/**
 * @author xiaowei
 * @date 2017年4月25日
 */
Ext.define('App.view.common.FTGPanel', {
    extend: 'App.ux.BasePanel',
	mixins: ['App.view.common.CommonFun'],
	xtype: 'commonftgpanel',

    requires: [
		'App.view.common.Form',
		'App.view.common.Toolbar',
		'App.view.common.CRUDGrid',
		'App.view.common.Window'
    ],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	_confs: {
		dataMapping: [/*
			{name: 'name',type: 'string',text: '姓名'},
			{name: 'age',text: '年龄'},
			{name: 'email',type: 'string',text: '邮箱'},
			{name: 'date',type: 'date',text: '日期'},
			{name: 'core',text: '内核'}*/
		],
		form: {
			//components: [['core','age'],['email','date']]
		},
		toolbar: {
			
		},
		grid: {
			
		}
	},
	_form: 'commonform',
	_toolbar: 'commontoolbar',
	_grid: 'crudgrid',
	initComponent: function(){
		this._cleanDataMapping();
		this._initComp();
		this.callParent();
	},
	
	_cleanDataMapping:function(){
		if(this._confs && this._confs.dataMapping){
			var mc = this._confs.dataMapping;
			for(var i = 0; i<mc.length; i++){
				if(mc[i].name){
					mc[i].name = mc[i].name.replace(/\s/g,"");
				}
				if(mc[i].type){
					mc[i].type = mc[i].type.toLowerCase().replace(/\s/g,"");
				}
				/*if(mc[i].text){
					mc[i].text = mc[i].text.replace(/\s/g,"");
				}*/
			}
		}
	},
	
	_initComp: function(){
		var form = this._getForm(),
			grid = this._getGrid(),
			toolbar = this._getToolbar();
		form._targetObject = grid;
		toolbar._targetObject = grid;
		
		this.items = [
			form,
			toolbar,
			grid
		];
	},
	
	_getForm: function(){
		var tempObj = {
			ftgPanel: this,
			xtype: 'commonform',
			bodyStyle: {
				background:'#fafafa'
			},
			_mainConfs: this._confs.dataMapping
		};
		if(Ext.isString(this._form)){
			tempObj.xtype = this._form;
		}else if(Ext.isObject(this._form)){
			tempObj = Ext.applyIf(Ext.clone(this._form),tempObj);
		}
		
		if(this._confs.form){
			tempObj._confs = this._confs.form;
		}else{
			tempObj.hidden = true;
		}
		var obj = Ext.create(tempObj);
		this._formObj = obj;
		return obj;
	},
	_getToolbar: function(){
		var tempObj = {
			ftgPanel: this,
			xtype: 'commontoolbar',
			_mainConfs: this._confs.dataMapping
		};
		if(Ext.isString(this._toolbar)){
			tempObj.xtype = this._toolbar;
		}else if(Ext.isObject(this._toolbar)){
			tempObj = Ext.applyIf(Ext.clone(this._toolbar),tempObj);
		}
		
		if(this._confs.toolbar){
			tempObj._confs = this._confs.toolbar;
		}
		var obj = Ext.create(tempObj);
		this._toolbarObj = obj;
		return obj;
	},
	_getGrid: function(){
		var tempObj = {
			xtype: 'crudgrid',
			_mainConfs: this._confs.dataMapping,
			ftgPanel: this,
			flex: 1
		};
		
		if(Ext.isString(this._grid)){
			tempObj.xtype = this._grid;
		}else if(Ext.isObject(this._grid)){
			tempObj = Ext.applyIf(Ext.clone(this._grid),tempObj);
		}
		
		if(this._confs.grid){
			tempObj._confs = this._confs.grid;
		}
		var obj = Ext.create(tempObj);
		this._gridObj = obj;
		return obj;
	}

});
