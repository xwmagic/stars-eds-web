Ext.define('Admin.view.common.LRPassword', {
    extend: 'Ext.form.TextField',
    xtype: 'lrpassword',
	inputType: 'password',
	minLength: 4,
	//regex:/^(([a-zA-Z]+[0-9]+)|([-`=\\\[\];',./~!@#$%^&*()_+|{}:"<>?]+[0-9]+)|([-`=\\\[\];',./~!@#$%^&*()_+|{}:"<>?]+[a-zA-Z]+)|([a-zA-Z]+[-`=\\\[\];',./~!@#$%^&*()_+|{}:"<>?]+)|([0-9]+[-`=\\\[\];',./~!@#$%^&*()_+|{}:"<>?]+)|([0-9]+[a-zA-Z]+)).*$/i,
	regexText:'密码度复杂度不够！必须是[字母、数字、符号]的任意组合！',
	onTheSame: function(obj,newValue,pw,info){
		if(obj.isValid()  && pw.wasValid ){
			var val = pw.getValue();
			if(val !== newValue){//正则表达式验证通过，但是值不相等。
				info.setText('两次密码不一致!');
				info.setStyle('color', 'red');
				info.flag = false;
				return false;
			}else{
				info.setText('密码一致');
				info.setStyle('color', 'green');
				info.flag = true;
				return true;
			}
		}else{
			info.setText('');
			info.setStyle('color', 'black');
			info.flag = false;
		}
	}
});
