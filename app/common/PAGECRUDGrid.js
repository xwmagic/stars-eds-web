/**
 * 分页Grid
 */
Ext.define('App.view.common.PAGECRUDGrid',{
    extend: 'App.view.common.CRUDGrid',
	mixins: ['App.view.common.CommonFun'],
	xtype: 'pageCrudGrid',
    selType: false,
	isPageGrid: true
});	