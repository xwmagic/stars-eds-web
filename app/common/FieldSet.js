Ext.define('App.view.common.FieldSet',{
	extend: 'Ext.form.FieldSet',
	xtype: 'commonFieldSet',
	collapsible: true,
	layout: 'fit'
});