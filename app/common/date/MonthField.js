Ext.define('App.view.common.date.MonthField', {
    extend: 'Ext.form.field.Date',
    xtype: 'commondateMonthField',
    format: 'Ym',
    submitFormat:'Y-m-d',//默认提交格式
    createPicker: function () {
        var me = this,
            format = Ext.String.format;

        // Create floating Picker BoundList. It will acquire a floatParent by looking up
        // its ancestor hierarchy (Pickers use their pickerField property as an upward link)
        // for a floating component.
        return new Ext.picker.Month({
            pickerField: me,
            floating: true,
            preventRefocus: true,
            hidden: true,
            minDate: me.minValue,
            maxDate: me.maxValue,
            disabledDatesRE: me.disabledDatesRE,
            disabledDatesText: me.disabledDatesText,
            ariaDisabledDatesText: me.ariaDisabledDatesText,
            disabledDays: me.disabledDays,
            disabledDaysText: me.disabledDaysText,
            ariaDisabledDaysText: me.ariaDisabledDaysText,
            format: me.format,
            showToday: me.showToday,
            startDay: me.startDay,
            minText: format(me.minText, me.formatDate(me.minValue)),
            ariaMinText: format(me.ariaMinText, me.formatDate(me.minValue, me.ariaFormat)),
            maxText: format(me.maxText, me.formatDate(me.maxValue)),
            ariaMaxText: format(me.ariaMaxText, me.formatDate(me.maxValue, me.ariaFormat)),
            listeners: {
                scope: me,
                okclick: me.onSelect,
                monthdblclick: me.onSelect,
                yeardblclick: me.onSelect,
                cancelclick: function () {
                    this.collapse();
                },
                tabout: me.onTabOut
            },
            keyNavConfig: {
                esc: function () {
                    me.inputEl.focus();
                    me.collapse();
                }
            }
        });
    },
    onSelect: function (m, d) {
        var me = this;
        d = new Date(d[1],d[0],1);
        me.setValue(d);
        me.rawDate = d;
        me.fireEvent('select', me, d);

        me.onTabOut(m);
    }
});