Ext.define('App.view.common.date.DateRange', {
	extend: 'Ext.form.FieldContainer',
	xtype: 'commonDateRange',
	start: {},
	end: {},
	layout: 'hbox',
	initComponent: function(){
		this.initDate();
		this.callParent();
	},
	
	initDate: function(){
		var start = this.createStart();
		var end = this.createEnd();
		if(Ext.isObject(this.start)){
			start = Ext.applyIf(Ext.clone(this.start),start);
		}
		if(Ext.isObject(this.start)){
			end = Ext.applyIf(Ext.clone(this.end),end);
		}
		this.items = [start,end];
	},
	createStart: function(){
		return {
			xtype : 'datefield',
			width : 130,
			format : 'Y-m-d',
			editable : false,
			vtype : 'commondaterange',
			dateFlag : 'start',
			emptyText : '开始日期'
		};
	},
	createEnd: function(){
		return {
			xtype : 'datefield',
			width : 136,
			editable : false,
			format : 'Y-m-d',
			labelWidth: 1,
			fieldLabel: '~',
			labelSeparator : '',
			emptyText : '结束日期',
			vtype : 'commondaterange',
			dateFlag : 'end'
		};
	},
	getValue:function () {
		var start = this.down('datefield[dateFlag=start]');
		var end = this.down('datefield[dateFlag=end]');
		var value = {};
		value[start.name] = start.getValue();
		value[end.name] = end.getValue();
		return value;
    }
});