/**
 * 时间类型
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.pas.common.DateType', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.pascommonDateType',
	checkObject: {columns: 4},
    comType: 'radiogroup',//checkboxgroup/radiogroup/combo/combobox
    statics:{
	    Datas:{
	        1:{text:'按整月',width:80,color:'#3862c6'},
	        2:{text:'按日期',width:80,color:'#2ad699'}
        }
    }
});

