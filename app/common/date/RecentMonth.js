/**
 * 最近一个月组件
 * @author xiaowei
 * @date 2019年12月13日
 */
Ext.define('App.view.common.date.RecentMonth', {
    extend: 'App.view.common.BaseCheckGroup',
    alias: 'widget.dateRecentMonth',
    comType: 'radiogroup',//checkboxgroup/radiogroup/combo/combobox
    recentNum: 3,
    initComponent: function () {
        var val = {};
        val[this.fieldName]=this.getYearMonth(Ext.Date.add(new Date(), Ext.Date.MONTH, -1));
        this.checkObject= {
                columns: 3,
                value:val,
                listeners: {
                change: function (th, val) {
                    th.up('dateRecentMonth').change(val);
                }
            }
        };
        this.items = this.getMonthItems();

        this.callParent();
    },
    change: function (val) {//change事件处理方法

    },
    getYearMonth: function (date) {
        var year, month;
        year = date.getFullYear();
        month = date.getMonth() + 1;
        month = (month < 10 ? "0" + month : month);
        return year.toString() + month.toString();
    },
    getMonthItems: function () {
        var items = [],ym;
        for (var i = this.recentNum; i > 0; i--) {
            ym = this.getYearMonth(Ext.Date.add(new Date(), Ext.Date.MONTH, -i));
            items.push({
                boxLabel: ym,
                width: 80,
                inputValue: ym
            });
        }
        return items;
    }
});

