/**
 * 支持选择多附件，与fileFieldGrid组合使用
 * Created by csot.qhlinyunfu on 2018/12/24.
 */
Ext.define('App.view.common.file.FileFields', {
    extend: 'Ext.form.field.File',
    xtype: 'fileFields',

    /*
    accept: "application/zip,application/rar,text/xml,image/x-png,image/jpeg,image/gif,application/msword,application/pdf," +
    "application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet," +
    "application/vnd.openxmlformats-officedocument.presentationml.presentation," +
    "application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.wordprocessingml.document",//过滤文件类型 "application/zip,text/xml,image/!*",
    regex: /(txt)|(pdf)|(jpg)|(png)|(gif)|(doc)|(docx)|(xls)|(xlsx)|(zip)|(rar)|(JPG)|(PNG)|(ppt)|(pptx)$/i,
    regexText: '仅能上传txt、pdf、jpg、png、gif、zip、rar、doc、docx、ppt、xls和xlsx格式的文件',
    */
    buttonOnly: true,
    buttonConfig: {
        text: '添加附件',
        iconCls: 'fa fa-file-o fa-lg'
    },
    allowBlank:true,
    msgTarget: 'side',

    supportMultFn: function () {
        //2.1 为input添加支持多文件选择属性
        //var typeArray = ["application/zip", "text/*", "image/*"];
        // th = this.down('filefield');
        var th = this;
        var fileDom = th.getEl().down('input[type=file]');
        fileDom.dom.setAttribute("multiple", "multiple");
        fileDom.dom.setAttribute("accept", this.accept);
    },

    listeners: {
        change: function () {
            this.targetGridAddFile();
        },
        afterrender: function () {
            this.supportMultFn();
        }
    },

    //添加文件
    targetGridAddFile: function () {
        var fileDom = this.getEl().down('input[type=file]');
        var files = fileDom.dom.files;
        if (files.length > 0) {
            var fileArr = [], sumFileSize = 0;
            for (var i = 0; i < files.length; i++) {
                fileArr.push({
                    fileName: files[i].name,
                    fileSize: files[i].size
                });
                sumFileSize += files[i].size;
            }
            var grid = this.up('grid');
            //允许上传不大于20M的文件
            sumFileSize = grid._sumFileSize + sumFileSize;
            if (sumFileSize > 20971520*10) {
                Ext.Msg.alert('提示', '附件总大小只能上传小于200M！');
                this.reset();
                return;
            }
            grid._sumFileSize = sumFileSize;
            grid.getStore().insert(0, fileArr);
            var fileFields = Ext.create({
                xtype: 'fileFields',
                maxWidth:80,
                name: grid.name
            });
            this.hide();
            var dockedItems = grid.getDockedItems();
            dockedItems[0].insert(0, fileFields);
        }
    }
});