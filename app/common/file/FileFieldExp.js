Ext.define('App.view.common.file.FileFieldExp', {
	extend: 'Ext.form.FieldContainer',
	xtype: 'commonFileFieldExp',
	layout:'hbox',
	_recordId: '',//业务数据id
	attachMentId: '',//附件id，用于保存附件的id
	searchUrl:"sqm/base/findAttachNameFile",
	deleteUrl:"sqm/base/delFile",
	readOnly: false,
	orginValue:'',//初始值
	value:'',//默认值
	allowBlank: true,
	fileField: {
		xtype: 'commonFileField',
		hideLabel: true,
		width: 100,
		buttonOnly: true
	},
	initComponent: function(){
		this.items = this._createItems();
		this.on({
			beforerender: function(){
				if(this._recordId){
					this._setFileData();
				}
			}
		}); 
		
		this.callParent();
	},
    setAllowBlank: function(boolea){
        var item = this.items.items[0];
        item.allowBlank = boolea;
        if(this.fieldLabel && boolea != this.allowBlank){
            if(boolea === false){
                if(this.labelWidth){
                    this.labelWidth = this.labelWidth + 5;
                }
                this.setFieldLabel(this.fieldLabel + '<span style="color:red;font-weight:bold;">*</span>');

            }else{
                if(this.labelWidth){
                    this.labelWidth = this.labelWidth - 5;
                }
                var label = this.fieldLabel.split('<span')[0];
                this.setFieldLabel(label);

            }
        }
        this.allowBlank = boolea;
    },
	_createFileField: function(){
		var me = this;
		var fileField = Ext.clone(this.fileField);
		fileField.name = this.name;
		fileField.allowBlank = this.allowBlank;
		fileField.listeners = {
			change: function (cmp, value) {
				var fileName = me.down("container[name=attName]");
				fileName.removeAll();
                me.value = value;
				fileName.add({html: "<span style='color:blue'>" + value + "</span>"});
				me.down('button[name=close]').show();
			}
		};
		var filefield = Ext.create(fileField);
		return filefield;
	},
	_createItems: function(){
		var items, me = this;
		if(!this.readOnly){
			items = [
				this._createFileField(), {
					xtype: 'container',
					flex: 1,
					name: 'attName',
                    height: 24
				},{
					xtype: 'button',
					iconCls: 'fa fa-close',
					name: 'close',
					hidden: true,
                    tooltip: '删除附件',
                    tooltipType:'title',
					handler: function(btn){
						me.deleteFile(btn);
					}
				},{
					xtype:'hidden',
					name: this.attachMentId
				}
			];
		}else{
			items = [{
				xtype: 'container',
				flex: 1,
				name: 'attName',
                height: 24
			},{
				xtype:'hidden',
				name: this.attachMentId
			}];
		}
		return items;
	},
	setReadOnly: function(bool){
		var filefield = this.down('commonFileField');
		if(filefield){
			filefield.setReadOnly(bool);
		}
		
	},
    /**
     * 获取附件数据
     * @param id 当前数据id
     * @param attIdentify 当前附件标识
     * @returns {string} 附件对象数量JSON
     */
    findAttByIdAndIdentify:function (id,attIdentify) {
        var retdata='';
		if(!id || !attIdentify){
			return;
		}
        Ext.Ajax.request({
            url: this.searchUrl,
            params: {id: id,attIdentify:attIdentify},
            async:false,
            method: 'GET',
            success: function (response) {
                var data = response.responseText;
                if(data){
                    retdata = Ext.decode(data);
                }
            }
        });
        return retdata;
    },
	/**
     * 删除附件数据
     */
    deleteFile:function (btn) {
		var me = this;
		if(!this._recordId || !this.name){
			me.down('filefield').reset();
            me.value = "";
			me.down("container[name=attName]").removeAll();
			btn.hide();
			return;
		}
        
		Ext.MessageBox.confirm('删除确认', '确认要删除该附件吗?',function(isYes){
			if (isYes == 'yes'){
				App.ux.Ajax.request({
					url: me.deleteUrl,
					params: {
                        businessId: me._recordId,
                        identify:me.name
					},
					method: 'GET',
					success: function (response) {
						me.down('filefield').reset();
                        me.value = "";
						me.down("container[name=attName]").removeAll();
						btn.hide();
					}
				});
			}
		});
        
    },
    /**
     * 设置附件的url下载地址
     */
    _setFileData :function () {
        var data = this.findAttByIdAndIdentify(this._recordId, this.name);
		var fileName = this.down("container[name=attName]");
		var fileField = this.down("filefield");
        if(fileName){
            fileName.removeAll();
            if (!data) {
                fileName.add({html: "空"});
            } else {
                var id = data.id;
                var name = data.attName;
                var path = "sqm/base/downloadFile?id=" + id;
                this.orginValue = name;
                this.value = name;
                fileName.add({html: "<a href='" + path + "' ><i class='fa fa-cloud-download' aria-hidden='false'></i>" + name + "</a>"});
				fileName.attId = id;
				//this.down('hidden').setValue(id);
				if(fileField){
					fileField.allowBlank = true;
				}
				var btn = this.down('button[name=close]');
				if(btn){
					btn.show();
				}
            }
        }
    }
});