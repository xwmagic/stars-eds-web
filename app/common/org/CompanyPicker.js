/**
 * 公司类型
 * @author xiaowei
 * @date 2019年10月6日
 */
Ext.define('App.view.common.org.CompanyPicker', {
	extend: 'App.view.common.TreeMuiltiPicker',
	alias: 'widget.CompanyPicker',
    gridClassName:'App.classic.view.company.MainTreeGrid',
    checkModel:'single',//选择模式
    onlyLeaf:false,
    displayField: 'companyName',
    valueField: 'id'
});

