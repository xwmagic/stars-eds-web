/**
 * @author xiaowei
 * @date 2017年4月25日
 */
Ext.define('App.view.common.CommonFun', {
    //添加_validator验证规则给_mainConfs中的数据
    _addValidator: function () {
        var mc = Ext.clone(this._mainConfs);
        var validator = this._validator;
        if (mc && Ext.isObject(validator)) {
            for (var i = 0; i < mc.length; i++) {
                if (validator[mc[i].name]) {
                    mc[i].validator = validator[mc[i].name];
                }
            }
        }
        this._mainConfs = mc;
    },

    //通过名称name获取_mainConfs中的相匹配的数据对象
    _getFieldByName: function (name) {
        var mc = this._mainConfs;

        if (!mc || !name) {
            return;
        }

        if (!this._BaseData) {
            var BaseData = {},dataModel = {},dataFields = [];
            for (var i = 0; i < mc.length; i++) {
                if (!mc[i].type) {
                    mc[i].type = 'string';
                }
                mc[i].type = mc[i].type.toLowerCase();
                BaseData[mc[i].name] = mc[i];
                dataModel[mc[i].name] = '';
                dataFields.push(mc[i].name);
            }
            this._BaseData = BaseData;
            this._BaseDataModel = dataModel;
            this._BaseDataFields = dataFields;
        }

        return this._BaseData[name];
    },

    _getByteLength: function (str) {
        if (str == null) return 0;
        if (typeof str != "string") {
            str += "";
        }
        return str.replace(/[^\x00-\xff]/g, "01").length;
    },

    _getBaseUrl: function (ftgPanel) {
        if (ftgPanel && App && App.Application) {
            var className = Ext.getClassName(Ext.getClass(ftgPanel)).split('.');
            var url = App.Application.appName + '/' + className[className.length - 2];
            return url;
        }

    },

    _getXtype: function (type) {
        switch (type) {
            case 'int':
                return 'numberfield';
                break;
            case 'long':
                return 'numberfield';
                break;
            case 'double':
                return 'numberfield';
                break;
            case 'date':
                return 'datefield';
                break;
            case 'bigdecimal':
                return 'numberfield';
                break;
            case 'number':
                return 'numberfield';
                break;
            case 'float':
                return 'numberfield';
                break;
            case 'file':
                return 'commonFileFieldExp';
                break;
            case 'boolean':
                return 'booleanYNC';
                break;
            default:
                return 'textfield';
        }

    }
});