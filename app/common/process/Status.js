/**
 * 审批流程状态
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.process.Status', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.commonProcessStatus',
	checkObject: {value: '0'},
	items: [
        {
            boxLabel  : '全部',
            name      : 'processStatus',
            width: 50,
            inputValue: 'all'
        },{
			boxLabel  : '草稿',
			name      : 'processStatus',
			width: 50,
			inputValue: '0'
		}, {
			boxLabel  : '待审',
			name      : 'processStatus',
			width: 50,
			inputValue: '1'
		},{
			boxLabel  : '暂停',
			name      : 'processStatus',
			width: 50,
			inputValue: '2'
		},{
			boxLabel  : '驳回',
			name      : 'processStatus',
			width: 50,
			inputValue: '3'
		},{
			boxLabel  : '结束',
			name      : 'processStatus',
			width: 50,
			inputValue: '4'
		},{
			boxLabel  : '废弃',
			name      : 'processStatus',
			width: 50,
			inputValue: '5'
		}
	],
    valueToName: function(value,metaData, record){
        var color = '#888';
        var name = '';
        value = value + '';
		switch (value){
            case "0":
                color = '#000';
                name = '未审核';
                break;
            case "1":
                color = '#5e61dd';
                name = '审核中';
                break;
            case "2":
                color = '#cbcb00';
                name = '暂停中';
                break;
            case "3":
                color = '#000';
                name = '已驳回';
                break;
            case "4":
                color = '#1baa0a';
                name = '已结束';
                break;
            case "5":
                color = '#aaa';
                name = '已废弃';
                break;
            default:
                color = '#000';
                name = '未审核';
                break;
        }
        return Ext.Component.getColorView(name,color);
    }
});

