/**
 * 小区
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.view.common.boolea.HouseStatus', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.HouseStatus',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '全部',
			width: 50,
			inputValue: ''
		},{
			boxLabel  : '未扫',
			width: 80,
			inputValue: '0'
		}, {
			boxLabel  : '已完成',
			width: 80,
			inputValue: '1'
		}, {
			boxLabel  : '已入群',
			width: 80,
			inputValue: '2'
		}, {
			boxLabel  : '已绑定',
			width: 80,
			inputValue: '3'
		}, {
			boxLabel  : '忽略',
			width: 80,
			inputValue: '4'
		}
	],
    valueToName: function(value,metaData, record){
        var color = '#888';
        var name = '';
        value = value + '';
        switch (value){
            case "0":
                color = '#030622';
                name = '未扫';
                break;
            case "1":
                color = '#17d118';
                name = '已完成';
                break;
            case "2":
                color = '#d1c711';
                name = '已入群';
                break;
            case "3":
                color = '#2a56a1';
                name = '已绑定';
                break;
            case "4":
                color = '#6f8285';
                name = '忽略';
                break;
            default:
                color = '#030622';
                name = '未扫';
                break;
        }
        return Ext.Component.getColorView(name,color);
    }

});

