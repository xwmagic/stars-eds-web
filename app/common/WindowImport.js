Ext.define("App.view.common.WindowImport", {
    extend: "App.view.common.Window",
    alias: "widget.commonWindowImport",
    openValidation: true,
    isGetChangeValue:false,
    width: 500, // 窗口宽度
    bodyPadding: '15px',
    items: [
        {
            xtype: 'form',
            layout:'fit',
            isParam:true,
            items: [
                {
                    xtype: 'filefield',
                    fieldLabel: 'Excel导入',
                    name: 'file',
                    buttonText:'选择',
                    allowBlank: false
                }
            ]
        }
    ],
    buttons: [{name: 'submit'}, 'close'],
    _onSubmit: function(btn){
        var me = btn.up('commonwindow'), form,url;
        form = me.down('form');
        var params = me.getParam(),
            valid = me.isValid();
        if(!valid.valid){
            App.ux.Toast.show('提示',valid.message,'w');
            return;
        }
        if(!me._confs || !me._confs.url){
            throw "当前window的_confs属性必须提供可提交的地址，配置示例： _confs:{url:'user/importExcel'}"
        }
        url = URLPreName + me._confs.url;
        me._formSubmit(form,btn,url,params);
    }
});
