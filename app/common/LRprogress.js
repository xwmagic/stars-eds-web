/**
 * Created by lrwl on 8/20/2016.
 */
Ext.define('Admin.view.common.LRprogress', {
	extend: 'Ext.container.Container',
    xtype: 'lrprogress',
    
    width: 150,
    height: 20,
    reference: 'lrprogress',
    val: 0,
    /*
    Uncomment to give this component an xtype
    xtype: 'lrprogress',
    */
    items: [
	      /* include child components here */
	      {
	      	xtype: 'progress'
//	      	bind: {
//	      		value: '{lrprogress.val}'*0.01,
//		        text: '{lrprogress.val}'
//            }
	      }
	  ]
});