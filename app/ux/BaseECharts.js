/**
 * Echart图基类，使用：
 * { 
 * xtype:'baseECharts',
 * width:350,
 * height:350, 
 * option:{
 *    //详见echart官网
 * }
 * }
 */

Ext.define('App.ux.BaseECharts', {
    alias: 'widget.baseECharts',
    extend: 'Ext.container.Container',
    alternateClassName: 'Ext.form.Echarts',
    params: {},//请求参数对象
    title: '',
    subtext: '',
    seriesName: '',//制定series类名，默认折线图
    optionName: '',//基本的option配置类名
    option: {},//option自定义属性
    series: {},//series单体自定义属性
    url: '',//数据请求地址
    defaultParams: {},
    _autoLoad:true,
    showZoomNumber:5,//当x轴数据大于5组时，将出现dataZoom
    valueToName: '',//x轴key-name转换对象，通常用于键值对的状态类型进行转换
    setTitle:function (title) {
        if(!title){
            return;
        }
        if(this.option.title){
            this.option.title.text = title;
        }else {
            this.option.title = {
                text:title,
                x: 'center'
            }
        }
    },
    initComponent: function () {
        var me = this;
        this.defaultParams = Ext.clone(this.defaultParams);
        me.option = Ext.clone(this.option);
        me.series = Ext.clone(this.series);
        this.addChartConfigToOption();
        if(this._autoLoad){
            me.requestAjax();
        }
        this.setTitle(this.title);
        me.on('boxready', function () {
            me.setOption(me.option);
        });
        me.callParent(arguments);
        /*this.add({xtype:'button',iconCls:'x-fa fa-expand',style:{
            float:'right',
            'z-index':100
        },handler:function (btn) {

        }});*/
        me.on({
            resize: me.onResize,
            scope: me
        });
    },
    addChartConfigToOption: function () {
        if (!this.optionName) {
            return
        }
        var baseOption = Ext.create(this.optionName).create();
        if (this.option) {
            Ext.Component.depthCopyIf(this.option, baseOption);
        }
    },
    onRender: function () {
        var me = this;
        me.inputEl = document.createElement('div');
        document.body.appendChild(me.inputEl);
        me.echarts = echarts.init(me.inputEl,'macarons');
        if (me.option) {
            me.echarts.setOption(me.option);
        }
        me.echartsInnerId = Ext.id();
        me.inputEl.id = me.echartsInnerId;
        me.inputEl.style.height = "100%";
        me.inputEl.style.width = "100%";
        me.contentEl = me.echartsInnerId;
        me.callParent(arguments);
        me.rendered = true;
    },
    onResize: function (o, width, height) {
        var me = this;
        if (me.echarts) {
            me.inputEl.style.height = width;
            me.inputEl.style.width = height;
            me.echarts.resize();
        }
    },
    onDestroy: function () {
        var me = this;
        if (me.rendered) {
            try {
                Ext.EventManager.removeAll(me.echarts);
                for (prop in me.echarts) {
                    if (me.echarts.hasOwnProperty(prop)) {
                        delete me.echarts[prop];
                    }
                }
            } catch (e) {
            }
        }
        me.callParent();
    },
    setOption: function (option) {
        var me = this;
        if(me.echarts){
            me.echarts.setOption(option,true);
            me.echarts.resize();
        }

    },
    getOption: function () {
        var me = this;
        if (me.echarts)
            return me.echarts.getOption();
    },
    resize: function () {
        var me = this;
        me.echarts.resize();
    },
    /**
     * 移除图中的数据
     */
    removeDatas: function () {
        var option = this.getOption();
        if (!option) {
            return;
        }
        var series = option.series;
        if (!series) {
            return;
        }
        for (var i = 0; i < series.length; i++) {
            series[i].data = [];
        }
        this.setOption(option);
    },
    load: function (params) {
        this.requestAjax(params)
    },
    requestAjax: function (params) {
        if (!this.url) {
            console.log('requestAjax方法需要当前对象配置url请求地址！');
            return;
        }
        var me = this, param = this.defaultParams;
        if (params) {
            param = Ext.apply(this.defaultParams, params);
        }
        this.removeDatas();//首先清楚数据
        App.ux.Ajax.request({
            url: this.url,
            params: param,
            success: function (response) {
                me.requestSuccess(response.data);
            }
        });
    },
    /**
     *
     {
         title:'公司XX信息统计',
         list:[{
          name:'AA公司',
          id:'123',
          list:[
           {id:1,name:'未完成',value:1},
           {id:3,name:'已完成',value:5}
          ]
         },
         {
          name:'BB公司',
          id:'123',
          list:[
           {id:1,name:'未完成',value:1},
           {id:3,name:'已完成',value:5}
          ]
         }]
     }
     * @param data
     */
    requestSuccess: function (data) {
        if(!data || !data.list || data.list.length < 1){
            return;
        }

        var dataList = data.list;
        var option = this.option;
        option.series = [];
        //设置title
        this.setTitle(data.title);

        option.xAxis.data = this.parseAxisData(dataList[0].list);
        //如果数据量大于了showZoomNumber，那么出现缩放工具栏
        if(dataList[0].list && dataList[0].list.length > this.showZoomNumber && !this.option.dataZoom){
            this.option.dataZoom = [{
                type: 'slider'
            }];
        }

        if (this.seriesName) {
            var serie;
            for(var i in dataList){
                serie = this.createSeries();
                serie.data = dataList[i].list;
                serie.name = dataList[i].name;
                option.series.push(serie);
            }
        }

        this.setLegend(dataList);
        this.setOption(option);
    },
    parseAxisData:function (dataList) {
        var data = [];
        for(var i in dataList){
            data.push(dataList[i].name);
        }
        return data;
    },
    createSeries: function () {
        if (this.series) {
            return Ext.applyIf(Ext.clone(this.series), Ext.create(this.seriesName).create());
        }
        return Ext.create(this.seriesName).create();
    },

    setLegend: function (data) {
        var keyMap;
        if (this.valueToName) {
            keyMap = this.valueToName;
        }
        if (this.option.legend) {
            var text, datas = [];
            for (var i in data) {
                if(Ext.isObject(data[i])){
                    if(keyMap && keyMap[data[i].name].text){
                        text = keyMap[data[i].name].text ;
                    }else{
                        text = data[i].name;
                    }
                }else{
                    if(keyMap && keyMap[data[i]].text){
                        text = keyMap[data[i]].text ;
                    }else{
                        text = data[i];
                    }
                }
                datas.push(text);
            }

            this.option.legend.data = datas;
        }
    }
});