/**
 * 基本布尔值对象
 * @author xiaowei
 * @date 2017年5月2日
 */
Ext.define('App.ux.code.TreeKeyCombo', {
	extend: 'App.view.common.BaseCheckGroup',
	alias: 'widget.uxcodeTreeKeyCombo',
	checkObject: {columns: 2},
	fieldsName: '',
	items: [
		{
			boxLabel  : '显示名称',
			width: 50,
			inputValue: 'key'
		}, {
			boxLabel  : '值',
			width: 50,
			inputValue: 'value'
		}
	],
    valueToName: function(value,metaData, record){
        var color = '#888';
        var name = '';
        value = value + '';
        switch (value){
            case "menu":
                color = '#2a56a1';
                name = '显示名称';
                break;
            case "child":
                color = '#10880f';
                name = '值';
                break;
            default:
                color = '#888';
                name = '空';
                break;
        }
        return Ext.Component.getColorView(name,color);
    }
});

