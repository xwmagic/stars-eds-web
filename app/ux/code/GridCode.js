/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.GridCodeCheck',{
    extend: 'Ext.form.FieldSet',
	xtype: 'uxcodeGridCodeCheck',
    layout: 'column',
    defaults: {
        xtype: 'checkbox',
        margin: 10,
        columnWidth:0.1
    },
    items: [
        {
            boxLabel: 'Main',
            name: 'Main'
        }, {
            boxLabel: 'SearchForm',
            name: 'SearchForm'
        }, {
            boxLabel: 'Toolbar',
            name: 'Toolbar'
        }, {
            boxLabel: 'Datas.js',
            name: 'Datas'
        },{
            boxLabel: 'Grid',
            name: 'Grid'
        },{
            boxLabel: 'EditGrid',
            name: 'EditGrid'
        }, {
            boxLabel: 'Form',
            name: 'Form'
        }, {
            boxLabel: 'ViewModel',
            name: 'ViewModel'
        }, {
            boxLabel: 'Window',
            name: 'Window'
        }, {
            boxLabel: 'WindowAdd',
            name: 'WindowAdd'
        }, {
            boxLabel: 'WindowEdit',
            name: 'WindowEdit'
        }, {
            boxLabel: 'WindowView',
            name: 'WindowView'
        }, {
            boxLabel: 'GridMini',
            name: 'GridMini'
        }, {
            boxLabel: 'Picker',
            name: 'Picker'
        }, {
            boxLabel: 'Combo',
            name: 'Combo'
        }, {
            boxLabel: 'GridSelect',
            name: 'GridSelect'
        }, {
            boxLabel: 'SelectWindow',
            name: 'SelectWindow'
        }, {
            text: '全选',
            xtype:'button',
            name: 'allCheck',
            handler: function (th) {
                var checks = th.up('fieldset').query('checkbox');
                for(var i in checks){
                    checks[i].setValue(true);
                }
            }

        }, {
            text: '取消',
            xtype:'button',
            name: 'allCancel',
            handler: function (th) {
                var checks = th.up('fieldset').query('checkbox');
                for(var i in checks){
                    checks[i].setValue(false);
                }
            }
        }
    ]
});