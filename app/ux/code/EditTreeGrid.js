/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.EditTreeGrid',{
    extend: 'App.ux.code.Grid',
	xtype: 'uxcodeEditTreeGrid',
    cName:'树组件配置列表',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'name',width:150,align:'left'},
            {name: 'text',width:150,align:'left'},
            {name: 'length',width:120},
            {name: 'width',width:120},
            {name: 'component',width:300,editor:{xtype:'classicsystemcomponentsPicker'},align:'left'},
            {name: 'group',width:120},
            {name: 'flex',width:100},
            {name: 'hidden',width:100},
            {name: 'tree',width:100,editor:{xtype:'uxcodeTreeKeyCombo'}},
            {name: 'code',flex:1,align:'left'}
        ]
    }
});