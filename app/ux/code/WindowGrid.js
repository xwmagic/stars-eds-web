/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.WindowGrid', {
    extend: 'App.ux.code.Grid',
    cName: '弹窗组件配置',
    xtype: 'uxcodeWindowGrid',
    _mainConfs: App.view.ux.Datas.WindowGrid,
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'name'},
            {name: 'component', width: 200,editor:{xtype:'classicsystemcomponentsPicker'},align:'left'},
            {name: 'isParam', width: 200,valueToName:'booleanYNC',editor:{xtype:'booleanYNC'}},
            {name: 'autoSetSelect', width: 200,valueToName:'booleanYNC',editor:{xtype:'booleanYNC'}},
            {name: 'width'},
            {name: 'height'},
            {name: 'code', width: 150,flex: 1,align:'left'}
        ]
    },
    tools: [
        {
            xtype: 'button',
            text: '更新',
            name: 'update',
            margin: '0 5',
            tooltip: '根据上方表单配置进行更新',
            handler: function (btn) {
                btn.up('grid').updateDatas();
            }
        },
        {
            xtype: 'button', text: '历史字段',
            handler: function (btn) {
                var me = btn.up('grid');
                var win = Ext.create('Ext.window.Window', {
                    layout: 'fit',
                    width: 1000,
                    height: 600,
                    items: [{xtype: me.xtype}],
                    buttons: [{
                        text: '确定', handler: function () {
                            var winGrid = this.up('window').down('grid');
                            var selections = winGrid.getSelectionModel().getSelection();
                            var datas = [];
                            for (var i = 0; i < selections.length; i++) {
                                datas.push(Ext.clone(selections[i].data));
                            }
                            winGrid.getStore().remove(selections);
                            me.historyRemove = winGrid.getDatas();
                            me.getStore().loadData(datas, true);
                            win.close();
                        }
                    }, {
                        text: '取消',
                        handler: function () {
                            win.close();
                        }
                    }]
                });
                win.down('grid').getStore().loadData(me.historyRemove);
                win.show();
            }
        },
        'multiEdit','plus', 'minus'
    ],
    //根据配置的类名更新当前列表
    updateDatas: function () {
        var win = this.up('window');
        var values = this.up('window').down('form').getValues();
        var temp = this.getStore().findRecord('id', 'baseFrom');
        var allNames = this.getAllComponetNames(values);
        if(temp){
            temp.set('component',allNames.formXtype);
        }else{
            this.getStore().loadData([{id:'baseFrom',name: 'baseFrom', component: allNames.formXtype, isParam: 1, autoSetSelect: 1}],true);
        }
        temp = this.getStore().findRecord('id', 'details');
        if(temp){
            temp.set('component',allNames.xtypePreName+'infoEditGrid');
        }else{
            if(win.codeDatas.tableType === 'MAIN' && values.mainUrl){//添加默认规则的从表列表
                this.getStore().loadData([{id:'details',name: 'details', component: allNames.xtypePreName+'infoEditGrid', isParam: 1,autoSetSelect: true, height:400,code:"mainKeyName:'"+values.mainUrl+"Id',border:true,title:'明细列表'"}],true);
            }
        }

    }
});