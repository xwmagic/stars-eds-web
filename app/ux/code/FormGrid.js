/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.FormGrid',{
    extend: 'App.ux.code.Grid',
	xtype: 'uxcodeFormGrid',
    cName:'表单组件配置列表',
    _confs: {// 配置grid的columns,支持原生columns的所有写法
        columns: [
            {name: 'name',width:150,align:'left'},
            {name: 'type',width:80,align:'left'},
            {name: 'text',width:150,align:'left'},
            {name: 'length',width:110},
            {name: 'width',width:80},
            {name: 'component',width:300,editor:{xtype:'classicsystemcomponentsPicker'},align:'left'},
            {name: 'group',width:120},
            {name: 'flex',width:110},
            {name: 'hidden',width:110,valueToName:'booleanTF',editor:{xtype:'booleanTF'}},
            {name: 'notEmpty',width:120,valueToName:'booleanYNC',editor:{xtype:'booleanYNC'}},
            {name: 'code',flex:1,align:'left'}
        ]
    }
});