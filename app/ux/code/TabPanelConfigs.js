/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.TabPanelConfigs', {
    extend: 'Ext.tab.Panel',
    xtype: 'uxcodeTabPanelConfigs',
    layout: 'fit',
    defaults: {
        layout: 'fit'
    },
    items: [
        {
            title: 'Datas配置',
            items: [{
                xtype: 'uxcodeDatasGrid',
                name: 'dataGrid'
            }]
        }, {
            title: 'mainForm配置',
            items: [{
                xtype: 'uxcodeFormGrid',
                name: 'mainForm'
            }]
        }, {
            title: 'searchForm配置',
            items: [{
                xtype: 'uxcodeSearchGrid',
                name: 'searchFrom'
            }]
        }, {
            title: 'Grid配置',
            items: [{
                xtype: 'uxcodeGrid',
                name: 'mainGrid'
            }]
        }, {
            title: 'TreeGrid配置',
            items: [{
                hidden: true,
                xtype: 'uxcodeEditTreeGrid',
                name: 'mainEditTreeGrid'
            }]
        }, {
            title: 'Toolbar配置',
            items: [{
                xtype: 'uxcodeToolbarGrid',
                name: 'toolbarGrid'
            }]
        }, {
            title: 'Window配置',
            items: [{
                xtype: 'uxcodeWindowGrid',
                name: 'windowGrid'
            }]
        }
    ],
    //检查字段是否存在关联表，如果存在，自动生成关联字段。
    checkRelationTables:function (fields) {
        var newFields = [];
        for(var k in fields){
            if(fields[k].dictTable && fields[k].product_gg){
                newFields.push({
                    name:fields[k].name + 'Name'
                    ,text:fields[k].text + '名称'
                    ,type:"string"
                })
            }
        }
    },
    setNewBaseJsonConfigs: function (fields) {
        //fields = this.checkRelationTables(fields);
        this.down('[name=mainForm]').NewBaseJsonConfig = fields;
        this.down('[name=searchFrom]').NewBaseJsonConfig = fields;
        this.down('[name=mainGrid]').NewBaseJsonConfig = fields;
        this.down('[name=dataGrid]').NewBaseJsonConfig = fields;
        this.down('[name=mainEditTreeGrid]').NewBaseJsonConfig = fields;
        this.down('[name=toolbarGrid]').updateByJson();
        this.down('[name=windowGrid]').updateDatas();
    },
    updateAllGrids: function () {
        this.down('[name=mainForm]').updateByJson();
        this.down('[name=searchFrom]').updateByJson();
        this.down('[name=mainGrid]').updateByJson();
        this.down('[name=dataGrid]').updateByJson();
        this.down('[name=mainEditTreeGrid]').updateByJson();
        this.down('[name=toolbarGrid]').updateByJson();
        this.down('[name=windowGrid]').updateDatas();
    },
    checkAllGrids: function () {
        this.down('[name=mainForm]').checkDatasRight();
        this.down('[name=searchFrom]').checkDatasRight();
        this.down('[name=mainGrid]').checkDatasRight();
        this.down('[name=mainEditTreeGrid]').checkDatasRight();
    },
    getValues:function (values) {
        values.datasConfig = this.down('grid[name=dataGrid]').getDatas();
        values.mainFromConfig = this.down('grid[name=mainForm]').getDatas();
        values.searchFromConfig = this.down('grid[name=searchFrom]').getDatas();
        values.mainGridConfig = this.down('grid[name=mainGrid]').getDatas();
        values.mainEditTreeGridConfig = this.down('grid[name=mainEditTreeGrid]').getDatas();
        values.toolbarConfig = this.down('grid[name=toolbarGrid]').getDatas();
        values.windowConfig = this.down('grid[name=windowGrid]').getDatas();
        return values;
    },
    setValues:function (values) {
        this.down('[name=mainForm]').getStore().loadData(values.mainFromConfig ? values.mainFromConfig : []);
        this.down('[name=searchFrom]').getStore().loadData(values.searchFromConfig ? values.searchFromConfig : []);
        this.down('[name=mainGrid]').getStore().loadData(values.mainGridConfig ? values.mainGridConfig : []);
        this.down('[name=mainEditTreeGrid]').getStore().loadData(values.mainEditTreeGridConfig ? values.mainEditTreeGridConfig : []);
        this.down('[name=dataGrid]').getStore().loadData(values.datasConfig ? values.datasConfig : []);
        this.down('[name=toolbarGrid]').getStore().loadData(values.toolbarConfig ? values.toolbarConfig : []);
        if(values.windowConfig){
            this.down('[name=windowGrid]').getStore().loadData(values.windowConfig);
        }

    }
});