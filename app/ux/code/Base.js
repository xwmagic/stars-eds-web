/**
 * @author xiaowei
 * @date 2019年9月30日
 */
Ext.define('App.ux.code.Base',{
    tab1: "    ",
    tab2: "        ",
    tab3: "            ",
    tab4: "                ",
    getGridPropString: function (data) {
        var propString = 'name:"' + data.name + '",';

        propString = this.commonString(data, propString);

        if (data.tree === 'key') {
            propString += ',hyperlinkMode:true,hyperlinkWindow:"' + this.codeGen.windowViewXtype + '"';
        }

        return propString;
    },
    getTreeGridPropString: function (data) {
        var propString = 'name:"' + data.name + '",';

        if (data.tree === 'key') {
            propString += 'xtype:"treecolumn",';
            if(!data.width){//默认250
                propString += 'width:250,';
            }

        }else if (data.component) {
            propString += 'editor:{xtype:"' + data.component + '"},';
        }
        propString = this.commonString(data, propString);
        return propString;
    },
    getPropString: function (data) {
        var propString = 'name:"' + data.name + '",';
        if (data.component) {
            propString += 'xtype:"' + data.component + '",';
        }
        if (data.length) {
            propString += 'maxLength:' + data.length + ',';
        }
        propString = this.commonString(data, propString);
        return propString;
    },
    commonString: function (data, propString) {
        if (data.flex) {
            propString += 'flex:' + data.flex + ',';
        }
        if (data.width) {
            propString += 'width:' + data.width + ',';
        }
        if (data.hidden) {
            propString += 'hidden:' + data.hidden + ',';
        }
        if (data.code) {
            propString += data.code;
        }
        if (propString.charAt(propString.length - 1) === ',') {
            propString = propString.substring(0, propString.length - 1);
        }
        return propString;
    },
    addCompToForm: function (fields, codes, tab3, tab4) {
        var group = '';
        for (var i = 0; i < fields.length; i++) {
            if (i === 0) {
                codes.push(tab3 + '[');
                group = fields[i].group;
                codes.push(tab4 + "{" + this.getPropString(fields[i]) + "}");
            } else if (group !== fields[i].group || !fields[i].group) {
                codes.push(tab3 + '],');
                codes.push(tab3 + '[');
                group = fields[i].group;
                codes.push(tab4 + "{" + this.getPropString(fields[i]) + "}");
            } else {
                codes.push(tab4 + ",{" + this.getPropString(fields[i]) + "}");
            }
            if (i == fields.length - 1) {
                codes.push(tab3 + ']');
                continue;
            }
        }
    },
    createGridColumns: function (codes) {
        var tab3 = this.tab3;
        var cols = this.codeGen.fields;
        if (this.value.mainGridConfig && this.value.mainGridConfig.length > 0) {
            cols = this.value.mainGridConfig;
        }
        for (var i = 0; i < cols.length; i++) {
            if (i === cols.length - 1) {
                codes.push(tab3 + "{" + this.getGridPropString(cols[i]) + ",minWidth:100}");
            }else{
                codes.push(tab3 + "{" + this.getGridPropString(cols[i]) + "},");
            }

        }
    },
    getGridKeyData:function () {
        var cols = this.codeGen.fields;
        if (this.value.mainGridConfig && this.value.mainGridConfig.length > 0) {
            cols = this.value.mainGridConfig;
        }
        for (var i = 0; i < cols.length; i++) {
            if (cols[i].tree === 'key') {
                return cols[i];
                break;
            }
        }
    }
});