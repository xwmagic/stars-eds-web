Ext.define("App.ux.code.grid.Form", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.formAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.Form",');
        codes.push(tab1 + "xtype: '" + this.codeGen.formXtype + "',");
        codes.push(tab1 + '_mainConfs: ' + this.codeGen.dataName + ',');
        codes.push(tab1 + 'defaults: {flex: 1},');
        codes.push(tab1 + '_validator: {//数据的验证，最大值，最小值在这里配置');
        codes.push(tab2 + '//orgName: {allowBlank: false}');
        this.createValidator(codes);
        codes.push(tab1 + '},');
        codes.push(tab1 + '_confs: {');
        codes.push(tab2 + 'components: [//表单字段在这里配置');
        this.createFormComp(codes);
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');
        codes.push('});');
        return codes;
    },
    //添加非空验证信息
    createValidator:function (codes) {
        var tab2 = this.tab2,count = 0;
        var fields = this.codeGen.fields;
        if (this.value.mainFromConfig && this.value.mainFromConfig.length > 0) {
            fields = this.value.mainFromConfig;
        }
        for (var i = 0; i < fields.length; i++) {
            if(fields[i].notEmpty == '1'){
                //id忽略
                if(fields[i].name === 'id'){
                    continue;
                }
                if (count === 0) {
                    codes.push(tab2 + fields[i].name + ":{allowBlank:false}");
                }else{
                    codes.push(tab2 + "," + fields[i].name + ":{allowBlank:false}");
                }
                count++;
            }
        }
    },
    createFormComp: function (codes) {
        var tab3 = this.tab3, tab4 = this.tab4;
        var fields = this.codeGen.fields;
        if (this.value.mainFromConfig && this.value.mainFromConfig.length > 0) {
            fields = this.value.mainFromConfig;
        }
        /*codes.push(tab3 + '[');
        codes.push(tab4 + "{name: 'id',hidden:true}");
        codes.push(tab3 + '],');*/
        this.addCompToForm(fields, codes, tab3, tab4);

    }
});
