Ext.define("App.ux.code.grid.Toolbar", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var tab3 = this.tab3;
        var codes = [], name = this.codeGen.toolbarAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.Toolbar",');
        codes.push(tab1 + "xtype: '" + this.codeGen.toolbarXtype + "',");
        codes.push(tab1 + '_confs: {');
        codes.push(tab2 + 'components: [//toolbar的组件在这里配置');
        /*codes.push(tab3 + "{name: 'c',actionObject:'" + this.codeGen.windowAddXtype + "'},//处理方法_create");
        codes.push(tab3 + "{name: 'u',actionObject:'" + this.codeGen.windowEditXtype + "'},//处理方法_update");
        codes.push(tab3 + "{name: 'd',actionUrl:'" + this.value.delUrl + "'}//处理方法_delete，actionUrl配置删除的请求地址");
        this.addIsExportButton(codes);*/
        this.createButtons(codes);
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');
        codes.push('});');

        return codes;
    },
    /**
     * 添加导入导出按钮
     * @param codes
     */
    addIsExportButton:function (codes) {
        var tab3 = this.tab3;
        if (this.value.isExport) {
            codes.push(tab3 + ",{name: 'import',actionUrl:'" + this.value.importUrl + "'}//处理方法_import，actionUrl配置导入Excel的请求地址");
            codes.push(tab3 + ",{name: 'export',actionUrl:'" + this.value.exportUrl + "'}//处理方法_export，actionUrl配置导出Excel的请求地址");
        }
    },
    createButtons: function (codes) {
        var tab3 = this.tab3, tab4 = this.tab4;
        var fields,i,k,code = '';
        if (this.value.toolbarConfig && this.value.toolbarConfig.length > 0) {
            fields = this.value.toolbarConfig;
            for(i in fields){
                code = '';
                for(k in fields[i]){
                    if(k === 'id'){
                        code += "name:'" + fields[i][k] + "',";
                        if(fields[i][k] === 'c' && !fields[i].actionObject){
                            code += "actionObject:'" + this.codeGen.windowAddXtype + "',";
                        }else if(fields[i][k] === 'u' && !fields[i].actionObject){
                            code += "actionObject:'" + this.codeGen.windowEditXtype + "',";
                        }else if(fields[i][k] === 'copy' && !fields[i].actionObject){
                            code += "actionObject:'" + this.codeGen.windowAddXtype + "',";
                        }else if(fields[i][k] === 'd' && !fields[i].actionUrl){
                            code += "actionUrl:'" + this.value.delUrl + "',";
                        }else if(fields[i][k] === 'export' && !fields[i].actionUrl){
                            code += "actionUrl:'" + this.value.exportUrl + "',";
                        }else if(fields[i][k] === 'import' && !fields[i].actionUrl ){
                            code += "actionUrl:'" + this.value.importUrl + "',";
                        }
                    } else if(k === 'code'){
                        code += fields[i][k] + ",";
                    }else if(fields[i][k]){
                        code += k + ":'" + fields[i][k] + "',";
                    }
                }
                //移除最后多余的逗号
                code = code.substr(0,code.length-1);
                codes.push(tab3 + "{" + code + "},");
            }
            //移除最后多余的逗号
            codes[codes.length - 1] = codes[codes.length - 1].substr(0,codes[codes.length - 1].length-1);
        }
    }
});
