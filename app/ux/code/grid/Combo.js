Ext.define("App.ux.code.grid.Combo", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.packAllName + '.' + this.codeGen.ComboName;
        var keyData = this.getGridKeyData();
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.BaseCombo",');
        codes.push(tab1 + "xtype: '" + this.codeGen.xtypePreName + this.codeGen.ComboName + "',");
        codes.push(tab1 + "dynamicDataUrl:'" + this.codeGen.value.searchUrl + "',//数据的查询地址");
        codes.push(tab1 + "valueField: '"+keyData.name+"',");
        codes.push(tab1 + "displayField: '"+keyData.name+"'");
        codes.push('});');
        return codes;
    }
});
