Ext.define("App.ux.code.grid.GridSelect", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.packAllName + '.' +this.codeGen.GridSelectName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.CRUDGridMini",');
        codes.push(tab1 + "xtype: '" + this.codeGen.xtypePreName + this.codeGen.GridSelectName+ "',");
        codes.push(tab1 + '_mainConfs: ' + this.codeGen.dataName + ',');
        codes.push(tab1 + "url:'" + this.codeGen.value.searchUrl + "',//tree的查询地址");
        codes.push(tab1 + "excludeFieldName:'id',");
        codes.push(tab1 + 'isExcludeDatas:true,');
        codes.push(tab1 + '_confs: {// 配置grid的columns,支持原生columns的所有写法');
        codes.push(tab2 + 'columns: [');
        this.createGridColumns(codes);
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');

        codes.push('});');
        return codes;
    },
    getGridPropString: function (data) {
        var propString = 'name:"' + data.name + '",';

        propString = this.commonString(data, propString);

        return propString;
    }
});
