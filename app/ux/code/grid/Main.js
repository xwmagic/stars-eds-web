Ext.define("App.ux.code.grid.Main", {
    extend:'App.ux.code.Base',

    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1;
        var codes = [], name = this.codeGen.name;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + "extend: 'App.ux.BasePanel',");
        codes.push(tab1 + "xtype: '" + this.codeGen.xtypeName + "',");
        //this.createRequires(codes);
        codes.push(tab1 + "viewModel: '" + this.codeGen.viewModelXtype + "',");
        codes.push(tab1 + "layout: {type: 'vbox',align: 'stretch'},");
        this.createMainItems(codes);
        this.createMainListeners(codes);
        codes.push('});');

        return codes;
    },
    createRequires: function (codes) {
        var tab1 = this.tab1, tab2 = this.tab2;
        var temp = '', name = this.codeGen.packAllName+'.';
        codes.push(tab1 + 'requires: [ ');
        for (var i = 0; i < this.codeGen.fileNames.length; i++) {
            if (i == this.codeGen.fileNames.length - 1) {
                temp = '"' + name + this.codeGen.fileNames[i] + '"'
            } else {
                temp = '"' + name + this.codeGen.fileNames[i] + '",'
            }
            codes.push(tab2 + temp);
        }
        codes.push(tab1 + '],');
    },
    createMainItems: function (codes) {
        var tab1 = this.tab1, tab2 = this.tab2, hideSearch = false, hideToolbar = false;
        if (this.value.hideMainSearch) {
            hideSearch = true;
        }
        if (this.value.hideMainToolbar) {
            hideToolbar = true;
        }
        codes.push(tab1 + 'items: [');
        codes.push(tab2 + "{xtype: '" + this.codeGen.searchFormXtype + "',hidden: " + hideSearch + "},");
        codes.push(tab2 + "{xtype: '" + this.codeGen.toolbarXtype + "',hidden: " + hideToolbar + "},");
        codes.push(tab2 + "{xtype: '" + this.codeGen.gridXtype + "', flex: 1}");
        codes.push(tab1 + "],");
    },
    createMainListeners: function (codes) {
        var tab1 = "    ", tab2 = "        ", tab3 = "            ";
        codes.push(tab1 + 'listeners: {');
        codes.push(tab2 + 'beforerender: function (th) {');
        codes.push(tab3 + "var toolbar = th.down('" + this.codeGen.toolbarXtype + "');");
        codes.push(tab3 + "var form = th.down('" + this.codeGen.searchFormXtype + "');");
        codes.push(tab3 + "var grid = th.down('" + this.codeGen.gridXtype + "');");
        codes.push(tab3 + "form._targetObject = grid;//给查询框关联操作对象");
        codes.push(tab3 + "toolbar._targetObject = grid;//给工具栏添加操作对象");
        codes.push(tab3 + "grid._toolbar = toolbar;//给grid添加工具栏对象引用");
        codes.push(tab3 + "grid.actionControlLogic();//添加选择列表数据控制操作按钮的逻辑");
        codes.push(tab2 + '}');
        codes.push(tab1 + '}');
    }
});
