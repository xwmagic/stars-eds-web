Ext.define("App.ux.code.grid.Grid", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.gridAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.CRUDGrid",');
        codes.push(tab1 + "xtype: '" + this.codeGen.gridXtype + "',");
        codes.push(tab1 + 'openRowButtons:true,');
        codes.push(tab1 + 'columnsDefault:{autoLinefeed:true},');
        codes.push(tab1 + '_mainConfs: ' + this.codeGen.dataName + ',');
        codes.push(tab1 + "url:'" + this.codeGen.value.searchUrl + "',//grid的查询地址");
        codes.push(tab1 + '_confs: {// 配置grid的columns,支持原生columns的所有写法');
        codes.push(tab2 + 'columns: [');
        this.createGridColumns(codes);
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');

        codes.push('});');

        return codes;
    }
});
