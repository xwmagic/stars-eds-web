Ext.define('App.ux.grid.CodeGen', {
    requires: [
        'App.ux.code.grid.Main',
        'App.ux.code.grid.Form',
        'App.ux.code.grid.Grid',
        'App.ux.code.grid.SearchForm',
        'App.ux.code.grid.Toolbar',
        'App.ux.code.grid.ViewModel',
        'App.ux.code.grid.Window',
        'App.ux.code.grid.WindowAdd',
        'App.ux.code.grid.WindowEdit'
    ],
    fileNames: ["Form", "Grid", "SearchForm", "Toolbar", "ViewModel", "Window", "WindowAdd", "WindowEdit"],
    value: '',//填写参数对象
    fields: [],
    packAllName: '',
    dataName: '',
    name: '',
    className: '',
    xtypeName: '',

    formAllName: '',
    formName: 'Form',
    formXtype: '',

    baseGridAllName: '',
    baseGridName: 'BaseGrid',
    gridAllName: '',
    gridName: 'Grid',
    gridXtype: '',
    editGridAllName: '',
    editGridName: 'EditGrid',
    editGridXtype: '',
    GridMiniName: 'GridMini',
    pickerName: 'Picker',
    GridSelectName: 'GridSelect',
    SelectWindowName: 'SelectWindow',
    ComboName:'Combo',

    searchFormAllName: '',
    searchFormName: 'SearchForm',
    searchFormXtype: '',

    toolbarAllName: '',
    toolbarName: 'Toolbar',
    toolbarXtype: '',

    viewModelAllName: '',
    viewModelName: 'ViewModel',
    viewModelXtype: '',

    windowAllName: '',
    windowName: 'Window',
    windowXtype: '',

    windowAddAllName: '',
    windowAddName: 'WindowAdd',
    windowAddXtype: '',

    windowEditAllName: '',
    windowEditName: 'WindowEdit',
    windowEditXtype: '',

    windowViewAllName: '',
    windowViewName: 'WindowView',
    windowViewXtype: '',

    datasName: '',
    xtypePreName: '',

    tab1: "    ",
    tab2: "        ",
    tab3: "            ",
    tab4: "                ",
    init: function (name, fieldstr) {
        this.name = name;
        this.createFields(fieldstr);
        this.initName();
    },
    createFiles: function (value) {
        this.value = value;
        var me = this,
            path = value.path,
            type = value.type;
        if (value.Main) {
            this.submit(path, type, this.main(), this.className);
        }
        if (value[me.formName])
            setTimeout(function () {
                me.submit(path, type, me.form(), me.formName);
            }, 50);
        if (value[me.gridName])
            setTimeout(function () {
                me.submit(path, type, me.grid(), me.gridName);
            }, 100);
        if (value[me.searchFormName])
            setTimeout(function () {
                me.submit(path, type, me.searchForm(), me.searchFormName);
            }, 150);
        if (value[me.toolbarName])
            setTimeout(function () {
                me.submit(path, type, me.toolbar(), me.toolbarName);
            }, 200);
        if (value[me.viewModelName])
            setTimeout(function () {
                me.submit(path, type, me.viewModel(), me.viewModelName);
            }, 250);
        if (value[me.windowName])
            setTimeout(function () {
                me.submit(path, type, me.window(), me.windowName);
            }, 300);
        if (value[me.windowAddName])
            setTimeout(function () {
                me.submit(path, type, me.windowAdd(), me.windowAddName);
            }, 350);
        if (value[me.windowEditName])
            setTimeout(function () {
                me.submit(path, type, me.windowEdit(), me.windowEditName);
            }, 400);
        if (value["Datas"])
            setTimeout(function () {
                me.submit(path, type, me.createDatas(), "Datas");
            }, 450);
        if (value[me.GridMiniName])
            setTimeout(function () {
                me.submit(path, type, me.gridMini(), me.GridMiniName);
            }, 500);
        if (value[me.pickerName])
            setTimeout(function () {
                me.submit(path, type, me.picker(), me.pickerName);
            }, 550);
        if (value[me.windowViewName])
            setTimeout(function () {
                me.submit(path, type, me.windowView(), me.windowViewName);
            }, 600);
        if (value[me.SelectWindowName])
            setTimeout(function () {
                me.submit(path, type, me.windowSelect(), me.SelectWindowName);
            }, 650);
        if (value[me.GridSelectName])
            setTimeout(function () {
                me.submit(path, type, me.gridSelect(), me.GridSelectName);
            }, 700);
        if (value[me.ComboName])
            setTimeout(function () {
                me.submit(path, type, me.comboName(), me.ComboName);
            }, 750);
        if (value[me.editGridName])
            setTimeout(function () {
                me.submit(path, type, me.editGrid(), me.editGridName);
            }, 800);

        //调用自动扫描当前文件路径，并将组件添加到组件表中
        setTimeout(function () {
            var win = Ext.create('App.view.classic.system.components.WindowAdd');
            win.down('[name=path]').setValue(path);
            win._submit(win.down("button[name=submit]"));
            win.destroy();
        }, 1600);
    },
    //去除空格
    createFields: function (f) {
        var i;
        var value = f.replace(/\s+/g, "");
        if (value.substring(5).split('[').length > 0) {
            this.fields = eval(value);
        } else {
            this.fields = eval('[' + value + ']');
        }
    },
    initName: function () {
        var temp, name = this.name;
        var cNames = name.split('.');

        var packName = '', packAllName = '';
        for (var i = 0; i < cNames.length; i++) {
            if (i > 1 && i < cNames.length - 1) {
                packName = packName + cNames[i];
            }
            if (i == 0) {
                packAllName = cNames[i];
            } else if (i < cNames.length - 1) {
                packAllName = packAllName + '.' + cNames[i];
            }
        }

        this.packAllName = packAllName;
        this.xtypePreName = packName;
        var cName = cNames[cNames.length - 1];
        this.className = cName;
        this.dataName = packAllName + '.Datas.' + this.className;
        this.xtypeName = packName + cName;

        //生成form的类全名、类名、xtype名
        //this.formName = 'Form';
        this.formAllName = packAllName + '.' + this.formName;
        this.formXtype = packName + this.formName;

        //生成grid的类全名、类名、xtype名
        //this.gridName = 'Grid';
        this.baseGridAllName = packAllName + '.' + this.baseGridName;
        this.gridAllName = packAllName + '.' + this.gridName;
        this.gridXtype = packName + this.gridName;
        this.editGridAllName = packAllName + '.' + this.editGridName;
        this.editGridXtype = packName + this.editGridName;

        //生成searchForm的类全名、类名、xtype名
        //this.searchFormName = 'SearchForm';
        this.searchFormAllName = packAllName + '.' + this.searchFormName;
        this.searchFormXtype = packName + this.searchFormName;

        //生成toolbar的类全名、类名、xtype名
        //this.toolbarName = 'Toolbar';
        this.toolbarAllName = packAllName + '.' + this.toolbarName;
        this.toolbarXtype = packName + this.toolbarName;

        //生成toolbar的类全名、类名、xtype名
        //this.viewModelName = 'ViewModel';
        this.viewModelAllName = packAllName + '.' + this.viewModelName;
        this.viewModelXtype = packName + this.viewModelName;

        //生成window的类全名、类名、xtype名
        //this.windowName = 'Window';
        this.windowAllName = packAllName + '.' + this.windowName;
        this.windowXtype = packName + this.windowName;

        //生成windowAdd的类全名、类名、xtype名
        //this.windowAddName = 'WindowAdd';
        this.windowAddAllName = packAllName + '.' + this.windowAddName;
        this.windowAddXtype = packName + this.windowAddName;

        //生成windowAdd的类全名、类名、xtype名
        //this.windowEditName = 'WindowEdit';
        this.windowEditAllName = packAllName + '.' + this.windowEditName;
        this.windowEditXtype = packName + this.windowEditName;

        this.windowViewAllName = packAllName + '.' + this.windowViewName;
        this.windowViewXtype = packName + this.windowViewName;

        //生成Datas的类全名
        this.datasName = packAllName + '.Datas';

    },

    main: function () {
        return Ext.create('App.ux.code.grid.Main').createCode({value: this.value, codeGen: this});
    },
    form: function () {
        return Ext.create('App.ux.code.grid.Form').createCode({value: this.value, codeGen: this});
    },
    grid: function () {
        return Ext.create('App.ux.code.grid.Grid').createCode({value: this.value, codeGen: this});
    },
    editGrid: function () {
        return Ext.create('App.ux.code.grid.EditGrid').createCode({value: this.value, codeGen: this});
    },
    searchForm: function () {
        return Ext.create('App.ux.code.grid.SearchForm').createCode({value: this.value, codeGen: this});
    },
    toolbar: function () {
        return Ext.create('App.ux.code.grid.Toolbar').createCode({value: this.value, codeGen: this});
    },
    viewModel: function () {
        return Ext.create('App.ux.code.grid.ViewModel').createCode({value: this.value, codeGen: this});
    },
    window: function () {
        return Ext.create('App.ux.code.grid.Window').createCode({value: this.value, codeGen: this});
    },
    windowAdd: function () {
        return Ext.create('App.ux.code.grid.WindowAdd').createCode({value: this.value, codeGen: this});
    },
    windowEdit: function () {
        return Ext.create('App.ux.code.grid.WindowEdit').createCode({value: this.value, codeGen: this});
    },
    windowView: function () {
        return Ext.create('App.ux.code.grid.WindowView').createCode({value: this.value, codeGen: this});
    },
    gridMini: function () {
        return Ext.create('App.ux.code.grid.GridMini').createCode({value: this.value, codeGen: this});
    },
    picker: function () {
        return Ext.create('App.ux.code.grid.Picker').createCode({value: this.value, codeGen: this});
    },
    windowSelect: function () {
        return Ext.create('App.ux.code.grid.WindowSelect').createCode({value: this.value, codeGen: this});
    },
    gridSelect: function () {
        return Ext.create('App.ux.code.grid.GridSelect').createCode({value: this.value, codeGen: this});
    },
    comboName: function () {
        return Ext.create('App.ux.code.grid.Combo').createCode({value: this.value, codeGen: this});
    },
    createDatas: function () {
        var tab1 = this.tab1, tab2 = this.tab2;
        var tab3 = this.tab3;
        var codes = [];
        var fields = this.fields;
        if (this.value.datasConfig && this.value.datasConfig.length > 0) {
            fields = this.value.datasConfig;
        }
        codes.push('Ext.define("' + this.datasName + '", { ');
        codes.push(tab1 + "statics: {");
        codes.push(tab2 + this.className + ': [');
        for (var i = 0; i < fields.length; i++) {
            if (!fields[i]) {
                continue;
            }
            if (i === 0) {
                codes.push(tab3 + "{ name:'" + fields[i].name + "', type:'" + fields[i].type + "', text:'" + fields[i].text + "'}");
            } else {
                codes.push(tab3 + ",{ name:'" + fields[i].name + "', type:'" + fields[i].type + "', text:'" + fields[i].text + "'}");
            }

        }
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');
        codes.push('});');

        return codes;
    },

    submit: function (path, type, codes, className) {
        var re = /\\/gi;
        path = path.replace(re, "\\\\");
        Ext.Ajax.request({
            url: 'fileGenerate',
            method: 'post',
            params: {
                codes: codes,
                className: className,
                path: path,
                type: type
            },
            success: function (response, opts) {
                var obj = Ext.decode(response.responseText);
                if (obj.success) {
                    App.ux.Toast.show('成功', className+'生成前端代码成功！','s');
                    console.log('成功：'+className + ' 路径：'+path);
                }else {
                    App.ux.Toast.show('失败', className+'生成前端代码失败！','e');
                    console.log('失败：'+className + ' 路径：'+path);
                }
            },

            failure: function (response, opts) {
                App.ux.Toast.show('失败', className+'生成前端代码失败！','e');
                console.log('失败：'+className + ' 路径：'+path);
            }
        });

    }
});