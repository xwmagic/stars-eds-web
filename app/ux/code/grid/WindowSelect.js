Ext.define("App.ux.code.grid.WindowSelect", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.packAllName + '.' + this.codeGen.SelectWindowName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.classic.view.common.GridSelectWindow",');
        codes.push(tab1 + "xtype: '" + this.codeGen.xtypePreName + this.codeGen.SelectWindowName + "',");
        codes.push(tab1 + "formXtype: '"+this.codeGen.searchFormXtype+"',");
        codes.push(tab1 + "gridXtype: '" + this.codeGen.xtypePreName + this.codeGen.GridSelectName+ "'");
        codes.push('});');
        return codes;
    }
});
