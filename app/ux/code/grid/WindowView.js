Ext.define("App.ux.code.grid.WindowView", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1;
        var codes = [], name = this.codeGen.windowViewAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "' + this.codeGen.windowEditAllName + '",');
        codes.push(tab1 + 'alias: "widget.' + this.codeGen.windowViewXtype + '",');
        codes.push(tab1 + "title:'查看详细',");
        codes.push(tab1 + "_isView:true,");
        codes.push(tab1 + "buttons: ['close']");
        codes.push('});');

        return codes;
    }
});
