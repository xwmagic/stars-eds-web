Ext.define("App.ux.code.grid.Picker", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.packAllName + '.' + this.codeGen.pickerName;
        var keyData = this.getGridKeyData();
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.GridPicker",');
        codes.push(tab1 + "xtype: '" + this.codeGen.xtypePreName + this.codeGen.pickerName + "',");
        codes.push(tab1 + "gridClassName:'"+this.codeGen.packAllName + '.' + this.codeGen.GridMiniName+"',");
        codes.push(tab1 + "displayField: '"+keyData.name+"',");
        codes.push(tab1 + "valueField:'id'");
        codes.push('});');
        return codes;
    }
});
