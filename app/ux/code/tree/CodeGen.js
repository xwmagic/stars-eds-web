Ext.define('App.ux.tree.CodeGen', {
    extend: 'App.ux.grid.CodeGen',
    fileNames: ["TreeEditGrid", "SearchForm", "Toolbar", "TreeGridMini", "ViewModel", 'Picker'],
    gridName: 'TreeEditGrid',
    treeGridMiniName: 'TreeGridMini',
    main: function () {
        return Ext.create('App.ux.code.tree.Main').createCode({value: this.value, codeGen: this});
    },
    grid: function () {
        return Ext.create('App.ux.code.tree.TreeEditGrid').createCode({value: this.value, codeGen: this});
    },
    searchForm: function () {
        return Ext.create('App.ux.code.tree.SearchForm').createCode({value: this.value, codeGen: this});
    },
    toolbar: function () {
        return Ext.create('App.ux.code.tree.Toolbar').createCode({value: this.value, codeGen: this});
    },
    treeGridMini: function () {
        return Ext.create('App.ux.code.tree.TreeGridMini').createCode({value: this.value, codeGen: this});
    },
    picker: function () {
        return Ext.create('App.ux.code.tree.Picker').createCode({value: this.value, codeGen: this});
    },
    viewModel: function () {
        return Ext.create('App.ux.code.tree.ViewModel').createCode({value: this.value, codeGen: this});
    },
    createFiles: function (value) {
        this.value = value;
        var me = this,
            path = value.path,
            type = value.type;
        if (value.Main) {
            this.submit(path, type, this.main(), this.className);
        }
        if (value[me.gridName])
            setTimeout(function () {
                me.submit(path, type, me.grid(), me.gridName);
            }, 50);
        if (value[me.searchFormName])
            setTimeout(function () {
                me.submit(path, type, me.searchForm(), me.searchFormName);
            }, 100);
        if (value[me.toolbarName])
            setTimeout(function () {
                me.submit(path, type, me.toolbar(), me.toolbarName);
            }, 150);
        if (value[me.viewModelName])
            setTimeout(function () {
                me.submit(path, type, me.viewModel(), me.viewModelName);
            }, 200);
        if (value["Datas"])
            setTimeout(function () {
                me.submit(path, type, me.createDatas(), "Datas");
            }, 250);
        if (value[me.treeGridMiniName])
            setTimeout(function () {
                me.submit(path, type, me.treeGridMini(), me.treeGridMiniName);
            }, 300);
        if (value[me.pickerName])
            setTimeout(function () {
                me.submit(path, type, me.picker(), me.pickerName);
            }, 350);
        if (value[me.windowViewName])
            setTimeout(function () {
                me.submit(path, type, me.windowView(), me.windowViewName);
            }, 400);

        //调用自动扫描当前文件路径，并将组件添加到组件表中
        setTimeout(function () {
            var win = Ext.create('App.view.classic.system.components.WindowAdd');
            win.down('[name=path]').setValue(path);
            win._submit(win.down("button[name=submit]"));
            win.destroy();
        }, 1350);
    }
});