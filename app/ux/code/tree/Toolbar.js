Ext.define("App.ux.code.tree.Toolbar", {
    extend:'App.ux.code.grid.Toolbar',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var tab3 = this.tab3;
        var codes = [], name = this.codeGen.toolbarAllName;
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.TreeToolbar",');
        codes.push(tab1 + "xtype: '" + this.codeGen.toolbarXtype + "',");
        codes.push(tab1 + '_confs: {');
        codes.push(tab2 + 'components: [//toolbar的组件在这里配置');
        codes.push(tab3 + "'lock',//处理方法_create");
        codes.push(tab3 + "'c',//处理方法_create");
        codes.push(tab3 + "'addChild',");
        codes.push(tab3 + "'d',//处理方法_delete");
        codes.push(tab3 + "{name: 'save',actionUrl:'"+this.value.addUrl+"'}//处理方法_save");
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');
        codes.push('});');

        return codes;
    }
});
