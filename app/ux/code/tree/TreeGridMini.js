Ext.define("App.ux.code.tree.TreeGridMini", {
    extend:'App.ux.code.Base',
    createCode: function (obj) {
        this.value = obj.value;
        this.codeGen = obj.codeGen;
        var tab1 = this.tab1, tab2 = this.tab2;
        var codes = [], name = this.codeGen.packAllName + '.' + this.codeGen.treeGridMiniName;
        var keyData = this.getTreeKeyData();
        codes.push('Ext.define("' + name + '", { ');
        codes.push(tab1 + 'extend: "App.view.common.EditTreeGridMini",');
        codes.push(tab1 + "xtype: '" + this.codeGen.xtypePreName + this.codeGen.treeGridMiniName + "',");
        codes.push(tab1 + '_mainConfs: ' + this.codeGen.dataName + ',');
        codes.push(tab1 + "url:'" + this.codeGen.value.searchUrl + "',//tree的查询地址");
        codes.push(tab1 + "filterName: '"+keyData.name+"',");
        codes.push(tab1 + '_confs: {// 配置grid的columns,支持原生columns的所有写法');
        codes.push(tab2 + 'columns: [');
        this.createGridColumns(codes,keyData);
        codes.push(tab2 + ']');
        codes.push(tab1 + '}');

        codes.push('});');
        return codes;
    },
    getTreeKeyData:function () {
        var cols = this.codeGen.fields;
        if (this.value.mainEditTreeGridConfig && this.value.mainEditTreeGridConfig.length > 0) {
            cols = this.value.mainEditTreeGridConfig;
        }
        for (var i = 0; i < cols.length; i++) {
            if (cols[i].tree === 'key') {
                return cols[i];
                break;
            }
        }
    },
    createGridColumns: function (codes,keyData) {
        var tab3 = this.tab3;

        codes.push(tab3 + "{" + this.getTreeGridPropString(keyData) + "}");
    }
});
