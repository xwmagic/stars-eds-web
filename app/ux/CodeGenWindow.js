Ext.define("App.ux.CodeGenWindow", {
    extend: "Ext.window.Window",
    alias: "widget.uxCodeGenWindow",
    requires: [
        'App.ux.code.Base',
        'App.ux.CodeGen',
        'App.ux.grid.CodeGen',
        'App.ux.tree.CodeGen'

    ],
    title:'功能快速生成-灵活 快速 稳定 完善 美观',
    width: 1600, // 窗口宽度
    height: 850, // 窗口高度
    layout: 'fit',
    codeDatas: '',//主配置数据对象
    _targetObject: '',//主页面grid列表对象
    maximized: true,
    maximizable: true,
    items: [
        {
            xtype: 'form',
            bodyPadding: 10,
            scrollable: true,
            flex: 1,
            layout: 'column',
            defaults: {
                columnWidth: 0.33,
                margin: 5
            },
            defaultType: 'textfield',
            items: [
                {
                    fieldLabel: 'Ext类名',
                    name: 'classExtName',
                    allowBlank: false,
                    value: 'App.view.classic.'
                }, {
                    fieldLabel: '后端主URL',
                    name: 'mainUrl',
                    allowBlank: false,
                    listeners: {
                        change: function (th) {
                            var preName = th.getValue() + 'Ctl/';

                            var win = th.up('uxCodeGenWindow');
                            win.preUrlName = preName;
                            win.down('[name=searchUrl]').setValue(preName + 'pageList');
                            win.down('[name=addUrl]').setValue(preName + 'add');
                            if(win.codeDatas && win.codeDatas.tableType === 'MAIN'){//主表页面的新增编辑均采用add后缀地址
                                win.down('[name=editUrl]').setValue(preName + 'add');
                            }else {
                                win.down('[name=editUrl]').setValue(preName + 'update');
                            }
                            win.down('[name=delUrl]').setValue(preName + 'delById');
                            win.down('[name=exportUrl]').setValue(preName + 'exportList');
                            win.down('[name=importUrl]').setValue(preName + 'importExcel');
                        }
                    }
                }, {
                    fieldLabel: '查询URL',
                    name: 'searchUrl',
                    allowBlank: false
                }, {
                    fieldLabel: '新增URL',
                    name: 'addUrl'
                }, {
                    fieldLabel: '编辑URL',
                    name: 'editUrl'
                }, {
                    fieldLabel: '删除URL',
                    name: 'delUrl'
                },{
                    boxLabel: '是否包含导入导出功能',
                    margin:'0 0 0 110',
                    name: 'isExport',
                    hidden:true,
                    xtype:'checkbox',
                    listeners: {
                        change: function (th) {
                            var win = th.up('uxCodeGenWindow');
                            if (!th.getValue()) {
                                win.down('[name=exportUrl]').hide();
                                win.down('[name=importUrl]').hide();
                            } else {
                                win.down('[name=exportUrl]').show();
                                win.down('[name=importUrl]').show();
                            }

                        }
                    }
                },{
                    fieldLabel: '导出URL',
                    name: 'exportUrl',
                    hidden:true
                }, {
                    fieldLabel: '导入URL',
                    hidden:true,
                    name: 'importUrl'
                }, {
                    fieldLabel: '文件类型',
                    name: 'type',
                    value: 'js',
                    readOnly:true,
                    allowBlank: false
                }, {
                    fieldLabel: '生成路径',
                    name: 'path',
                    columnWidth: 0.66,
                    value:codeGenPathDef ? codeGenPathDef : '',
                    allowBlank: false
                },
                {
                    fieldLabel: '字段',
                    columnWidth: 0.8,
                    xtype: 'textarea',
                    name: 'field',
                    listeners: {
                        change: function (th) {
                            var value = th.getValue();
                            if (value.length < 10) {
                                return;
                            }
                            var datas;
                            if (value.substring(5).split('[').length > 0) {
                                datas = eval(value);
                            } else {
                                datas = eval('[' + value + ']');
                            }
                            for (var i = 0; i < datas.length; i++) {
                                if (datas[i].notEmpty) {
                                    datas[i].notEmpty = '1';
                                } else {
                                    datas[i].notEmpty = '0';
                                }
                            }
                            var win = th.up('uxCodeGenWindow');
                            if (win.down('grid[name=dataGrid]').getStore().getCount() === 0) {
                                win.down('grid[name=dataGrid]').getStore().loadData(Ext.clone(datas));
                            }
                            if (win.down('grid[name=mainForm]').getStore().getCount() === 0) {
                                win.down('grid[name=mainForm]').getStore().loadData(Ext.clone(datas));
                            }
                            if (win.down('grid[name=mainGrid]').getStore().getCount() === 0) {
                                win.down('grid[name=mainGrid]').getStore().loadData(Ext.clone(datas));
                            }
                            if (win.down('grid[name=mainEditTreeGrid]').getStore().getCount() === 0) {
                                win.down('grid[name=mainEditTreeGrid]').getStore().loadData(Ext.clone(datas));
                            }
                            if (win.down('grid[name=searchFrom]').getStore().getCount() === 0) {
                                win.down('grid[name=searchFrom]').loadQueryData(Ext.clone(datas));
                            }
                            win.setNewBaseJsonConfigs(datas);
                        }
                    }
                },
                {
                    xtype: 'button',
                    text: '全部更新',
                    columnWidth: 0.1,
                    tooltip: '更新当前配置当所有页面配置列表中',
                    handler: function () {
                        var textarea = this.up('form').down('textarea[name=field]');
                        var win = this.up('uxCodeGenWindow');
                        textarea.fireEvent('change', textarea);
                        setTimeout(function () {
                            win.updateAllGrids();
                        }, 20)
                    }
                },
                {
                    xtype: 'button',
                    text: '检查配置',
                    columnWidth: 0.1,
                    tooltip: '检查当前配置当所有页面配置列表中',
                    handler: function () {
                        var textarea = this.up('form').down('textarea[name=field]');
                        var win = this.up('uxCodeGenWindow');
                        textarea.fireEvent('change', textarea);
                        setTimeout(function () {
                            win.checkAllGrids();
                        }, 20)
                    }
                },
                {
                    xtype: 'uxcodeMainConfig', columnWidth: 1, title: 'main配置', name: 'main'
                },
                {
                    xtype: 'uxcodeGridCodeCheck', columnWidth: 1, title: 'Grid生成页面', name: 'gridCode'
                },
                {
                    xtype: 'uxcodeTreeEditCodeCheck',
                    title: 'TreeEditGrid生成页面',
                    columnWidth: 1,
                    hidden: true,
                    disabled: true,
                    name: 'treeEditCode'
                },
                {
                    xtype: 'uxcodeTabPanelConfigs',
                    height: 420,
                    columnWidth: 1
                }
            ],
            buttons: [{
                text: '帮助',
                handler: function () {
                    Ext.create({
                        xtype: 'window',
                        width: 600,
                        height: 500,
                        layout: 'fit',
                        items: [
                            {
                                xtype: 'image',
                                src: 'resources/images/CodeGenHelp.jpg'
                            }
                        ]
                    }).show();
                }
            }, {
                text: '重置',
                handler: function () {
                    this.up('form').getForm().reset();
                }
            }, {
                text: '暂存',
                handler: function () {
                    this.up('window')._ajaxSubmit();
                }
            }, {
                text: '生成代码',
                formBind: true,
                disabled: true,
                handler: function () {
                    var win = this.up('window');
                    var form = this.up('form').getForm();
                    var values = win.getValues();
                    var codeGen;
                    if (values.editTree) {
                        codeGen = Ext.create('App.ux.tree.CodeGen');
                    } else if (values.normalTree) {

                    } else {
                        codeGen = Ext.create('App.ux.grid.CodeGen');
                    }

                    codeGen.init(values.classExtName, values.field);

                    if (form.isValid()) {
                        win._ajaxSubmit();
                        codeGen.createFiles(values);
                    }

                }
            }, {
                text: '取消',
                handler: function () {
                    this.up('window').close();
                }
            }]
        }
    ],
    getValues: function () {
        var form = this.down('form').getForm();
        var values = form.getValues();
        values = this.down('uxcodeTabPanelConfigs').getValues(values);
        return values;
    },
    setNewBaseJsonConfigs: function (fields) {
        this.down('uxcodeTabPanelConfigs').setNewBaseJsonConfigs(fields);
    },
    updateAllGrids: function () {
        this.down('uxcodeTabPanelConfigs').updateAllGrids();
    },
    checkAllGrids: function () {
        this.down('uxcodeTabPanelConfigs').checkAllGrids();
    },
    setBaseDatas: function (data) {
        if (!data || !data.dataJson) {
            return;
        }
        var fields = eval(data.dataJson);
        for (var i = 0; i < fields.length; i++) {
            fields[i].name = Ext.Component.camelCase(fields[i].name)
        }
        this.setIsImport(data);
        this.down('[name=field]').setValue(JSON.stringify(fields));
        this.setNewBaseJsonConfigs(fields);

    },
    setIsImport:function (data) {
        if(data.isExport == 1){
            this.down('[name=isExport]').show();
            this.down('[name=isExport]').setValue(true);
        }else {
            this.down('[name=isExport]').hide();
            this.down('[name=isExport]').setValue(false);
        }
    },
    setValues: function (values) {
        this.down('form').getForm().setValues(values);
        this.down('uxcodeTabPanelConfigs').setValues(values);
    },
    _ajaxSubmit: function () {
        var me = this;
        var values = this.getValues();
        this.codeDatas.pageConfig = Ext.encode(values);

        App.ux.Ajax.request({
            url: 'myGenerate/update',
            params: this.codeDatas,

            success: function (response) {
                me._submitSuccess(response);
            },
            failure: function (response) {
                me._submitFailure(response);
            }
        });
    },
    _submitSuccess: function (response, options) {
        App.ux.Toast.show('提示', '页面配置保存成功！','s');
        if (this._targetObject) {
            this._targetObject._refresh();
        }
    },

    _submitFailure: function (btn, response, options) {

    }
});
