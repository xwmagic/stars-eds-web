Ext.define("App.view.ux.chart.base.OptionPie", {
    extend:'App.view.ux.chart.base.Object',
    baseConfig:{
        legend: {
            data: [],
            left:'center',
            bottom: true
        },

        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },

        visualMap: {
            show: false,
            min: 80,
            max: 600,
            inRange: {
                colorLightness: [0, 1]
            }
        },
        series : [

        ]
    }
});
