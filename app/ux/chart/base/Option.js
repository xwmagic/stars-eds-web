Ext.define("App.view.ux.chart.base.Option", {
    extend:'App.view.ux.chart.base.Object',
    baseConfig:{
        tooltip: {
            trigger: 'axis'
        },
        /*dataZoom: [/!*{
            type: 'inside'
        },*!/ {
            type: 'slider'
        }],*/
        legend: {
            //color: ["#F58080", "#47D8BE", "#F9A589"],
            data: [],
            left: 'center',
            bottom:true
        },
        grid: {
            top: 'middle',
            left: '1%',
            right: '1%',
            // bottom: '2%',
            height: '80%',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            axisLabel: {interval:0,rotate:15 },
            data: []
        },
        yAxis: {
            type: 'value',
            nameTextStyle: {
                color: "#999"
            }
        },
        series: []
    }
});
