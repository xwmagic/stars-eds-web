Ext.define("App.view.ux.chart.base.Object", {
    baseConfig:{},//基础配置
    config:{},//自定义可覆盖配置
    create:function () {
        if(!Ext.isObject(this.config)){
            return
        }
        var config = Ext.clone(this.config);
        Ext.Component.depthCopyIf(config,this.baseConfig);
        return config;
    }
});
