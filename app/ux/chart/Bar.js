Ext.define("App.view.ux.chart.Bar", {
    extend: "App.ux.BaseECharts",
    xtype: 'uxchartBar',
    optionName:'App.view.ux.chart.base.Option',
    seriesName:'App.view.ux.chart.series.Bar'
});
