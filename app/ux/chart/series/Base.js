Ext.define("App.view.ux.chart.series.Base", {
    extend:'App.view.ux.chart.base.Object',
    //基础配置
    baseConfig:{
        name: '',
        data: [],
        label: {
            normal: {
                show: true,
                position: 'inside'
            }
        },
        smooth: true
    },
    config:{}//自定义可覆盖配置
});
