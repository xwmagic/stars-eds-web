Ext.define("App.view.ux.chart.Pie", {
    extend: "App.ux.BaseECharts",
    xtype: 'uxchartPie',
    optionName:'App.view.ux.chart.base.OptionPie',
    seriesName:'App.view.ux.chart.series.Pie',
    requestSuccess:function (values) {
        var option = this.option;
        option.series = [];
        var seriesData = values.seriesData;
        var xaxisData = values.xaxisData;
        var keyMap;
        if(this.valueToName){
            keyMap = this.valueToName;
        }
        var text,datas = [];
        for (var i in xaxisData) {
            if(keyMap){
                text= keyMap[Number(xaxisData[i])].text;
            }else {
                text = xaxisData[i];
            }
            datas.push({
                value:seriesData[0].data[i],
                name:text
            });
        }
        if(this.seriesName){
            var serie = this.createSeries();
            serie.data = datas;
            option.series.push(serie);
        }
        this.setLegend(xaxisData);
        this.setOption(option);
    }
});
