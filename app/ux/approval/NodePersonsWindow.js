/**
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.NodePersonsWindow',{
    extend: 'Ext.window.Window',
	xtype: 'approvalNodePersonsWindow',
	modal: true,
	closable: true,
	/*data为节点信息，格式如下
	[
		{
			activityId:"p-002",
			activityName:"部门领导审批",
			activityType:"userTask",
			isAllowSetAssignee:null,
			setAssigneeList:null
		}
	]
	*/
	data:'',
	width: 600,
	height: 600,
	layout: 'fit',
	initComponent: function(){
		
		this.items = [
			{
				xtype: 'form',
				name: 'main',
				layout: 'anchor',
				defaults: {
					anchor: '100%'
				},
				bodyPadding: 10,
				items: this.createFormItems()
			}
		];
		this.callParent();
	},
	createFormItems: function(){
		var data = this.config.data, items = [], i, maxLength = 0,len,  name, 
			height = data.length * 32 + 100;
			
		this.setHeight(height);
		
		if(!Ext.isEmpty(data)){
			for(i=0; i<data.length; i++){
				name = data[i].activityName + '( '+ data[i].activityId + ')';
				len = this._getByteLength(name) * 7.5;
				if(len > maxLength ){
					maxLength = len;
				}
				items.push({
					xtype: 'memberMulitSelectField',
					labelWidth: 150,
					//labelAlign:'right',
					allowBlank: false,
					emptyText: '请选择处理人',
					fieldLabel:  name,
					name: data[i].activityId
				});
			}
			for(i=0; i<items.length; i++){
				items[i].labelWidth = maxLength;
			}
			
		}
		return items;
	},
	buttons: [
		{
			text: '提交',
			name: 'confirm',
			handler: function(btn){
				btn.up('window').onConfirm(btn);
			}
		}
	],
	
	getValues: function(){
		var form = this.down('form[name=main]');
		if(!form.isValid()){
			return;
		}
		var values = form.getForm().getValues(), res = {};
		for(var id in values) {
			res[id] = values[id].join(",");
		}
		var object = {'setAssignees':res};
		return object;
	},
	
	onConfirm: function(btn){
		this.close();
	},
	
	_getByteLength : function(str) {
		if (str == null) return 0;
		if (typeof str != "string"){
			str += "";
		}
		return str.replace(/[^\x00-\xff]/g,"01").length;
	}
	
});