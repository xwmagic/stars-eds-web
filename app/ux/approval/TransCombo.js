/**事务选择下拉框
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.TransCombo',{
    extend: 'Ext.form.ComboBox',
	requires: [
		'App.ux.approval.store.TransComboStore'
	],
	xtype: 'approvalTransCombo',
	width: 300,
	margin: '0 10 0 0',
	forceSelection: true,
	queryMode: 'local',
	displayField: 'transName',
	valueField: 'transId',
	initComponent: function() {
		this.store = this._createStore();
		
		this.callParent();	
	},
	_createStore: function(){
		var me = this, 
			params = '',
			url = this.url;
		
		if(this.defaultParams){
			params = Ext.clone(this.defaultParams);
		}
		
		var store = Ext.create('App.ux.approval.store.TransComboStore',{autoLoad : false});
		if(this.defaultDatas){
			store.loadData(Ext.clone(this.defaultDatas));
		}
		
		return store;
	}
	
});