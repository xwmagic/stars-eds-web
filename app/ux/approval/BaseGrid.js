/**
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.BaseGrid',{
    extend: 'Ext.grid.Panel',
	xtype: 'approvalBaseGrid',
	_autoLoad: true,//自动请求数据
	openValidation: false,
	//sortableColumns: false,
	enableColumnMove: false,
	enableColumnHide: false,
	columnLines : true,
	isPageGrid: true,
	defaultDatas:'',//默认数据
	viewConfig : {
        enableTextSelection : true
    },
	autoScroll : true,
	//plugins: 'gridfilters',
	
	initComponent: function() {
		this.store = this._createStore();
		this.columns = this._createColumns();
		this._setPagingToolBar();
		this.callParent();	
	},
	
	_createStore: function(){
		var me = this, 
			className,
			params = '',
			url = this.url;
		
		if(this.defaultParams){
			params = Ext.clone(this.defaultParams);
		}
		
		var store = Ext.create('Ext.data.Store',{autoLoad : false});
		
		if(this.defaultDatas){
			store.loadData(Ext.clone(this.defaultDatas));
		}
		return store;
	},
	
	_createColumns: function(){
		var columns = [
			
		];
		return columns;
	},
	
	_setPagingToolBar: function(){
		if(this.isPageGrid){
			this.bbar = {
				xtype: 'pagingtoolbar',
				displayInfo: true,
				store: this.store,
				displayMsg: '当前显示 {0} - {1} 条,总数 {2}条',
				emptyMsg: "没有数据"
			};
		}
	}
});