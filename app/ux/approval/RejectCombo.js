/**驳回节点下拉框
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.RejectCombo',{
    extend: 'Ext.form.ComboBox',
	requires: [
		'App.ux.approval.store.RejectComboStore'
	],
	xtype: 'approvalRejectCombo',
	width: 300,
	margin: '0 10 0 0',
	forceSelection: true,
	queryMode: 'local',
	displayField: 'activityName',
	valueField: 'activityId',
	emptyText: '请选择节点',
	initComponent: function() {
		this.store = this._createStore();
		
		this.callParent();	
	},
	_createStore: function(){
		var me = this, 
			params = '',
			url = this.mainRunTimeUrl + 'getActivityListForReject';
		
		if(this.defaultParams){
			params = Ext.clone(this.defaultParams);
		}
		
		var store = Ext.create('App.ux.approval.store.RejectComboStore',{
			autoLoad : false,
			url: url,
			defaultParam: params
		});
		if(this.defaultDatas){
			store.loadData(Ext.clone(this.defaultDatas));
		}
		
		return store;
	}
	
});