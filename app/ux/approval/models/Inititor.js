/**起草人模版
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.models.Inititor', {
    extend: 'App.ux.approval.Process',
    alias: 'widget.approvalmodelsInititor',
    _getItems: function () {
        var me = this;
        var informOption = this._informOptions();
        informOption.right.items[0].margin = '0 0 0 5';

        return [
            this._opinions(),//处理意见
            //,this._optionButtons(['催办'/* ,'撤回','起草人废弃' */])//操作按钮
            this._initTransaction()
        ].concat(this._centerAreaCenter()).concat(
            [
                this._importantLevel()//通知紧急程度
                //, informOption//通知选项
                , this._currentPerson()//当前处理人
                , this._donePerson()//已处理人
            ]
        );
    },
    _centerAreaCenter: function () {

        var radios = [], front = [], center = [], optionButtonList = this._data.currAssigneeList;
        if (!optionButtonList || !optionButtonList[0].inititorButtonList) {
            return radios;
        }
        this.taskId = optionButtonList[0].taskId;
        optionButtonList = optionButtonList[0].inititorButtonList;
        for (var i = 0; i < optionButtonList.length; i++) {
            radios.push(optionButtonList[i].type);
            center.push(this._getOptionButtonsRalations(optionButtonList[i].type));
        }

        front = [this._optionButtons(radios)];//操作按钮

        return front.concat(center);

    },
    _submitBtn: function () {
        var btn = this.callParent(arguments);
        btn.hidden = true;
        return btn;
    },

    _opinions: function () {
        var opinion = this.callParent();
        opinion.isFirst = true;
        return opinion;
    },

    submitSuccess: function (callBack) {
        var param = this.getValues();
        var win = this.up('window');
        if (win) {
            win.close();
        }
        if (param.cz != 203) {
            this.parentProcess.submitSuccess(callBack);
        }
    },

    //处理事务 获取参数名 切换事务
    _initTransaction: function(){
        var records = this._data.currAssigneeList;
        if (!records || !records[0].inititorButtonList) {
            return {hidden: true};
        }

        var me = this
            ,value = records[0].activityId;

        return {
            xtype:'approvalContainer',
            height: 44,
            name: 'initTransaction',
            hidden: records.length <= 1 ? true : false,
            left: {
                value: '<b>事务</b>',
                margin: '5 0 0 0'
            },
            right: {
                padding: 5,
                layout: {type: 'hbox', align: 'stretch'},
                defaults: {
                    readOnly: this._isView
                },
                items:[
                    {
                        xtype: 'combobox',
                        queryMode: 'local',
                        displayField: 'activityName',
                        valueField: 'activityId',
                        value: value,
                        name: 'initTransaction',
                        store: Ext.create('Ext.data.Store', {
                            fields: ['activityName', 'activityId','inititorButtonList'],
                            data : records
                        }),
                        listeners: {
                            select: function(combo, record){
                                var radiogroup = me.down('approvalContainer[name=optionButtons]').down('radiogroup');
                                var optionButtonList = record.get('inititorButtonList');
                                me.taskId = record.get('taskId');
                                var radios = [];
                                for(var i=0; i<optionButtonList.length; i++){
                                    radios.push(optionButtonList[i].type);
                                }
                                var items = me._getOptionButtonsItems(radios);
                                radiogroup.removeAll();
                                radiogroup.add(items);
                            }
                        }
                    }
                ]
            }
        };
    }
});
