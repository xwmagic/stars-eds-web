/**选择意见下拉框
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.SuggestionCombo',{
    extend: 'Ext.form.ComboBox',
	requires: [
		'App.ux.approval.store.SuggestionComboStore'
	],
	xtype: 'approvalSuggestionCombo',
	fieldLabel: '常用意见',
	width: 300,
	labelWidth: 80,
	emptyText : '==请选择意见==',
	queryMode: 'local',
	displayField: 'name',
	valueField: 'val',
	initComponent: function() {
		this.store = this._createStore();
		
		this.callParent();	
	},
	_createStore: function(){
		var me = this, 
			params = '',
			url = this.url;
		
		if(this.defaultParams){
			params = Ext.clone(this.defaultParams);
		}
		
		var store = Ext.create('App.ux.approval.store.SuggestionComboStore',{autoLoad : false});
		if(this.defaultDatas){
			store.loadData(Ext.clone(this.defaultDatas));
		}
		
		return store;
	}
	
});