/**审批节点流向记录列表
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.store.ProgressInfoGridStore', {
	extend : 'App.ux.approval.store.BaseStore',
	alias : 'store.progressInfoGridStore',
	url: 'getActivityRecordPage',
	fields:['activityName', 'operator', 'operatorName','log','systemLog','createTime'],
	proxy : {
		type : 'ajax',
		actionMethods:{
			create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'
		},
		reader : {
			type : 'json',
			rootProperty : 'data.data',
			totalProperty: 'data.resultCount'
		}
	}
});
