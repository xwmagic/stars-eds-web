/**事务选择下拉框Store
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.store.TransComboStore', {
	extend : 'App.ux.approval.store.BaseStore',
	alias : 'store.transComboStore',
	fields: ['transId', 'transType','transName','transUrl','taskId','activityId','activityName']
});
