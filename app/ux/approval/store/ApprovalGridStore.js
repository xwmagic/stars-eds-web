/**审批记录列表
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.store.ApprovalGridStore', {
	extend : 'App.ux.approval.store.BaseStore',
	alias : 'store.approvalGridStore',
	fields:[ 'activityName','comment', 'operator', 'operatorName','log','systemLog','createTime']
});
