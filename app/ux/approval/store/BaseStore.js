Ext.define('App.ux.approval.store.BaseStore', {
	extend : 'Ext.data.Store',
	alias : 'store.approvalBaseStore',
	pageSize : 15,
	autoLoad : true,
	autoSync : false,
	proxy : {
		type : 'ajax',
		actionMethods:{
			create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'
		},
		reader : {
			type : 'json',
			rootProperty : 'data',
			totalProperty: 'resultCount'
		}
	},
	defaultParam:'',//默认参数
	constructor : function() {
		this.callParent(arguments);
		var me = this;
		if(me.proxy && !me.proxy.url && me.url){
			me.proxy.url = me.url;
		}
		if(me.proxy){
			if(Ext.isObject(this.defaultParam)){
				me.proxy.extraParams = Ext.clone(this.defaultParam);
			}
		}
	},
	_getFields: function(){
		if(!this._confs){
			return;
		}
		var mcs = this._mainConfs,
			fs = this._confs.fields,
			fsRemove = this._confs.fieldsRemove,
			fsOverride = this._confs.fieldsOverride,
			temp, i, j, confObj,flag, type, field,
			fields = [];
			
		if(!fs && mcs){//如果没有配置fields，那么自动根据_mainConfs生成fields
			for(i = 0; i<mcs.length; i++){
				flag = true;
				type = mcs[i].type;
				if(!type){
					type = 'string';
				}
				
				if(fsRemove){//判断是否配置需要移除的字段
					for(j=0; j<fsRemove.length; j++){
						if(mcs[i].name == fsRemove[j]){
							flag = false;
						}
					}
				}
				if(fsOverride && flag){
					for(j=0; j<fsOverride.length; j++){
						confObj = fsOverride[j];
						if(mcs[i].name == confObj.name){
							temp = this._getFieldByName(confObj.name);//该方法来自common.CommonFun对象
							this._autoProperty(confObj, temp, ['name','type']);//自动生成
							fields.push(confObj);
							flag = false;
						}
					}
				}
				if(flag){
					field = {
						name: mcs[i].name,
						type: type
					};
					
					fields.push(field);
					
				}
			}
			return fields;
		}else if(fs){//如果配置了fields，那么优先采用配置的fields
			
			for(i = 0; i<fs.length; i++){
				confObj = fs[i];
				if(Ext.isString(confObj)){//如果是字符串
					temp = this._getFieldByName(confObj);//该方法来自common.CommonFun类
					if(temp){
						type = temp.type;
						if(!type){
							type = 'string';
						}
						
						field = {
							name: temp.name,
							type: type
						};
						fields.push(field);
					}else{
						fields.push(confObj);
					}
					
				}else if(Ext.isObject(confObj)){//如果是对象
					temp = this._getFieldByName(confObj.name);//该方法来自common.CommonFun对象
					this._autoProperty(confObj, temp, ['name','type']);//自动生成
					fields.push(confObj);
				}
			}
			
		}
		return fields;
	},
	
	//将temp对象中的args字段复制到target对象中
	_autoProperty: function(target, temp ,args){
		
		if(!target.offAuto){//是否自动生成开关，默认开启
			if(Ext.isString(args)){
				if(!target.hasOwnProperty(args) && temp[args]){
					target[args] = temp[args];
				}
			}
			if(Ext.isArray(args)){
				for(var i; i<args.length; i++){
					if(!target.hasOwnProperty(args[i]) && temp[args[i]]){
						target[args[i]] = temp[args[i]];
					}
				}
			}
		}
	},
	
	_getFieldByName: function(name){
		var mc = this._mainConfs;
		
		if(!mc || !name){
			return;
		}
		
		for(var i = 0; i<mc.length; i++){
			if(mc[i].name == name){
				return mc[i]
				break;
			}
		}
	},
	_getXtype: function(type){
		switch(type){
			case 'int':
				return 'numberfield';
				break;
			case 'date':
				return 'datefield';
				break;
			case 'bigdecimal':
				return 'numberfield';
				break;
			case 'number':
				return 'numberfield';
				break;
			default:
				return 'textfield';
		}
		
	}
});
