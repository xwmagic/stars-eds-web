/**
 * 人员选择公共组件，调用时配置以下参数，有什么参数组件就传值给什么组件
 * @memberSelModel:single/multi 单选或者复选，默认多选
 * 
 */
Ext.define('App.ux.approval.MemberMulitSelectField', {
    extend: 'Ext.form.field.Tag',
    alias: 'widget.memberMulitSelectField',
	requires: [
		'App.ux.approval.MemberSelectWindow'
	],

    //editable :false,
    selectOnFocus: false,
    store: {
        fields: ['name','loginName']
    },
    displayField: 'name',
    valueField: 'loginName',
    queryMode: 'local',
    publishes: 'value',
	minHeight: 34,
	forceSelection: true,
	emptyText: '添加人员',
	excludes: [],//排除人员，当查询人员时该组中的成员不会被显示

    initComponent: function () {
        var me = this;
        var memberSelModel = me.memberSelModel;

        var selectWin;

        this.listeners = {
			expand: function (field, eOpts) {
				if(selectWin && !selectWin.destroyed){
					return;
				}else{
					selectWin = Ext.create({
						xtype: "approvalMemberSelectWindow",
						myTagField: me,
						excludes: me.excludes,
						memberSelModel: memberSelModel
					});
					selectWin.show();
				}
				
			},
			focus: function (field, eOpts) {
				if(selectWin && !selectWin.destroyed){
					return;
				}else{
					selectWin = Ext.create({
						xtype: "approvalMemberSelectWindow",
						myTagField: me,
						excludes: me.excludes,
						memberSelModel: memberSelModel
					});
					selectWin.show();
				}
			}
		};
		
        this.callParent();
    }

});
