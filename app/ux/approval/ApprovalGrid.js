/**审批记录列表
 * @author xiaowei
 * @date 2017年6月2日
 */
Ext.define('App.ux.approval.ApprovalGrid',{
    extend: 'App.ux.approval.BaseGrid',
	requires: [
		'App.ux.approval.store.ApprovalGridStore', 
		'App.ux.approval.BaseGrid'
	],
	xtype: 'approvalGrid',
	isPageGrid: false,
	
	_createStore: function(){
		var me = this, 
			className,
			params = '',
			url = this.mainUrl;
		
		if(this.defaultParams){
			params = Ext.clone(this.defaultParams);
		}
		
		var store = Ext.create('App.ux.approval.store.ApprovalGridStore',{autoLoad : false});
		if(this.defaultDatas){
			store.loadData(Ext.clone(this.defaultDatas));
		}
		return store;
	},
	
	_createColumns: function(){
		var columns = [
			 {xtype: 'rownumberer'}
            ,{text: '时间', dataIndex: 'createTime',flex: 1,minWidth: 155}
            ,{text: '节点名称', dataIndex: 'activityName',flex: 1,minWidth: 120}
            ,{text: '操作者', dataIndex: 'operatorName',flex: 1,minWidth: 100}
            ,{text: '操作', dataIndex: 'log',flex: 2,minWidth: 100}
			,{text: '处理意见', dataIndex: 'comment',flex: 4,
				renderer: function(value, meta, record) {
            		meta.style = 'overflow:auto;padding: 3px 6px;text-overflow: ellipsis;white-space: nowrap;white-space:normal;line-height:20px;';
            		return value;
        	}}
		];
		return columns;
	}
	
});