/**
 * 单选框选择组件
 * @author xiaowei
 * @date 2018年9月2日
 */
Ext.define('App.view.BaseCheckboxGroup', {
	extend: 'Ext.form.CheckboxGroup',
	alias: 'widget.BaseCheckboxGroup',
    params:{},//默认参数
    dynamicDataUrl:'',//远程请求地址
    autoLoad: true,//是否自动加载
    initComponent : function() {
        if(this.autoLoad){
            this.load();
        }
        this.callParent();
    },
    load:function (param) {
        var me = this;
        Ext.Ajax.request({
            url: this.dynamicDataUrl,
            method: 'get',
            params: param ? param : this.params,
            success: function (response) {
                var obj = Ext.decode(response.responseText);
                var data = obj.data, radios = [];
                for(var i=0; i<data.length; i++){
                    var item = {
                        boxLabel: data[i].itemName,
                        name:me.name,
                        readOnly:me.readOnly,
                        inputValue:data[i].id
                    };
                    if(me.itemWidth){
                        item.width = me.itemWidth;
                    }
                    radios.push(item);
                }
                me.add(radios);
                if(me.value){
                    me.setValue(me.value);
                }
            },
            failure: function (response) {

            }

        });
    }
});

