/**
 * 单选框选择组件
 * @author xiaowei
 * @date 2018年9月2日
 */
Ext.define('App.view.BaseRadioGroup', {
	extend: 'Ext.form.RadioGroup',
	alias: 'widget.BaseRadioGroup',
    params:{},//默认参数
    dynamicDataUrl:'',//远程请求地址
    autoLoad: true,//是否自动加载
    isSelectFirst:false,//是否默认选中第一个值
    initComponent : function() {
	    if(this.autoLoad){
            this.load();
        }
        //必填时数据验证添加红色边框效果
        var radiogroup = this;
        radiogroup.on('validitychange', function (file, isValid, eOpts) {
            if(!isValid){
                file.setStyle({borderColor:'#cf4c35', borderWidth:'1px',borderStyle:'solid'});
            }else {
                file.setStyle({borderWidth:'0px'});
            }
        });
        this.callParent();
    },
    load:function (param) {
        var me = this;
        Ext.Ajax.request({
            url: this.dynamicDataUrl,
            method: 'get',
            params: param ? param : this.params,
            success: function (response) {
                var obj = Ext.decode(response.responseText);
                var data = obj.data, radios = [];
                for(var i=0; i<data.length; i++){
                    if(i == 0 && me.isSelectFirst){
                        radios.push({
                            boxLabel: data[i].itemName,
                            name:me.name,
                            readOnly:me.readOnly,
                            inputValue:data[i].id,
                            checked: true
                        });
                        continue;
                    }
                    radios.push({
                        boxLabel: data[i].itemName,
                        name:me.name,
                        readOnly:me.readOnly,
                        inputValue:data[i].id
                    });
                }
                me.add(radios);
                if(me.value){
                    me.setValue(me.value);
                }
            },
            failure: function (response) {

            }

        });
    }
});

