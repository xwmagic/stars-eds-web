Ext.define('App.view.BaseWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.base-window',

    requires: [
        'Ext.layout.container.Fit'
    ],

    autoShow: true,
    modal: true,
    layout: 'fit',
    width: 800,
    checkData: function(grid, modified) {
        var result = true;
        modified.each(function(record) {
            var keys = record.joined[0].config.fields; //获取Record的所有名称  
            Ext.each(keys, function(name) {
                //根据名称获取对应的值  
                var value = record.get(name);
                if (value == null) {
                    value = "";
                }
                Ext.each(grid.getColumns(), function(cm) {
                    if (cm.dataIndex == name) {
                        var editor = cm.getEditor();
                        if (editor) {
                            var r = editor.validateValue(value);
                            if (!r) {
                                Ext.MessageBox.alert("验证数据", "对不起,您输入的" + editor.column.text +
                                    "数据不符合规则！");
                                result = false;
                                return;
                            }
                        }

                    }
                });
                if (!result) {
                    return;
                }
            });
            if (!result) {
                return;
            }

        });
        return result;
    }
});