Ext.define('Admin.store.SubMonitorGridStore',{
	extend: 'Ext.data.Store',
	alias: 'store.submonitor-list',

    fields: [
        'name'
    ],

    data: [
		{ name: '温度偏高' },
		{ name: '电压偏高'},
		{ name: '空挂数偏高'},
		{ name: '时间偏高'}
    ]
});