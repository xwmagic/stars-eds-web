var Authorization = localStorage.getItem('Authorization');
if(Authorization){
    setTimeout(function () {
        Ext.Microloader.run();
    },50);
    $("#loadHTMLText2017").attr("style","display:none;");
}else{
    if (!!window.ActiveXObject || "ActiveXObject" in window){

    }else{
        var canvas = document.getElementById('canvas');
        canvas.height = window.innerHeight;
        var spaceboi = new Thpace(canvas);
        spaceboi.start();
    }
}
$("input[ name='userName']").bind('keyup',function(event){
    if(event.keyCode == "13")
    {
        login();
    }
});
$("input[ name='password']").bind('keyup',function(event){
    if(event.keyCode == "13")
    {
        login();
    }
});
function login() {
    //1.先拿到帐号和密码
    var data = {
        userName: $("input[ name='userName']").val(),
        password: $("input[ name='password']").val()
    };
    var myRoot = {};
    if (!data.userName || !data.password || !data.userName.trim()) {
        var emptyfields = $(".input_outer");
        emptyfields.each(function() {
            $(this).stop()
                .animate({ left: "-10px" }, 100).animate({ left: "10px" }, 100)
                .animate({ left: "-10px" }, 100).animate({ left: "10px" }, 100)
                .animate({ left: "0px" }, 100);
        });
        return;
    }
    $.ajax({
        //请求方式
        type : "POST",
        //请求的媒体类型
        dataType: "json",
        //请求地址
        url : "api/auth/token",
        //数据，json字符串
        data : {
            username: data.userName.trim(),
            password: hex_md5(data.password)
        },
        //请求成功
        success : function(obj) {
            if(obj.hasErrors){
                $.message({
                    message:obj.errorMessage,
                        type:'error'
                });
                return;
            }
            localStorage.setItem("Authorization", obj.data.token);
            localStorage.setItem("currentUser", JSON.stringify(obj.data.user));
            document.cookie = "userId="+obj.data.user.id;
            Ext.Microloader.run();
            $("#loadHTMLText2017").attr("style","display:none;");
            $("#loadHTMLprogress2017").attr("style","display:'inline';");
        },
        //请求失败，包含具体的错误信息
        error : function(e){
            console.log(e.status);
            console.log(e.responseText);
        }
    });
}